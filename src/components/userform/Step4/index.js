/**
 * Component for the Signup Form
 * @WhatToDo
 * - Creates a Component for the Signup
 * - This form includes the following inputs
 *   |-----------------------------------------------------------------------------+
 *   |   MedNurse Enrollment Form (Step 4 - Summary and Edit Page)                 |
 *   |-----------------------------------------------------------------------------+
 *   |   This Page includes all inputs entered in previous screens                   |
 *   |-----------------------------------------------------------------------------+
 * */

import React, {Component} from 'react';
import {Alert, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Button,} from '../../shared';
import g_styles from './styles';
import contstants from '../../_base_data/constants';
import {CheckBox} from 'native-base';
import {POP} from '../../patients/_CustomViews/POP';
import theme from '../../../theme/default';

class Step4 extends Component {
    _scrollView = null;

    state = {};

    state1 = {
        selectedTabIndex: 0,
        stepFormData1: {
            first_name: 'Nahrae Jin',
            last_name: 'last_name',
            gender: 'female',
            dob: 'dob',
            address1: 'address1',
            address2: 'address2',
            state: 'Alabama',
            county: 'county',
            city: 'city',
            zip: 'zip',
            mobile_phone: 'mobile_phone',
            home_phone: 'home_phone',
            bio: 'bio',
            licensure_cards: [
                {state: 'Alabama', license_no: '111', designation: 'RN'},
                {state: 'Arizona', license_no: '222', designation: 'CNA'}
            ],
            upload_resume: {},
            upload_picture: {},
            email: 'email',
            password: 'password',
            password_confirmation: 'password_confirmation'
        },
        stepFormData2: {
            states: ['Alabama', 'Arizona'],
            counties: {
                'Alabama': [
                    'Autauga', 'Baldwin'
                ],
                'Arizona': [
                    'Apache'
                ]
            },
            stateKey: '',
            shownCountiesList: false,
            tmpSelectedCounties: []
        },
        stepFormData3: {
            shownMap: false,
            states: ['Alabama'],
            counties: {
                'Alabama': ['Autauga', 'Baldwin'],
                'Arizona': ['Apache']
            },
            zipcodes: {
                'Autauga': ['35136', '35160', '35096']
            },
            countyKey: '',
            tmpSelectedZipCodes: []
        },
        stepFormData4: {},
    };

    constructor(props) {
        super(props);
    }

    humanize = (str) => {
        switch (str) {
            case '':
                return 'None';
            case 'dob':
                return 'Date of birth';
            case 'upload_resume':
                return 'Resume';
            case 'upload_picture':
                return 'Picture';
        }
        return str
            .replace(/^[\s_]+|[\s_]+$/g, '')
            .replace(/[_\s]+/g, ' ')
            .replace(/^[a-z]/, function (m) {
                return m.toUpperCase();
            });
    };

    componentWillMount(): void {
    }

    componentDidMount(): void {

    }

    _handlePress = () => {
        if (this.state.tos !== true) {
            this.setState({show_tos_mandatory: true});
            return;
        }
        Alert.alert(contstants.APP.NAME, 'Successfully saved!');
    };


    _openTOS = () => {
        POP.modal({
            contentView: <View style={{padding: 10}}>
                <Text style={{fontSize: theme.fontSizeMedium, marginVertical: 10}}>Terms of Service</Text>
                <Text style={{lineHeight: 22,}}>
                    Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of
                    classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin
                    professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words,
                    consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical
                    literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33
                    of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This
                    book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of
                    Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
                </Text>
            </View>
            ,
            buttons: [
                {
                    text: 'Agree', options: {primary: true, block: true},
                    onPress: async () => {
                        await this.setState({tos: true});
                    }
                },
                {
                    text: 'Cancel', options: {bordered: true, danger: true, block: true},
                    onPress: () => {

                    }
                }
            ],
            container: this
        });
    };

    render() {
        return (
            <ScrollView
                style={{marginTop: 40, padding: 15, backgroundColor: 'rgba(255,255,255,1)',}}
                ref={view => (this._scrollView = view)}
            >
                <View>
                    <Text style={styles.step4.title.large}>
                        Step 1
                    </Text>
                    <Button
                        buttonStyle={[
                            g_styles.nav_button, {width: 50, position: 'absolute', right: 10, top: 5}
                        ]}
                        textStyle={[g_styles.nav_button_text]}
                        onPress={this.props.onGotoStep && this.props.onGotoStep.bind(this, 0)}
                    >
                        Edit
                    </Button>
                </View>
                <View style={{
                    marginTop: 20,
                    borderBottomWidth: 1,
                    borderColor: 'lightgray'
                }}>
                    <Text style={styles.step4.title.medium}>
                        Personal Information
                    </Text>
                </View>
                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        Name:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.props.formData1.first_name}, {' '}
                        {this.props.formData1.last_name}
                    </Text>
                </View>
                }
                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        Email:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.props.formData1.email}
                    </Text>
                </View>
                }
                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        Gender:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.humanize(this.props.formData1.gender)}
                    </Text>
                </View>
                }
                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        Date of Birth:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.humanize(this.props.formData1.dob || '')}
                    </Text>
                </View>
                }
                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        Address 1:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.props.formData1.address1 || 'None'}
                    </Text>
                </View>
                }
                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        Address 2:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.props.formData1.address2 || 'None'}
                    </Text>
                </View>
                }
                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        State:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.props.formData1.state || 'None'}
                    </Text>
                </View>
                }
                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        County:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.props.formData1.county || 'None'}
                    </Text>
                </View>
                }
                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        City:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.props.formData1.city || 'None'}
                    </Text>
                </View>
                }
                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        Zip:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.props.formData1.zip || 'None'}
                    </Text>
                </View>
                }
                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        Mobile Phone:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.props.formData1.mobile_phone || 'None'}
                    </Text>
                </View>
                }
                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        Home Phone:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.props.formData1.home_phone || 'None'}
                    </Text>
                </View>
                }

                <View style={{
                    marginTop: 40,
                    borderBottomWidth: 1,
                    borderColor: 'lightgray'
                }}>
                    <Text style={styles.step4.title.medium}>
                        Documents
                    </Text>
                </View>

                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        Picture:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.props.formData1.upload_picture.filename || 'Not Uploaded'}
                    </Text>
                </View>
                }

                {this.props.formData1 &&
                <View style={styles.step4.line.small}>
                    <Text style={styles.step4.title.small}>
                        Resume:
                    </Text>
                    <Text style={styles.step4.title.small}>
                        {this.props.formData1.upload_resume.filename || 'Not Uploaded'}
                    </Text>
                </View>
                }

                <View style={{
                    marginTop: 40,
                    borderBottomWidth: 1,
                    borderColor: 'lightgray'
                }}>
                    <Text style={styles.step4.title.medium}>
                        Licensure Data
                    </Text>
                </View>

                {this.props.formData1 &&
                this.props.formData1.licensure_cards &&
                this.props.formData1.licensure_cards.map((licensure_card, index) => {
                    return (
                        <View key={`key_${index}`}
                              style={{
                                  marginLeft: 5,
                                  borderBottomColor: 'lightgray', borderBottomWidth: 1
                              }}>
                            <View style={styles.step4.line.small}>
                                <Text style={styles.step4.title.small}>
                                    State of Licensure:
                                </Text>
                                <Text style={styles.step4.title.small}>
                                    {licensure_card.state || 'None'}
                                </Text>
                            </View>
                            <View style={styles.step4.line.small}>
                                <Text style={styles.step4.title.small}>
                                    Designation:
                                </Text>
                                <Text style={styles.step4.title.small}>
                                    {licensure_card.designation || 'None'}
                                </Text>
                            </View>
                            <View style={styles.step4.line.small}>
                                <Text style={styles.step4.title.small}>
                                    License No:
                                </Text>
                                <Text style={styles.step4.title.small}>
                                    {licensure_card.license_no || 'None'}
                                </Text>
                            </View>
                        </View>
                    );

                })
                }


                <View style={{marginTop: 30, height: 50}}>
                    <Text style={styles.step4.title.large}>
                        Step 2
                    </Text>
                    <Button
                        buttonStyle={[
                            g_styles.nav_button, {width: 50, position: 'absolute', right: 10, top: 5}
                        ]}
                        textStyle={[g_styles.nav_button_text]}
                        onPress={this.props.onGotoStep && this.props.onGotoStep.bind(this, 1)}
                    >
                        Edit
                    </Button>
                </View>


                <View style={{
                    borderBottomWidth: 1,
                    borderColor: 'lightgray'
                }}>
                    <Text style={styles.step4.title.medium}>
                        County Selection
                    </Text>
                </View>

                {this.props.formData1 &&
                this.props.formData1.licensure_cards &&
                this.props.formData1.licensure_cards.map((licensure_card, index) => {
                    let state = licensure_card.state;
                    return (
                        <View key={`key_${index}`}
                              style={{
                                  marginLeft: 5,
                                  borderBottomColor: 'lightgray', borderBottomWidth: 1
                              }}>
                            <View style={styles.step4.line.small}>
                                <Text style={styles.step4.title.small}>
                                    State:
                                </Text>
                                <Text style={styles.step4.title.small}>
                                    {state || 'None'}
                                </Text>
                            </View>
                            {this.props.formData2.counties[state] &&
                            this.props.formData2.counties[state].map((county, index) => {
                                return (
                                    <View key={`key_${index}`} style={styles.step4.line.small}>
                                        <Text style={styles.step4.title.small}>
                                            County:
                                        </Text>
                                        <Text style={styles.step4.title.small}>
                                            {county || 'None'}
                                        </Text>
                                    </View>
                                );
                            })
                            }
                        </View>
                    );

                })
                }


                <View style={{marginTop: 30, height: 50}}>
                    <Text style={styles.step4.title.large}>
                        Step 3
                    </Text>
                    <Button
                        buttonStyle={[
                            g_styles.nav_button, {width: 50, position: 'absolute', right: 10, top: 5}
                        ]}
                        textStyle={[g_styles.nav_button_text]}
                        onPress={this.props.onGotoStep && this.props.onGotoStep.bind(this, 2)}
                    >
                        Edit
                    </Button>
                </View>


                <View style={{
                    borderBottomWidth: 1,
                    borderColor: 'lightgray'
                }}>
                    <Text style={styles.step4.title.medium}>
                        Zip Code Selection
                    </Text>
                </View>

                {this.props.formData1 &&
                this.props.formData1.licensure_cards &&
                this.props.formData1.licensure_cards.map((licensure_card, index) => {
                    let state = licensure_card.state;
                    return (
                        <View key={`key_${index}`}
                              style={{
                                  marginLeft: 5,
                                  borderBottomColor: 'lightgray', borderBottomWidth: 1
                              }}>
                            <View style={styles.step4.line.small}>
                                <Text style={styles.step4.title.small}>
                                    State:
                                </Text>
                                <Text style={styles.step4.title.small}>
                                    {state || 'None'}
                                </Text>
                            </View>
                            {this.props.formData2.counties[state] &&
                            this.props.formData2.counties[state].map((county, index) => {
                                return (
                                    <View key={`key_${index}`}>
                                        <View key={`key_${index}`} style={styles.step4.line.small}>
                                            <Text style={styles.step4.title.small}>
                                                County:
                                            </Text>
                                            <Text style={styles.step4.title.small}>
                                                {county || 'None'}
                                            </Text>
                                        </View>


                                        <View style={styles.step4.line.small}>
                                            <Text style={styles.step4.title.small}>
                                                Zip Codes:
                                            </Text>
                                        </View>

                                        <View style={[styles.step4.line.nowrap]}>
                                            {this.props.formData3.zipcodes[county] &&
                                            this.props.formData3.zipcodes[county].map(({zipcode}, index) => {
                                                return (
                                                    <Text key={`key_${index}`}
                                                          style={[styles.step4.title.rounded]}>
                                                        {zipcode || 'None'}
                                                    </Text>
                                                );
                                            })
                                            }
                                        </View>


                                    </View>
                                );
                            })
                            }
                        </View>
                    );

                })
                }


                <TouchableOpacity
                    style={{flex: 1, flexDirection: 'row', marginTop: 30}}
                    onPress={() => this.setState({tos: !this.state.tos})}>
                    <CheckBox
                        disabled
                        style={{marginLeft: -10, marginRight: 15}}
                        checked={this.state && this.state.tos}
                    />
                    <Text>Terms of Service</Text>

                </TouchableOpacity>


                {this.state && !this.state.tos && this.state.show_tos_mandatory &&
                <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'row'}}>
                    <Text style={{color: theme.redColor}}>
                        It is required to check {' '}
                    </Text>
                    <TouchableOpacity onPress={this._openTOS}>
                        <Text style={{color: theme.blueColor}}>Terms of Service</Text>
                    </TouchableOpacity>
                </View>
                }


                <View style={{marginTop: 20, marginBottom: 50, flexDirection: 'row'}}>
                    <Button
                        buttonStyle={[
                            g_styles.nav_button
                        ]}
                        textStyle={[g_styles.nav_button_text]}
                        onPress={this.props.onPrevStep && this.props.onPrevStep.bind(this)}
                    >
                        Previous
                    </Button>
                    <Button
                        disabled={true}
                        buttonStyle={[
                            g_styles.nav_button,
                            g_styles.disabled
                        ]}
                        textStyle={[g_styles.nav_button_text]}
                    >
                        Next
                    </Button>
                    <Button
                        buttonStyle={[
                            g_styles.nav_button
                        ]}
                        textStyle={[g_styles.nav_button_text]}
                        onPress={this._handlePress}
                    >
                        Finish
                    </Button>
                </View>
            </ScrollView>
        );
    }
}

export default Step4;
const styles = {
    step4: {
        base: {},
        title: {
            large: {
                fontSize: 18,
                fontWeight: '500'
            },
            medium: {
                fontSize: 16,
                fontWeight: '400'
            },
            small: {
                fontSize: 14,
                lineHeight: 30
            },
            rounded: {
                fontSize: 14,
                lineHeight: 20,
                borderWidth: 1,
                paddingHorizontal: 5,
                marginHorizontal: 5,
                borderColor: 'darkgray',
                borderRadius: 5
            },
            nowrap: {},

        },
        line: {
            large: {
                height: 40
            },
            medium: {
                paddingHorizontal: 10,
                borderBottomWidth: 1,
                borderBottomColor: 'rgba(222,222,222, 0.5)',
                height: 40,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 20
            },
            small: {
                paddingHorizontal: 10,
                borderBottomWidth: 1,
                borderBottomColor: 'rgba(222,222,222, 0.5)',
                height: 40,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
            },
            nowrap: {
                flexDirection: 'row',
                flexWrap: 'wrap'
            },
        }
    }
};
