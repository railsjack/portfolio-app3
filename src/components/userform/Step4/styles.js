const styles = {
    form: {
        base: {
            width: "100%",
            fontSize: 14,
            color: "rgb(64, 64, 64)"
        },
        card_line: {
            flexDirection: "column",
            marginBottom: 10,
            backgroundColor: "transparent"
        },
        text_input: {
            paddingHorizontal: 10,
            paddingVertical: 0,
            backgroundColor: "white",
            height: 35
        },
        text_area: {},
        desc: {
            base: {
                flexDirection: "column",
                backgroundColor: "transparent"
            },
            content: {
                fontSize: 16
            },
            title: {
                color: "rgb(64, 64, 64)",
                height: 25,
                lineHeight: 25,
                marginTop: 15
            }
        },
        first_name: {},
        last_name: {},
        gender: {
            height: 50
        },
        date_of_birth: {
            width: "100%",
            backgroundColor: "red"
        },
        address1: {},
        address2: {},
        state: {
            color: "rgb(125, 125, 125)",
            borderColor: "darkgray",
            borderWidth: 1,
            height: 35,
            fontSize: 10
        },
        state_item: {
            paddingHorizontal: 0,
            paddingVertical: 0
        },
        county: {
            color: "rgb(125, 125, 125)",
            borderColor: "darkgray",
            borderWidth: 1,
            height: 35,
            fontSize: 10
        },
        city: {},
        zip: {},
        mobile_phone: {},
        home_phone: {},
        bio: {
            paddingTop: 5,
            height: 60,
            lineHeight: 18,
            textAlignVertical: "top"
        },
        licensure_card: {},
        upload_view: {
            base: {
                flexDirection: "column",
                backgroundColor: "transparent"
            },
            header: {},
            title: {
                fontSize: 16,
                color: "black",
                padding: 4
            },
            buttons: {
                width: "100%",
                flexDirection: "row",
                backgroundColor: "transparent"
            },
            button_picture: {
                width: "40%",
                height: 25,
                paddingTop: 2,
                paddingBottom: 2,
                borderWidth: 0,
                flex: 1,
                alignSelf: "stretch",
                backgroundColor: "rgb(65, 178, 171)"
            },
            button_picture_text: {
                height: 25,
                lineHeight: 20,
                textAlign: "center",
                fontWeight: "500",
                fontSize: 12,
                color: "#fff"
            },
            text_picture: {
                width: "60%",
                borderBottomWidth: 1,
                borderBottomColor: "#6b6b6b",
                padding: 0,
                marginLeft: 5
            },
            button_resume: {
                width: "40%",
                height: 25,
                marginTop: 10,
                paddingTop: 2,
                paddingBottom: 2,
                borderWidth: 0,
                flex: 1,
                alignSelf: "stretch",
                marginLeft: "auto",
                backgroundColor: "rgb(65, 178, 171)"
            },
            button_resume_text: {
                height: 25,
                lineHeight: 20,
                textAlign: "center",
                fontWeight: "500",
                fontSize: 12,
                color: "#fff"
            },
            text_resume: {
                width: "60%",
                marginTop: 10,
                borderBottomWidth: 1,
                borderBottomColor: "#6b6b6b",
                padding: 0,
                marginLeft: 5
            }
        },
        email: {},
        password: {},
        password_confirmation: {},
        mandatory: {
            color: "red",
            fontSize: 12
        }
    },
    input: {
        base: {
            backgroundColor: "rgba(255,255,255,0.5)",
            height: 40,
            marginBottom: 10,
            color: "rgba(0,0,0,0.75)",
            paddingHorizontal: 10
        },
        first_name: {}
    },
    login_title: {
        height: 40,
        lineHeight: 40,
        color: "black",
        fontSize: 16,
        paddingHorizontal: 5,
        marginTop: 10
    },
    login_desc: {
        height: 30,
        lineHeight: 30,
        fontSize: 12
    },
    license_form: {
        base: {
            width: "100%"
        },
        desc: {
            base: {
                flexDirection: "row",
                marginBottom: 5
            },
            title: {
                height: 35,
                lineHeight: 35,
                marginRight: "auto"
            },
            picker: {
                width: 100,
                height: 35,
                lineHeight: 35,
                marginRight: "auto"
            }
        }
    },
    license_cards: {
        base: {},
        card_view: {
            base: {
                width: "100%",
                marginLeft: "auto",
                marginRight: "auto"
            },
            title: {fontSize: 16, color: "green"},
            line1: {
                base: {
                    width: "50%"
                }
            },
            line2: {
                base: {
                    flexDirection: "row"
                },
                state: {
                    marginLeft: "auto",
                    width: "50%"
                },
                license_no: {
                    marginTop: 10,
                    width: "50%",
                    height: 30,
                    paddingVertical: 0,
                    borderBottomWidth: 1,
                    borderColor: "lightgray"
                }
            }
        }
    },

    multi_counties: {
        backgroundColor: "red",
        height: 50
    },

    step2_states: {
        base: {
            borderRadius: 4,
            borderWidth: 1,
            borderColor: "lightgray",
            marginTop: 10,
            backgroundColor: "transparent",
        },
        title: {
            paddingHorizontal: 5,
            paddingVertical: 5,
            flexDirection: "row",
        },
        right_arrow: {
            marginLeft: "auto",
            width: 20,
            height: 20,
            borderRadius: 20,
            color: "darkgray",
            textAlign: "center",
            backgroundColor: "rgba(192,192,192, 0.2)"
        },
        selected_counties: {
            paddingHorizontal: 5,
            paddingVertical: 5,
        },
        select_states_button: {
            container: {
                width: "100%",
                height: 30,
                flex: 1,
                flexDirection: "row",
                position: "absolute",
                bottom: 17,
                left: 0,
                zIndex: 1000,
                paddingHorizontal: 5
            },
            base: {
                width: "50%",
                height: 30,
                backgroundColor: "rgb(78, 179, 172)",
                paddingHorizontal: 10,
                marginLeft: "auto"
            },
            text: {
                lineHeight: 30,
                color: "white",
                textAlign: "center",
                fontSize: 14,
                paddingVertical: 0,
                paddingHorizontal: 0
            }
        }
    },


    nav_button: {
        backgroundColor: "rgb(78, 179, 172)",
        width: "33.3333%",
        height: 30,
    },
    nav_button_text: {
        lineHeight: 30,
        color: "white",
        textAlign: "center",
        fontSize: 14,
        paddingHorizontal: 0
    },
    disabled: {
        backgroundColor: "rgb(192, 192, 192)"
    },

    label: {
        base: {

            color: "#6b6b6b"
        }
    }
};

export default styles;
