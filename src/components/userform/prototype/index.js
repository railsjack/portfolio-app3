/**
 * The SignUp Form Component
 * @WhatToDo
 *  Renders the Signup Form
 *  This SignUp Form has a Tab and a screen area
 *  consists of 4 steps (Index, Index, Index and Index)
 *  Every tab has a screen on itself.
 * @Structure

 *
 * */

import React, { Component } from "react";
import { ImageBackground, View, StatusBar } from "react-native";
import { Container, Button, H3, Text } from "native-base";

const launchscreenBg = require("./launchscreen-bg.png");
const launchscreenLogo = require("./logo-kitchen-sink-2.png");

import styles from "./styles";

class SignUpForm extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <Container>
                <StatusBar barStyle="light-content" />
                <ImageBackground source={launchscreenBg} style={styles.imageContainer}>
                    <View style={styles.logoContainer}>
                        <ImageBackground source={launchscreenLogo} style={styles.logo} />
                    </View>
                    <View
                        style={{
                            alignItems: "center",
                            marginBottom: 50,
                            backgroundColor: "transparent"
                        }}
                    >
                        <H3 style={styles.text}>
                            Ruby on Rails, PHP, {"\n"}
                            Laravel, CodeIgniter, WordPress
                        </H3>
                        <View style={{ marginTop: 40 }} />
                        <H3 style={styles.text}>React-Native, Ionic,{"\n"}
                            Angular, iOS, Android</H3>
                        <View style={{ marginTop: 8 }} />
                    </View>
                    <View style={{ marginBottom: 80 }}>
                        <Button
                            style={{ backgroundColor: "#6FAF98", alignSelf: "center" }}
                            onPress={() => this.props.navigation.openDrawer()}
                        >
                            <Text>Lets Go!</Text>
                        </Button>
                    </View>
                </ImageBackground>
            </Container>
        );
    }
}

export default SignUpForm;
