/**
 * The SignUp Form Component
 * @WhatToDo
 *  Renders the Signup Form
 *  This SignUp Form has a Tab and a screen area
 *  consists of 4 steps (Index, Index, Index and Index)
 *  Every tab has a screen on itself.
 * @Structure
 *  StepTabs
 *    Index
 *        |--------------------------------------------------------------------+
 *        |   MedNurse Enrollment Form (Step 1 - Personal Information)         |
 *        |--------------------------------------------------------------------+
 *        |   1.  First Name          |   11.  Mobile Phone                    |
 *        |   2.  Last Name           |   12.  Home Phone                      |
 *        |   3.  Sex                 |   13.  Bio                             |
 *        |   4.  Date of Birth       |   14.  LicensureCards                  |
 *        |   5.  Address 1           |   15.  Upload Resume                   |
 *        |   6.  Address 2           |   16.  Upload Picture                  |
 *        |   7.  State               |   17.  Create Login Credentials        |
 *        |   8.  County              |   18.  Password                        |
 *        |   9.  City                |   19.  Password Confirmation           |
 *        |  10.  Zip                 |                                        |
 *        |--------------------------------------------------------------------+
 *    Index
 *        |--------------------------------------------------------------------+
 *        |   MedNurse Enrollment Form (Step 2 - Select County for Work)       |
 *        |--------------------------------------------------------------------+
 *        |   21. Select a County for Work                                     |
 *        |--------------------------------------------------------------------+
 *    Index
 *        |--------------------------------------------------------------------+
 *        |   MedNurse Enrollment Form (Step 3 - Select Zip Codes              |
 *        |--------------------------------------------------------------------+
 *        |   22. Select Zip Codes                                             |
 *        |--------------------------------------------------------------------+
 *    Index
 *        |--------------------------------------------------------------------+
 *        |   MedNurse Enrollment Form (Step 4 - Review & Post your Profile)   |
 *        |--------------------------------------------------------------------+
 *        |   Review Pages                                                     |
 *        |--------------------------------------------------------------------+
 *
 *  ScreenArea
 *
 * */

import React, {Component} from 'react';
import {Platform, StatusBar, View} from 'react-native';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import Step4 from './Step4';
import {StepTabs} from './_CustomViews/StepTabs';
import constants from '../_base_data/constants';
import UserStore from '../../store/UserStore';

class UserForm extends Component {

    state = {
        selectedTabIndex: 0,
        stepFormData1: null, //constants.APP.USERFORM_DATA_STRUCTURE.stepFormData1,
        stepFormData2: null, //constants.APP.USERFORM_DATA_STRUCTURE.stepFormData2,
        stepFormData3: null, //constants.APP.USERFORM_DATA_STRUCTURE.stepFormData3,
        stepFormData4: null, //constants.APP.USERFORM_DATA_STRUCTURE.stepFormData4,
        tabs: [
            {
                key: 'step1',
                name: 'Step 1',
                screen: '',
                passed: true
            },
            {
                key: 'step2',
                name: 'Step 2',
                screen: '',
                passed: false
            },
            {
                key: 'step3',
                name: 'Step 3',
                screen: '',
                passed: false
            },
            {
                key: 'step4',
                name: 'Step 4',
                screen: '',
                passed: false
            }
        ]
    };

    constructor(props) {
        super(props);
    }

    componentWillUnmount(): void {

    }

    async componentDidMount(): void {
        if (constants.APP.USE_PRE_DATA.IN_SIGNUP) {
            this.setState({stepFormData1: constants.PRE_DATA.STEP1_INFORMATION});
        }


        if (this.props.mode === constants.APP.USERFORM_MODE.EDIT) {
            await this._getUserData();
        } else {

            await this.setState({
                stepFormData1: Object.assign({}, constants.APP.USERFORM_DATA_STRUCTURE.stepFormData1),
                stepFormData2: Object.assign({}, constants.APP.USERFORM_DATA_STRUCTURE.stepFormData2),
                stepFormData3: Object.assign({}, constants.APP.USERFORM_DATA_STRUCTURE.stepFormData3),
                stepFormData4: Object.assign({}, constants.APP.USERFORM_DATA_STRUCTURE.stepFormData4),
            });
        }
    }

    _getUserData = async () => {
        let _formData = await UserStore.getUserFormData();

        await this.setState({
            stepFormData1: _formData.stepFormData1,
            stepFormData2: _formData.stepFormData2,
            stepFormData3: _formData.stepFormData3
        });
    };

    goNextStep = async () => {
        /**
         * set stepPassed=true in stepFormDataN{1,2,3}
         * */
        if (this.state.selectedTabIndex === 0) {
            let _formData = this.state.stepFormData1;
            _formData.stepPassed = true;
            await this.setState({stepFormData1: _formData});
        }

        let index = this.state.selectedTabIndex + 1;
        if (index >= 0 && index <= 3) {
            let _tabs = this.state.tabs;
            _tabs[index].passed = true;
            await this.setState({tabs: _tabs, selectedTabIndex: index});
        }
    };

    goPrevStep = async () => {
        let index = this.state.selectedTabIndex - 1;
        if (index >= 0 && index <= 3) {
            await this.setState({selectedTabIndex: index});
        }

    };

    goToStep = (index) => {
        if (index >= 0 && index <= 3) {
            this.setState({selectedTabIndex: index});
        }

    };


    render() {

        return (
            <View style={{
                paddingTop: (Platform.OS === 'ios' ? 24 : 0),
                backgroundColor: 'rgb(245,245,245)'
            }}>
                <StatusBar color="light"/>
                <StepTabs
                    tabItems={this.state.tabs}
                    onSelectTab={index => {
                        this.setState({selectedTabIndex: index});
                    }}
                    selectedTabIndex={this.state.selectedTabIndex}
                />
                {this.state &&
                this.state.stepFormData1 &&
                this.state.selectedTabIndex === 0 &&
                <Step1 onNextStep={this.goNextStep} onPrevStep={this.goPrevStep}
                       onSaveState={async (formData) => {
                           /**
                            * When the user was successfully registered to the server,
                            *  we need to store the form data in state
                            * */
                           await this.setState({stepFormData1: formData});
                       }}
                       formData={this.state.stepFormData1}
                       mode={this.props.mode}/>
                }
                {this.state &&
                this.state.selectedTabIndex === 1 &&
                <Step2 onNextStep={this.goNextStep} onPrevStep={this.goPrevStep}
                       onSaveState={async (formData) => {
                           await this.setState({stepFormData2: formData});
                       }}
                       nurse={this.state.stepFormData1.nurse}
                       licensure_cards={this.state.stepFormData1.licensure_cards}
                       formData={this.state.stepFormData2}
                       mode={this.props.mode}
                />
                }
                {this.state &&
                this.state.selectedTabIndex === 2 &&
                <Step3 onNextStep={this.goNextStep} onPrevStep={this.goPrevStep}
                       onSaveState={async (formData) => {
                           await this.setState({stepFormData3: formData});
                       }}
                       licensure_cards={this.state.stepFormData1.licensure_cards}
                       counties={this.state.stepFormData2.counties}
                       nurse={this.state.stepFormData1.nurse}
                       formData={this.state.stepFormData3}
                       mode={this.props.mode}
                />
                }
                {this.state &&
                this.state.selectedTabIndex === 3 &&
                <Step4 onNextStep={this.goNextStep} onPrevStep={this.goPrevStep}
                       onGotoStep={this.goToStep}
                       formData1={this.state.stepFormData1}
                       formData2={this.state.stepFormData2}
                       formData3={this.state.stepFormData3}
                       formData={this.state.stepFormData4}
                       mode={this.props.mode}
                />
                }
            </View>
        );
    }
}

export default UserForm;
