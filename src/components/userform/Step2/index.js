/**
 * Component for the Signup Form
 * @WhatToDo
 * - Creates a Component for the Signup
 * - This form includes the following inputs
 *   |--------------------------------------------------------------------+
 *   |   MedNurse Enrollment Form (Step 2 - Personal Information)         |
 *   |--------------------------------------------------------------------+
 *   |   21. Select a County for Work                                     |
 *   |--------------------------------------------------------------------+
 * */

import React, {Component} from "react";
import {Alert, ScrollView, Text, TouchableOpacity, View} from "react-native";
import {Button, Card, CardSection, MultiCheckBox} from "../../shared";
import styles from "./styles";
import constants from "../../_base_data/constants";
import StateStore from "../../../store/StateStore";

class Step2 extends Component {
    _scrollView = null;


    constructor(props) {
        super(props);
    }

    componentWillMount(): void {
        this.setState(this.props.formData);
    }

    componentDidMount(): void {
        /**
         * Get the states from the nurse information once the data is save to server
         * */
        let _states = [];
        this.props.nurse &&
        this.props.nurse.nurse_state &&
        this.props.nurse.nurse_state.map(nurse_state => {
            _states.push(StateStore.abbrToName(nurse_state.state));
        });
        this.setState({states: _states});

        /**
         *Don't use this code lines when you test in production mode
         * */
        if (false && constants.APP.USE_PRE_DATA.IN_SIGNUP) {
            this.setState({counties: constants.PRE_DATA.STEP2_INFORMATION.counties});
        }


    }

    buildFormData = () => {

        let _counties = {};
        Object.keys(this.state.counties).map(county_name => {
            let _county_name = StateStore.nameToAbbr(county_name);
            _county_name &&
            !_counties[_county_name] &&
            (_counties[_county_name] = []);

            _county_name && (
                _counties[_county_name] = (this.state.counties[county_name]));
        });
        return {
            "territory": {
                "county": _counties
            }
        };
    };


    handleSaveStep2 = async () => {
        if (false && constants.APP.USE_PRE_DATA.IN_SIGNUP) {
            this.props.onNextStep && this.props.onNextStep();
        } else {
            /**
             * Start here to send request to API Server for Step - 2
             * */
            let data = this.buildFormData();



            /**
             * In case when the form works in edit mode,
             * we need to handle the process here.
             * */

            if (this.props.mode === constants.APP.USERFORM_MODE.EDIT) {
                this.props.onSaveState && await this.props.onSaveState(this.state);
                this.props.onNextStep && this.props.onNextStep();
                return;
            }


            let _url;
            this.props.nurse &&
            this.props.nurse.nurse &&
            this.props.nurse.nurse.id && (
                this.setState({inProcessing: true}) ,
                    _url = constants.APP.END_POINT.SIGNUP.STEP2.replace(/:id/gi, this.props.nurse.nurse.id),
                    fetch(_url,
                        {
                            method: "POST",
                            headers: {
                                "Content-Type": "application/json"
                            },
                            body: JSON.stringify(data)
                        }
                    ).then(response => response.json())
                        .then(async (response) => {

                            this.setState({inProcessing: false});
                            /**
                             * If there is an error on saving data to server
                             * we stop here after showing error message to the user
                             * */
                            if (response.error) {
                                Alert.alert(constants.APP.NAME, `An error occurred while saving to 
                            the serve. \n Details: ${response.error}`);
                            } else {
                                /**
                                 * If the data was successfully saved,
                                 *  - we need to _Store state data to the parent component
                                 *  - we need to send the user to the next step
                                 * */
                                this.props.onSaveState && await this.props.onSaveState(this.state);
                                this.props.onNextStep && this.props.onNextStep();
                            }
                        }).catch(error => {

                        /**
                         * If there is a problem on connecting to the server,
                         *  we stop here after showing error message to the user.
                         * */
                        Alert.alert(constants.APP.NAME, `An error occurred while saving to 
                            the server. \n Details: ${error}`);
                        this.setState({inProcessing: false});
                    })

            );

        }
    };

    selectCounties = async (key) => {
        await this.setState({
            stateKey: key,
            shownCountiesList: true,
            tmpSelectedCounties: this.state.counties[key]
        });
    };

    completeSelecting = async () => {
        let _tmpCounties = this.state.counties;
        _tmpCounties[this.state.stateKey] = this.state.tmpSelectedCounties;
        await this.setState({
            counties: _tmpCounties,
            tmpSelectedCounties: [],
            shownCountiesList: false
        });

    };


    putDemoData = async () => {

        await this.setState(constants.PRE_DATA.STEP1_INFORMATION);
        await this.setState({
            counties: {
                Arizona: ["Apache", "Cochise"],
                Wisconsin: ["Adams", "Ashland"],
            }
        });
    };

    render() {
        return (
            <View style={{marginTop: 50, backgroundColor: 'rgba(255,255,255,1)', }}>
                <TouchableOpacity
                    style={{
                        position: "absolute",
                        right: 0, top: 50,
                        backgroundColor: "rgba(0,0,0,0.05)",
                        width: 30, height: 30,
                        borderRadius: 30,
                        zIndex: 1
                    }}
                    onPress={this.putDemoData}
                />
                {this.state.shownCountiesList === true &&
                <View style={{width: "100%", height: "100%"}}>
                    <ScrollView
                        style={{marginBottom: 65}}
                    >
                        <MultiCheckBox
                            containerStyle={styles.form.multi_counties}
                            getNurseItems={() => {
                                return StateStore.allCounties(this.state.stateKey);
                            }}
                            data={StateStore.allCounties(this.state.stateKey)}
                            selectedValues={this.state.tmpSelectedCounties}
                            onSelected={async (selectedValues) => {
                                let _tmp = [];
                                let _keys = Object.keys(selectedValues);
                                _keys.map((item, index) => {
                                    selectedValues[item] && _tmp.push(item);
                                });
                                this.state.tmpSelectedCounties = _tmp;
                            }}
                        />
                    </ScrollView>
                    <View style={styles.step2_states.select_states_button.container}>
                        <Button
                            buttonStyle={[
                                styles.step2_states.select_states_button.base
                            ]}
                            textStyle={[styles.step2_states.select_states_button.text]}
                            onPress={this.completeSelecting}
                        >
                            OK
                        </Button>
                        <Button
                            buttonStyle={[
                                styles.step2_states.select_states_button.base
                            ]}
                            textStyle={[styles.step2_states.select_states_button.text]}
                            onPress={() => this.setState({shownCountiesList: false})}
                        >
                            Cancel
                        </Button>
                    </View>
                </View>
                }

                {this.state.shownCountiesList === false &&
                <ScrollView
                    ref={view => (this._scrollView = view)}
                >
                    <Card style={{elevation: 0, backgroundColor: "transparent"}}>
                        <CardSection style={styles.form.desc.base}>
                            <Text style={[styles.form.base, styles.form.desc.content]}>
                                Please select the county in which you wish to
                                provide services. You may select more than one county.
                            </Text>
                        </CardSection>
                        <CardSection style={{flex: 1}}>
                            <View style={{flex: 1}}>
                                {this.state.states &&
                                this.state.states.map((item, index) => {
                                    return <TouchableOpacity
                                        key={`key_${index}`} style={styles.step2_states.base}
                                        onPress={this.selectCounties.bind(this, item)}>
                                        <View style={styles.step2_states.title}>
                                            <Text>
                                                State {index + 1}: {item}
                                            </Text>
                                            <Text style={styles.step2_states.right_arrow}> > </Text>
                                        </View>
                                        {this.state.counties[item] &&
                                        this.state.counties[item].length > 0 &&
                                        <View style={{
                                            paddingHorizontal: 5,
                                            paddingBottom: 5,
                                            flexDirection: "row",
                                            flexWrap: "wrap"
                                        }}>
                                            <Text>
                                                {this.state.counties[item].join(", ")}
                                            </Text>
                                        </View>}
                                    </TouchableOpacity>;
                                })}
                            </View>

                        </CardSection>
                        <CardSection style={{marginBottom: 10, }}>
                            <Button
                                buttonStyle={[
                                    styles.nav_button
                                ]}
                                textStyle={[styles.nav_button_text]}
                                onPress={this.props.onPrevStep && this.props.onPrevStep.bind(this)}
                            >
                                Previous
                            </Button>
                            <Button
                                disabled={this.state.inProcessing}
                                loading={this.state.inProcessing}
                                buttonStyle={[
                                    styles.nav_button
                                ]}
                                textStyle={[styles.nav_button_text]}
                                onPress={this.handleSaveStep2}
                            >
                                Next
                            </Button>
                            <Button
                                disabled={true}
                                buttonStyle={[
                                    styles.nav_button,
                                    styles.disabled
                                ]}
                                textStyle={[styles.nav_button_text]}
                            >
                                Finish
                            </Button>
                        </CardSection>
                        {/**/}
                    </Card>
                </ScrollView>
                }
            </View>
        );
    }
}

export default Step2;
