/**
 * Component for the Signup Form
 * @WhatToDo
 * - Creates a Component for the Signup
 * - This form includes the following inputs
 *   |--------------------------------------------------------------------+
 *   |   MedNurse Enrollment Form (Step 3 - Personal Information)         |
 *   |--------------------------------------------------------------------+
 *   |   22. Select zipcodes for the selected counties                    |
 *   |--------------------------------------------------------------------+
 * */

import React, {Component} from 'react';
import {Alert, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Button, Card, CardSection} from '../../shared';
import styles from './styles';
import constants from '../../_base_data/constants';
import GeoCoderStore from '../../../store/GeoCoderStore';
import CustomMapView from '../_CustomViews/CustomMapView';

class Step3 extends Component {
    _scrollView = null;

    constructor(props) {
        super(props);
    }

    componentWillMount(): void {
        this.setState({shownMap: false});
    }

    componentDidMount(): void {
        this.applyStatesAndCounties();
    }

    /**
     * Get states from licensure_cards
     * */
    applyStatesAndCounties = async () => {

        let _tmp = [];
        this.props.licensure_cards.map((item) => {
            _tmp.push(item.state);
        });

        await this.setState(this.props.formData);
        await this.setState({states: _tmp, counties: this.props.counties});

    };

    buildFormData = () => {
        let _zipcodes = {};
        (Object.keys(this.state.zipcodes) || []).map(countyName => {
            _zipcodes[countyName] = GeoCoderStore.getZipCodesArrayFromZipCodeObjects(this.state.zipcodes[countyName]);
        });
        return {
            zip: _zipcodes
        };
    };

    handleSaveStep3 = async () => {

        //this.storeCountiesData();

        if (constants.APP.USE_PRE_DATA.IN_SIGNUP) {
            this.props.onNextStep && this.props.onNextStep();
        } else {
            let data = this.buildFormData();


            /**
             * In case when the form works in edit mode,
             * we need to handle the process for edit mode.
             * */
            if (this.props.mode === constants.APP.USERFORM_MODE.EDIT) {
                this.props.onSaveState && await this.props.onSaveState(this.state);
                this.props.onNextStep && this.props.onNextStep();
            }

            /**
             * Start to send request to Server API for Step 3
             * */
            let _url;
            this.props.nurse &&
            this.props.nurse.nurse &&
            this.props.nurse.nurse.id && (
                this.setState({inProcessing: true}) ,
                    _url = constants.APP.END_POINT.SIGNUP.STEP3.replace(/:id/gi, this.props.nurse.nurse.id),
                    fetch(_url,
                        {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify(data)
                        }
                    ).then(response => response.json())
                        .then(async (response) => {

                            this.setState({inProcessing: false});
                            /**
                             * If there is an error on saving data to server
                             * we stop here after showing error message to the user
                             * */
                            if (response.error) {
                                Alert.alert(constants.APP.NAME, `An error occurred while saving to the serve. \n Details: ${response.error}`);
                                this.props.onNextStep && this.props.onNextStep();
                            } else {
                                /**
                                 * If the data was successfully saved,
                                 *  - we need to _Store state data to the parent component
                                 *  - we need to send the user to the next step
                                 * */
                                this.props.onSaveState && await this.props.onSaveState(this.state);
                                this.props.onNextStep && this.props.onNextStep();
                            }
                        }).catch(error => {

                        this.setState({inProcessing: false});
                    })
            );

        }
    };

    selectZipCodes = async (selectedState, selectedCounty) => {
        await this.setState({
            selectedCounty: selectedCounty,
            selectedState: selectedState,
            tmpSelectedZipCodes: this.state.zipcodes[selectedCounty],
            shownMap: true
        });
    };

    completeCustomMapViewSelecting = async () => {
        let _tmpZipCodes = this.state.zipcodes;
        _tmpZipCodes[this.state.selectedCounty] = this.state.tmpSelectedZipCodes;

        this.setState({
            zipcodes: _tmpZipCodes,
            shownMap: false
        });
    };

    render() {
        return (
            <View style={{width: '100%', height: '100%'}}>
                {this.state.shownMap === true &&
                <View style={{width: '100%'}}>
                    <View
                        style={{
                            marginTop: 40, marginBottom: 47,
                            paddingHorizontal: 0
                        }}
                    >
                        {this.state.selectedCounty &&
                        <CustomMapView
                            style={[styles.map, {backgroundColor: 'red', width: '100%', height: '100%'}]}
                            zipcodes={this.state.zipcodes[this.state.selectedCounty]}
                            county={this.state.selectedCounty}
                            state={this.state.selectedState}
                            onSelected={async (markers) => {
                                let _tmp = [];
                                let _keys = Object.keys(markers);

                                _keys.map((item, index) => {
                                    markers[item] && _tmp.push(markers[item]);
                                });
                                this.setState({tmpSelectedZipCodes: _tmp});
                            }}
                        />
                        }
                    </View>

                    <View style={styles.step2_states.select_states_button.container}>
                        <Button
                            buttonStyle={[
                                styles.step2_states.select_states_button.base
                            ]}
                            textStyle={[styles.step2_states.select_states_button.text]}
                            onPress={this.completeCustomMapViewSelecting}
                        >
                            OK
                        </Button>
                        <Button
                            buttonStyle={[
                                styles.step2_states.select_states_button.base
                            ]}
                            textStyle={[styles.step2_states.select_states_button.text]}
                            onPress={() => this.setState({shownMap: false})}
                        >
                            Cancel
                        </Button>
                    </View>
                </View>
                }

                {this.state.shownMap === false &&
                <ScrollView
                    style={{marginTop: 40, backgroundColor: 'rgba(255,255,255,1)',}}
                    ref={view => (this._scrollView = view)}
                >
                    <Card>
                        <CardSection style={styles.form.desc.base}>
                            <Text style={[styles.form.base, styles.form.desc.content]}>
                                Please select the zip codes in which you wish to
                                provide services. You may select more than one zip code.
                            </Text>
                        </CardSection>
                        <CardSection>
                            <View style={{flex: 1}}>
                                {this.state.states &&
                                this.state.states.map((item, index) => {
                                    return (
                                        <View key={`view_${index}`}>
                                            {this.state.counties[item] &&
                                            this.state.counties[item].map((item1, index) => {
                                                return (
                                                    <TouchableOpacity key={`key_${index}`}
                                                                      style={styles.step2_states.base}
                                                                      onPress={this.selectZipCodes.bind(this, item, item1)}>
                                                        <View key={`key_${index}`}
                                                              style={styles.step2_states.selected_counties}>
                                                            <Text>
                                                                State : {item}
                                                            </Text>
                                                            <Text>
                                                                County : {item1}
                                                            </Text>
                                                            <View style={{
                                                                flexDirection: 'row',
                                                                flexWrap: 'wrap'
                                                            }}>

                                                                {this.state.zipcodes[item1] &&
                                                                <Text>
                                                                    {
                                                                        this.state.zipcodes[item1]
                                                                            .map(z => z.zipcode).join(', ')
                                                                    }
                                                                </Text>
                                                                }

                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                );
                                            })}
                                        </View>
                                    );
                                })}
                            </View>

                        </CardSection>
                        <CardSection style={{marginBottom: 10, }}>
                            <Button
                                buttonStyle={[
                                    styles.nav_button
                                ]}
                                textStyle={[styles.nav_button_text]}
                                onPress={this.props.onPrevStep && this.props.onPrevStep.bind(this)}
                            >
                                Previous
                            </Button>
                            <Button
                                buttonStyle={[
                                    styles.nav_button
                                ]}
                                disabled={this.state.inProcessing}
                                loading={this.state.inProcessing}
                                textStyle={[styles.nav_button_text]}
                                onPress={this.handleSaveStep3}
                            >
                                Next
                            </Button>
                            <Button
                                disabled={true}
                                buttonStyle={[
                                    styles.nav_button,
                                    styles.disabled
                                ]}
                                textStyle={[styles.nav_button_text]}
                            >
                                Finish
                            </Button>
                        </CardSection>
                    </Card>
                </ScrollView>
                }
            </View>
        );
    }
}

export default Step3;
const styles_map = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});
