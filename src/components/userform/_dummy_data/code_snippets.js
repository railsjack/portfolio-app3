let require = () => {
    return {default: ""}
};
let constants = {
    APP: {
        NAME: "AllLink",
        USE_PRE_DATA: true,
        END_POINT: {
            SIGNUP: {
                STEP1: "http://mobilemednurse.com/api/v1/nurses",
                STEP2: "http://mobilemednurse.com/api/v1/nurses/:id/step_2_a_state_selection"
            }
        },
        STORAGE_KEY: {
            COUNTIES: "@AllLink_opbJZ5kBI1"
        },
    },

    GOOGLE_MAP: {
        DEBUG_MODE: true,
        USE_PRE_DATA: false,
        API_KEY: "AIzaSyCS_1TAevxCErr7XLQPtt8jSSBfoXzLi70",
        GEOCODE_URL: {
            GET_BY_LATLNG: "https://maps.googleapis.com/maps/api/geocode/json?address=LAT,LNG&key=API_KEY",
            GET_BY_ZIPCODE: "https://maps.googleapis.com/maps/api/geocode/json?address=ZIPCODE+usa&sensor=false&key=API_KEY"
        },
        ADDRESS_COMPONENT: require("../../../_tmp_logs/MedNurse-GeoSearchByLatLng")
    },

    GENDERS: [
        {key: "male", label: "Male"},
        {key: "female", label: "Female"}
    ],

    PRE_DATA: {
        STEP1_INFORMATION: require("../_dummy_data/information_for_step1").default,
        STEP2_INFORMATION: require("../_dummy_data/information_for_step2").default,
        RESPONSE_WHEN_STEP1_SAVED: require("../_dummy_data/response_when_step1_saved").default,
        RESPONSE_WHEN_STEP2_SAVED: require("../_dummy_data/response_when_step2_saved").default,
    }
};

/**
 * Handles all method related Google Map and GeoCoder
 * */
class GeoCoder {
    constructor() {

    }


    /**
     * Get EndPoint URL corresponding to keys
     *  @params:
     *    key:
     *      It determines which to get end point
     *      GET_ZIP_CODE
     *        get zip code by lat,lng
     *      GET_LOCATION
     *        get location(lat,lng) by zip code
     *    ...args
     *      variant arguments
     *      It differs depending on keys
     *  @return
     *    it differs depending on keys
     *    in case key === "GET_ZIP_CODE"
     *      it returns endpoint URL for getting zip code by location
     *    in case key === "GET_LOCATION"
     *      it returns endpoint URL for getting location by zip code
     * */
    getAPIEndpointFor = (key, ...args) => {
        let _end_point, _arg, ret;

        /**
         * In case you need to get zip code by lat,lng
         * */
        ret = ((key === "GET_ZIP_CODE" &&
            (_arg = args[0]) &&
            (typeof _arg.latitude !== "undefined" && typeof _arg.longitude !== "undefined")) &&
            (_end_point = constants.GOOGLE_MAP.GEOCODE_URL.GET_BY_LATLNG ,
                _end_point = _end_point.replace(/API_KEY/g, constants.GOOGLE_MAP.API_KEY) ,
                _end_point = _end_point.replace(/LAT/g, _arg.latitude) ,
                _end_point.replace(/LNG/g, _arg.longitude)));

        /**
         * In case you need to get the location by zip code
         * */
        ret = (key === "GET_LOCATION" &&
            (_arg = args[0]) &&
            (typeof _arg !== "undefined") &&
            (_end_point = constants.GOOGLE_MAP.GEOCODE_URL.GET_BY_ZIPCODE ,
                _end_point = _end_point.replace(/API_KEY/g, constants.GOOGLE_MAP.API_KEY) ,
                _end_point.replace(/ZIPCODE/g, _arg)));

        return ret;
    };

    /**
     * Get the current location based on Mobile's GPS
     * */
    getCurrentLocation = () => {
        return new Promise((resolve, reject) => {
            navigator.geolocation
                .getCurrentPosition(
                    location => resolve(location),
                    error => reject(error),
                    {enableHighAccuracy: true}
                );
        });
    };


    /**
     * Get the Zip code from coordinates
     * */
    getZipCodeByCoordinate = async (coordinate) => {
        /**
         * we need to get the address component by coordinate from Google
         * */
        return new Promise((resolve, reject) => {
            let _end_point = this.getAPIEndpointFor("GET_ZIP_CODE", coordinate);
            fetch(_end_point)
                .then((response) => response.json())
                .then(addrComponent => {
                    let _zipcode = this.getZipCodeFromAddressComponent (addrComponent)
                    resolve(_zipcode)
                })
                .catch(error => reject(error));
        });
    };



    /**
     * Get Location from the Address Component of Google GeoCoder Response
     * @param
     *   response
     * @return
     *   return the location (latitude, longitude)
     * */
    getLocationFromAddressComponent = (response) => {
        return (response.status === "OK" &&
            response.results &&
            response.results[0] &&
            response.results[0].geometry &&
            response.results[0].geometry.location &&
            response.results[0].geometry.location.lat &&
            response.results[0].geometry.location.lng &&
            {
                latitude: response.results[0].geometry.location.lat,
                longitude: response.results[0].geometry.location.lng
            }) ||
            {latitude: 0, longitude: 0};

    };



    /**
     * Get Zip Code from the Address Component of Google GeoCoder Response
     * @param
     *   response
     * @return
     *   return the zip code
     * @note
     * */
    getZipCodeFromAddressComponent = (response) => {
        let components;
        return (response.status === "OK" &&
            response.results &&
            response.results[0] &&
            response.results[0].address_components &&
            (components = response.results[0].address_components.filter((addr_component) => {
                return addr_component &&
                    addr_component.types &&
                    // Google Map sends postal_code instead of zipcode
                    addr_component.types.indexOf("postal_code") > -1 &&
                    (addr_component.long_name);
            })) &&
            components.length > 0 &&
            components[0].long_name) || "";
    };

    /**
     * Get the location by Zip Code
     * @param: Zip code
     * @return:
     *   Promise for getting Location by Zip Code
     * */
    getLocationByZipCode = (zipcode) => {
        return new Promise((resolve, reject) => {
            let _end_point = this.getAPIEndpointFor("GET_LOCATION", zipcode);
            fetch(_end_point)
                .then((response) => response.json())
                .then(response => resolve(response))
                .catch(error => reject(error));

        });
    };

}

let geo = new GeoCoder();
geo.getAPIEndpointFor("GET_LOCATION", "94115")
