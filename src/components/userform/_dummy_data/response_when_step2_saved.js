export default {
    "data": {
        "nurse": {
            "id": 11,
            "first_name": "Munib",
            "last_name": "Rehman",
            "gender": "male",
            "dob": "1994-01-09",
            "designation": null,
            "mobile_number": "(612) 465-4564",
            "phone_number": "(169) 174-9219",
            "license_no": null,
            "licensure_state": null,
            "address1": "76 North Clarendon Extension",
            "address2": "Nihil qui dolore exc",
            "city": "Eos aut est maiores ",
            "county": "Dunn",
            "state": "WI",
            "zip": "74707",
            "personal_insights": null,
            "user_id": 14,
            "created_at": "2019-04-10T05:23:27.000Z",
            "updated_at": "2019-04-10T05:23:27.000Z",
            "territory": null,
            "photo_file_name": null,
            "photo_content_type": null,
            "photo_file_size": null,
            "photo_updated_at": null,
            "resume_file_name": null,
            "resume_content_type": null,
            "resume_file_size": null,
            "resume_updated_at": null,
            "latitude": null,
            "longitude": null,
            "bio": "Fugit mollit volupt"
        },
        "nurse_state": [
            {
                "id": 57,
                "nurse_id": 11,
                "state": "CA",
                "license_no": "456",
                "designation": 4,
                "county": null,
                "created_at": "2019-04-10T05:23:27.000Z",
                "updated_at": "2019-04-10T05:23:27.000Z"
            },
            {
                "id": 58,
                "nurse_id": 11,
                "state": "KY",
                "license_no": "46",
                "designation": 5,
                "county": null,
                "created_at": "2019-04-10T05:23:27.000Z",
                "updated_at": "2019-04-10T05:23:27.000Z"
            }
        ],
        "counties": [
            {
                "id": 76,
                "nurse_state_id": 57,
                "county": "Alameda",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 77,
                "nurse_state_id": 57,
                "county": "Alpine",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 78,
                "nurse_state_id": 58,
                "county": "Adair",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 79,
                "nurse_state_id": 58,
                "county": "Allen",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 92,
                "nurse_state_id": 57,
                "county": "Alameda",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 93,
                "nurse_state_id": 57,
                "county": "Alpine",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 94,
                "nurse_state_id": 58,
                "county": "Adair",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 95,
                "nurse_state_id": 58,
                "county": "Allen",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 96,
                "nurse_state_id": 57,
                "county": "Alameda",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 97,
                "nurse_state_id": 57,
                "county": "Alpine",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 98,
                "nurse_state_id": 58,
                "county": "Adair",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 99,
                "nurse_state_id": 58,
                "county": "Allen",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 100,
                "nurse_state_id": 57,
                "county": "Alameda",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 101,
                "nurse_state_id": 57,
                "county": "Alpine",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 102,
                "nurse_state_id": 58,
                "county": "Adair",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 103,
                "nurse_state_id": 58,
                "county": "Allen",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 104,
                "nurse_state_id": 57,
                "county": "Alameda",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 105,
                "nurse_state_id": 57,
                "county": "Alpine",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 106,
                "nurse_state_id": 58,
                "county": "Adair",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 107,
                "nurse_state_id": 58,
                "county": "Allen",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 112,
                "nurse_state_id": 57,
                "county": "Alameda",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 113,
                "nurse_state_id": 57,
                "county": "Alpine",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 114,
                "nurse_state_id": 58,
                "county": "Adair",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 115,
                "nurse_state_id": 58,
                "county": "Allen",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 116,
                "nurse_state_id": 57,
                "county": "Alameda",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 117,
                "nurse_state_id": 57,
                "county": "Alpine",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 118,
                "nurse_state_id": 58,
                "county": "Adair",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 119,
                "nurse_state_id": 58,
                "county": "Allen",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 120,
                "nurse_state_id": 57,
                "county": "Alameda",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 121,
                "nurse_state_id": 57,
                "county": "Alpine",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 122,
                "nurse_state_id": 58,
                "county": "Adair",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 123,
                "nurse_state_id": 58,
                "county": "Allen",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 124,
                "nurse_state_id": 57,
                "county": "Alameda",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 125,
                "nurse_state_id": 57,
                "county": "Alpine",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 126,
                "nurse_state_id": 58,
                "county": "Adair",
                "territory": null,
                "nurse_id": 11
            },
            {
                "id": 127,
                "nurse_state_id": 58,
                "county": "Allen",
                "territory": null,
                "nurse_id": 11
            }
        ]
    }
};
