export default {
    'data': {
        'nurse': {
            'id': 1,
            'first_name': 'Munib',
            'last_name': 'Rehman',
            'gender': 'male',
            'dob': '1994-01-09',
            'mobile_number': '(332) 699-6744',
            'phone_number': '(483) 700-3122',
            'designation': {
                'AZ': 1,
                'WI': 2
            },
            'license_no': {
                'AZ': '111',
                'WI': '222'
            },
            'licensure_state': [
                'AZ', 'WI'
            ],
            'counties': {
                Arizona: ['Apache', 'Cochise'],
                Wisconsin: ['Adams', 'Ashland']
            },
            'zipcodes': {
                Apache:     {zipcode: ['12345', '12346']},
                Cochise:    {zipcode: ['22345', '22346']},
                Adams:      {zipcode: ['32345', '32346']},
                Ashland:    {zipcode: ['42345', '42346']}
            },
            'address1': 'G3 Lahore',
            'address2': '',
            'city': 'Clearwater',
            'county': 'Clay',
            'state': 'FL',
            'zip': '50100',
            'user_id': 2,
            'created_at': '2019-04-08T12:02:06.000Z',
            'updated_at': '2019-04-08T12:02:06.000Z',
            'photo_file_name': null,
            'photo_content_type': null,
            'photo_file_size': null,
            'photo_updated_at': null,
            'resume_file_name': null,
            'resume_content_type': null,
            'resume_file_size': null,
            'resume_updated_at': null,
            'latitude': null,
            'longitude': null,
            'bio': 'Test Bio'
        },
        'user': {
            'email': 'munib.averlentcorp@nxvt.com',
            'authentication_token': 'tBMzxn27xKcuHnoF3gCB'
        }
    }
};
