export default {
    "nurse": {
        "first_name": "Christian Munib",
        "last_name": "Benton",
        "gender": "male",
        "dob": "09/01/1994",
        "address1": "76 North Clarendon Extension",
        "address2": "Nihil qui dolore exc",
        "state": "WI",
        "county": "Dunn",
        "city": "Eos aut est maiores ",
        "zip": "74707",
        "mobile_number": "6124654564",
        "phone_number": "(169) 174-9219",
        "bio": "Fugit mollit volupt",
        "licensure_state": {
            "1": "FL",
            "2": "GA"
        },
        "designation": {
            "1": "4",
            "2": "5"
        },
        "license_no": {
            "1": "456",
            "2": "46"
        }
    },
    "user": {
        "email": "jinnahrae11@gmail.com",
        "password": "[FILTERED]",
        "password_confirmation": "[FILTERED]"
    }
};
