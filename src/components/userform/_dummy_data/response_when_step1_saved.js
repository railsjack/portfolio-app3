export default {
    "nurse": {
        "nurse": {
            "id": 33,
            "first_name": "Christian Munib",
            "last_name": "Benton",
            "gender": "male",
            "dob": "1994-01-09",
            "designation": null,
            "mobile_number": "6124654564",
            "phone_number": "(169) 174-9219",
            "license_no": null,
            "licensure_state": null,
            "address1": "76 North Clarendon Extension",
            "address2": "Nihil qui dolore exc",
            "city": "Eos aut est maiores ",
            "county": "Dunn",
            "state": "WI",
            "zip": "74707",
            "personal_insights": null,
            "user_id": 35,
            "created_at": "2019-04-16T07:58:47.000Z",
            "updated_at": "2019-04-16T07:58:47.000Z",
            "territory": null,
            "photo_file_name": null,
            "photo_content_type": null,
            "photo_file_size": null,
            "photo_updated_at": null,
            "resume_file_name": null,
            "resume_content_type": null,
            "resume_file_size": null,
            "resume_updated_at": null,
            "latitude": null,
            "longitude": null,
            "bio": "Fugit mollit volupt"
        },
        "nurse_state": [
            {
                "id": 96,
                "nurse_id": 33,
                "state": "FL",
                "license_no": "456",
                "designation": 4,
                "county": null,
                "created_at": "2019-04-16T07:58:47.000Z",
                "updated_at": "2019-04-16T07:58:47.000Z"
            },
            {
                "id": 97,
                "nurse_id": 33,
                "state": "GA",
                "license_no": "46",
                "designation": 5,
                "county": null,
                "created_at": "2019-04-16T07:58:47.000Z",
                "updated_at": "2019-04-16T07:58:47.000Z"
            }
        ]
    }
};
