import theme from "../../../../theme/default";

const styles = {
    container: {
        width: "100%",
        paddingHorizontal: 0,
        paddingVertical: 0,
        marginTop: 0,
    },
    button: {
    },
    text: {
        lineHeight: theme.lineHeightMedium
    },
    label: {}
};

export default styles;
