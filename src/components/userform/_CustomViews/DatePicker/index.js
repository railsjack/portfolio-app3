import React, {Component} from "react";

import {Text, TouchableOpacity, View} from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import styles from "./styles";

class DatePicker extends Component {
    state = {
        isDateTimePickerVisible: false
    };

    constructor(props) {
        super(props);
        this.props = props;
        this.setDate = this.setDate.bind(this);
    }

    componentWillMount(): void {
        let current_date = +new Date();
        this.setState({
            format: this.props.format || "Y/M/D",
            currentDate: current_date,
            minDate: this.props.minDate || current_date - 31536000000 * 60,
            maxDate: this.props.maxDate || current_date - 31536000000 * 17,
        });
    }

    setDate(newDate) {
        this.props.onDateChange && this.props.onDateChange(newDate);
    }

    showDateTimePicker = () => {
        this.setState({isDateTimePickerVisible: true});
    };

    hideDateTimePicker = () => {
        this.setState({isDateTimePickerVisible: false});
    };

    handleDatePicked = async date => {
        let _date = this.formatDate(date);
        await this.setState({date: _date});
        await this.setDate((_date));
        this.hideDateTimePicker();
    };

    /**
     * Format Date String
     * */
    formatDate = date => {
        let d = typeof date === "object" ? date : new Date(date),
            month = "" + (d.getMonth() + 1),
            day = "" + d.getDate(),
            year = d.getFullYear();


        let ret_date = this.state.format;
        ret_date = ret_date.replace(/Y/gi, year);
        ret_date = ret_date.replace(/M/gi, month);
        ret_date = ret_date.replace(/D/gi, day);

        return ret_date;
    };

    /**
     * Get ISO Date String from date string
     * */
    getISODate = date => {
        let _ymd = this.state.format.split(/\//gi).reduce(function (result, item, i) {
            result[item] = i; //Y, M, D
            return result;
        }, {});
        let _date = date.split(/\//gi);
        return `${_date[_ymd["Y"]]}/${_date[_ymd["M"]]}/${_date[_ymd["D"]]}`;
    };

    render() {
        const {
            currentDate,
        } = this.state;

        const {...props} = this.props;

        return (
            <View style={[styles.container, props.containerStyle]}>
                {this.state.date &&
                    <Text
                        style={[styles.label, props.smallLabelStyle]}
                    >{props.label || " "}</Text>
                }
                <TouchableOpacity
                    style={[styles.button, props.buttonStyle]} onPress={() => this.showDateTimePicker()}>
                    <Text style={[styles.text, props.textStyle]}>
                        {this.state.date || props.date || props.label}
                    </Text>
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    date={new Date(this.state.date || props.date || currentDate)}
                    minimumDate={new Date(this.state.minDate)}
                    maximumDate={new Date(this.state.maxDate)}
                />
            </View>
        );
    }


}

export {DatePicker};
