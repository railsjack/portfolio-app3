/**
 * Tab bar styles in Signup Page
 *
 * */
import theme from "../../../../theme/default";

const TABBAR = {
  HEIGHT: 40,
  FONT_WEIGHT: theme.fontWeightBold,
  FONT_SIZE: theme.fontSizeSmall,
  FONT_COLOR: theme.defaultTextColor,
  FONT_FAMILY: theme.fontFamily,
  SELECTED_FONT_COLOR: theme.inverseTextColor,
  SELECTED_BG_COLOR: theme.defaultBgColor,
};

const styles = {
  steptab: {
    flex: 1, flexDirection: "row",
  },
  item: {
    width: "25%",
  },
  text: {
    base: {
      textAlign: "center",
      fontWeight: TABBAR.FONT_WEIGHT,
      fontSize: TABBAR.FONT_SIZE,
      color: TABBAR.FONT_COLOR,
      fontFamily: TABBAR.FONT_FAMILY,
      height: TABBAR.HEIGHT,
      lineHeight: TABBAR.HEIGHT,
    },
    selected: {
      color: TABBAR.SELECTED_FONT_COLOR,
      backgroundColor: TABBAR.SELECTED_BG_COLOR
    },
  }
};

export default styles;
