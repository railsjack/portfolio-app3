/**
 * Tabs control for 4 steps
 * @WhatToDo
 *   renders a Tab UI including Index, Index, Index and Index
 * @Structure
 *   View
 *     TouchableOpacity
 *       Text
 *         Step 1
 *       Text
 *         Step 2
 *       Text
 *         Step 3
 *       Text
 *         Step 4
 * */

import React, {Component} from "react";
import {Text, TouchableOpacity, View} from "react-native";
import styles from "./styles";

class StepTabs extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.steptab}>
        {this.props.tabItems.map((item, index) => {
          return (
            <TouchableOpacity
              key={"key_" + index}
              style={[styles.item]}
              onPress={
                this.props.onSelectTab &&
                this.props.onSelectTab.bind(this, index)
              }
              disabled={!this.props.tabItems[index].passed}
            >
              <Text
                style={[
                  styles.text.base,
                  index === this.props.selectedTabIndex && styles.text.selected
                ]}
              >
                {item.name}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  }
}

export {StepTabs};

