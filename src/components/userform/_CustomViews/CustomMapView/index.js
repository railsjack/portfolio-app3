/**
 * Custom Map for handling Zip Code and Location
 * */

import React, {Component} from 'react';
import {ActivityIndicator, Alert, Image, Text, TouchableOpacity, View} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import constants from '../../../_base_data/constants';
import srcMarkerIcon from '../../../../assets/images/marker-icon.png';
import GeoCoder from '../../../../store/GeoCoderStore';
import MarkerArray from '../../../../store/MarkerArrayStore';
import TimerMixin from 'react-timer-mixin';
import styles from './styles';

mixins: [TimerMixin];

/**
 * Customize MapView Class of react-native-maps
 * */
class CustomMapView extends Component {
    state = {
        /**
         * There is an opacity button in the right bottom of the Map.
         * This is for developers to debug the Map while developing
         * If the developer clicks this button, it will actionSheet a output area
         * for debug
         * */
        debugShow: false,
        debugInfo: '',
        debugWarn: '',
        debugError: '',
        /**
         * Debug information for coordinate and position
         * When the user clicks a Map, these variables will be set.
         * */
        coordinate: undefined,
        position: undefined,

        /**
         * Variable to _Store information of markers.
         * When the user clicks on a Map, it will create a Marker,
         * and it will be pushed to this variable
         * */
        markers: [],

        /**
         * Variable to determine whether to actionSheet a top information bar or not.
         * When a marker is removed, an information bar will be shown at the top.
         * It says like this.  "Zip code xxxxx removed"
         * */
        topInfoBar: false,
        /**
         * A variable to _Store a zipcode just removed from the Map
         * */
        removedZipCode: '',

        /**
         * Determines whether to actionSheet Loading icon or not.
         * When the user clicks on the Map, it will actionSheet a loading icon
         * As soon as it gets the response from the server, it will be disappeared.
         * */
        inProcessing: false,

        /**
         * Determines whether to actionSheet the Map or not.
         * At the first time when loaded a screen, it will not actionSheet a Map.
         * It will actionSheet a Map just after got the current location of the user.
         * */
        mapShown: false,

        /**
         * Initial region of the Map.
         * If we don't initialize this variables,
         *   then the Map will be crashed when it loads at first time.
         * I believe that there is no valid city or county in the location of (lat:0, lng:0)
         * */
        region: {
            latitude: 0, //37.78825,
            longitude: 0, //-122.4324,
            latitudeDelta: 0.0922 * 2 * 2,
            longitudeDelta: 0.0421 * 2 * 2,
        },

        /**
         * Stores the location when the user clicks on the Map
         * This variable is used as temp location to fir to cooridnates.
         * */

        clickedLocation: {
            latitude: 0,
            longitude: 0
        },
    };

    constructor(props) {
        super(props);
        this._isMounted = false;
    }


    /**
     * Shows Loading Indicator
     * */

    showLoading = () => this._isMounted && this.setState({inProcessing: true});

    /**
     * Hides Loading Indicator
     * */

    hideLoading = () => this._isMounted && this.setState({inProcessing: false});

    /**
     * As soon as mounted the Component,
     *  it will calls the method to get current the location
     * */
    componentDidMount(): void {

        // If you have already selected some zip codes on the Map
        if (this.props.zipcodes && this.props.zipcodes.length > 0) {
            /// zipcodes === markers
            this.addMarkersBulk(this.props.zipcodes)
                .then(result => {
                    this._isMounted = true;
                    MarkerArray.set(this.state.markers);
                    let _coordinates = MarkerArray.getCoordinates();
                    setTimeout(() => {
                        this._mapView.fitToCoordinates(_coordinates, {top: 10, left: 10, bottom: 10, right: 10});
                    }, 1000);

                })
                .catch(error => {
                    this._isMounted = true;
                    this.showMapWithCurrentLocation()
                        .then(result => this._isMounted = true)
                        .catch(error => this._isMounted = true);
                });
        } else {
            this.showLoading();

            GeoCoder.getLocationByAddress([this.props.county, this.props.state].join('+'))
                .then(location => {
                    this.hideLoading();
                    this._isMounted = true;
                    this.setMapRegion(location);
                })
                .catch(error => {
                    this.showMapWithCurrentLocation()
                        .then(result => this._isMounted = true)
                        .catch(error => this._isMounted = true);
                });
        }
    }

    onMapReady = () => {
    };

    componentWillUnmount() {
        this._isMounted = false;
    }


    /**
     * Set the region of the given Map
     * */
    setMapRegion = (location) => {
        let _region = this.state.region;
        this._isMounted && this.setState({
            region: {
                latitude: location.latitude,
                longitude: location.longitude,
                latitudeDelta: _region.latitudeDelta,
                longitudeDelta: _region.longitudeDelta,
            },
            mapShown: true
        });
    };

    /**
     * It's called when the user clicks a point on the Map
     * @WhatToDo
     *  - get coordinate(lat, lng) and position(x,y) of the map
     *  - fetch json from google map with the lat & lng
     *  - parse to get the necessary information including zip code and the latlng related to
     *  - create a maker on the map
     *    (need to _Store zipcode: latlng on mobile local storage as temp data)
     * */
    onPressMapView = async (e) => {

        this.showLoading();
        if (constants.GOOGLE_MAP.USE_PRE_DATA) {

            let addressComponent = constants.GOOGLE_MAP.ADDRESS_COMPONENT;
            let _location = GeoCoder.getLocationFromAddressComponent(addressComponent);
            let _zipcode = GeoCoder.getZipCodeFromAddressComponent(addressComponent);

            this.addOrRemoveMarkerTo(_zipcode, _location);

        } else {
            const {coordinate} = e.nativeEvent;

            this._isMounted && this.setState({clickedLocation: coordinate});


            /**
             * When the user clicks on the Map, we can get coordinate from it.
             * coordinate includes lat and lng
             * We need to get the Zip Code from Coordinate
             * */
            GeoCoder.getZipCodeByCoordinate(coordinate)
                .then(zipcode => {


                    /**
                     * Then we need to get the location by Zip Code
                     * */
                    GeoCoder.getLocationByZipCode(zipcode)
                        .then(location => {

                            /**
                             * Then we need to add/remove marker to the location given.
                             * */
                            this.addOrRemoveMarkerTo(zipcode, location);
                        })
                        .catch((error) => {
                            this._isMounted && this.setState({
                                debugError: JSON.stringify(error)
                            });
                            this.hideLoading();
                        });

                })
                .catch(error => {
                    this._isMounted && this.setState({
                        debugError: JSON.stringify(error)
                    });
                    this.hideLoading();
                });

        }
    };

    /**
     * Add Or Marker (with a given zipcode) to the location given.
     * In case there is already a same zipcode in the given location,
     *   it removes, otherwise it adds.
     * @params
     *   zipcode
     * @return
     *   none
     * */
    addOrRemoveMarkerTo = async (zipcode, location) => {

        let _marker = {
            coordinate: location,
            zipcode: zipcode
        };

        // We need to save non-redundant data into cache
        GeoCoder.addMarkerInfoToCache(_marker);

        _marker.title = zipcode;
        _marker.description = `Added zip code ${zipcode}`;

        MarkerArray.set(this.state.markers);
        if (MarkerArray.indexOf(_marker) === -1) {
            MarkerArray.add(_marker);
            this._isMounted && await this.setState({
                markers: MarkerArray.get(),
                topInfoBar: true,
                markerLogMessage: `Zip code "${_marker.zipcode}" added. You can click it to remove.`
            });

            this.props.onSelected && this.props.onSelected(this.state.markers);

            let _coordinates = MarkerArray.getCoordinates();
            _coordinates.length === 1 && _coordinates.push(this.state.clickedLocation);
            this._mapView.fitToCoordinates(_coordinates, {top: 10, left: 10, bottom: 10, right: 10});

        } else {

            let _message = `Zip code "${_marker.zipcode}" already added`;
            this._isMounted && await this.setState({
                markerLogMessage: _message,
                topInfoBar: true
            });
            Alert.alert(constants.APP.NAME, _message);
        }

        this.hideLoading();

        this.nTimerID && clearTimeout(this.nTimerID);
        this.nTimerID = setTimeout(() => {
            this._isMounted && this.setState({topInfoBar: false});
        }, 5000);


    };


    /**
     * Adds markers in Bulk mode
     *
     * */

    addMarkersBulk = async (markers) => {  // marker is same to zipcode
        return new Promise(async (resolve, reject) => {
            GeoCoder.readAllMarkersFromCache()
                .then(async (cached_markers) => {

                    /**
                     * We need to check the zipcode is cashed in local stroage
                     * If not, we send the request google to get location
                     * according to the zip code.
                     * */


                    let existing_markers = [];
                    let noexisting_markers = [];
                    MarkerArray.set(cached_markers);
                    (markers || []).map(marker => {
                        if (MarkerArray.indexOf(marker) > -1) {
                            existing_markers.push(marker);
                        } else {
                            noexisting_markers.push(marker);
                        }
                    });


                    /**
                     * Then we need to get the location by Zip Code
                     * */

                    /*if (false && noexisting_markers.length === 0) {
                        let _markers = [];
                        MarkerArray.set(markers);
                        cached_markers.map(marker=>{
                            MarkerArray.indexOf(marker) > -1 && _markers.push(marker);
                        });
                        this._isMounted = true;
                        this._isMounted && await this.setState({markers: _markers, mapShown: true});
                    } else*/
                    {
                        let search_zipcodes = markers.map(marker => marker.zipcode);
                        GeoCoder.getLocationsByZipCodes(search_zipcodes)
                            .then(async markers2 => {

                                let _markers = [];
                                for (let i = markers2.length - 1; i >= 0; i--) {
                                    let _marker = {
                                        coordinate: markers2[i],
                                        zipcode: search_zipcodes[i]
                                    };
                                    _markers.push(_marker);
                                }


                                // We need to save non-redundant data into cache
                                _markers.map(marker => GeoCoder.addMarkerInfoToCache(marker));
                                this._isMounted = true;
                                this._isMounted && await this.setState({markers: _markers, mapShown: true});
                                resolve(_markers);
                            })
                            .catch(error => {

                                this.showMapWithCurrentLocation()
                                    .then(result => this._isMounted = true)
                                    .catch(result => this._isMounted = true);
                            });
                    }

                })
                .catch(error => {

                    this.showMapWithCurrentLocation()
                        .then(result => this._isMounted = true)
                        .catch(result => this._isMounted = true);
                });
        });
    };

    /**
     * actionSheet the Map in current location
     * */

    showMapWithCurrentLocation = async () => {
        return new Promise((resolve, reject) => {
            GeoCoder.getCurrentLocation()
                .then(location => {
                    this.hideLoading();
                    this.setMapRegion({
                        latitude: location.coords.latitude,
                        longitude: location.coords.longitude
                    });
                    resolve(location);
                })
                .catch(error => {
                    this.hideLoading();
                    this._isMounted && this.setState({debugError: JSON.stringify(error)});
                    reject(error);
                });
        });
    };


    /**
     * Add or Remove Marker
     * @params
     *   Google GeoCoder Response
     * @return
     *
     * */
    onRemoveMarker = async (marker) => {

        MarkerArray.set(this.state.markers);
        MarkerArray.remove(marker);

        this._isMounted && await this.setState({
            markers: MarkerArray.get(),
            markerLogMessage: `Zip code "${MarkerArray.getJustRemovedMarker().zipcode}" removed`,
            topInfoBar: true
        });

        this.props.onSelected && this.props.onSelected(this.state.markers);

        this.hideLoading();

        return false;
    };

    render() {
        return (
            <View>
                <Image style={{position: 'absolute', top: -200}}
                       source={srcMarkerIcon}/>
                {this.state.mapShown &&
                <MapView
                    ref={(_map) => this._mapView = _map}
                    onMapReady={this.onMapReady}
                    style={[styles.map, {width: '100%', height: '100%'}]}
                    initialRegion={{
                        latitude: this.state.region.latitude,
                        longitude: this.state.region.longitude,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                    onPress={async (e) => {
                        await this.onPressMapView(e);
                    }}

                    _cacheEnabled={true}
                    _followsUserLocation={true}
                    _loadingEnabled={true}
                    moveOnMarkerPress={false}
                    _showsCompass={true}
                    _showsMyLocationButton={true}
                    _showsPointsOfInterest={true}
                    _showsScale={true}
                    showsUserLocation={true}
                    _stopPropagation={true}
                    _toolbarEnabled={true}
                    _tracksInfoWindowChanges={false}
                    _tracksViewChanges={false}
                    _zoomControlEnabled={true}
                    maxZoomLevel={10}

                >
                    {this.state.markers.map(marker => (
                        <Marker key={`key_${marker.zipcode}`}
                                coordinate={marker.coordinate}
                                onPress={this.onRemoveMarker.bind(this, marker)}
                                tracksViewChanges={false}
                                stopPropagation={true}
                        >
                            <Image
                                source={srcMarkerIcon}/>
                            <Text style={{
                                fontWeight: 'bold',
                                fontSize: 12,
                                position: 'absolute',
                                top: 5, left: 0,
                                width: '100%',
                                lineHeight: 12,
                                paddingVertical: 0,
                                textAlign: 'center'
                            }}>
                                {marker.zipcode}
                            </Text>
                        </Marker>
                    ))}

                </MapView>
                }
                {this.state.topInfoBar &&
                <View style={{
                    position: 'absolute',
                    zIndex: 12,
                    width: '100%',
                    height: 30,
                    left: 5,
                    backgroundColor: 'rgba(255,255,255, 0.5)'
                }}
                      onPress={() => this._isMounted && this.setState({topInfoBar: false})}
                >
                    <Text style={{
                        textAlign: 'center',
                        lineHeight: 30,
                    }}>{this.state.markerLogMessage}</Text>
                </View>
                }

                {this.state.inProcessing &&
                <ActivityIndicator size="large"
                                   style={{
                                       width: '100%', height: '100%',
                                       position: 'absolute',
                                       backgroundColor: 'rgba(255,255,255,0.5)'
                                   }}
                />
                }

                {constants.GOOGLE_MAP.DEBUG_MODE &&
                <View style={{
                    position: 'absolute',
                    zIndex: 10,
                    backgroundColor: 'transparent',
                    width: '100%',
                    height: 100,
                    bottom: 0,
                    right: 0
                }}>
                    {this.state.debugShow &&
                    <View style={{
                        backgroundColor: 'lightgray',
                        width: '100%', height: '100%',
                        flexDirection: 'row'
                    }}>
                        <Text style={{fontSize: 12}}>
                            coordinate: {JSON.stringify(this.state.coordinate)}
                        </Text>
                        <Text style={{fontSize: 12}}>
                            position: {JSON.stringify(this.state.position)}
                        </Text>
                        <Text>
                            {this.state.debugInfo}
                        </Text>
                        <Text>
                            {this.state.debugWarn}
                        </Text>
                        <Text>
                            {this.state.debugError}
                        </Text>
                    </View>
                    }
                    <TouchableOpacity
                        style={{
                            position: 'absolute',
                            zIndex: 11,
                            bottom: 0,
                            right: 0,
                            width: 20,
                            height: 20,
                            borderRadius: 20,
                            backgroundColor: 'darkgray',
                            opacity: 0.1
                        }}
                        onPress={() => this._isMounted && this.setState({debugShow: !this.state.debugShow})}>
                    </TouchableOpacity>
                </View>
                }
            </View>
        );
    }
}

export default CustomMapView;
