import {Text, TouchableOpacity, View} from "react-native";
import React from "react";
import styles from "./styles";

class Segment extends React.PureComponent {
    state: {
        value: null
    };

    constructor(props) {
        super(props);
    }

    componentWillMount(): void {
        this.setState({value: this.props.value});
    }

    _handlePress = async (id) => {
        await this.setState({value: id});
        this.props.onChange && this.props.onChange(this.state.value);
    };

    render() {
        return (
            <View style={{
                flex: 1, flexDirection: "row", marginTop: 2,
                marginLeft: 15,
            }}>
                {this.props.data.map(item => {
                    return (
                        <TouchableOpacity
                            onPress={this._handlePress.bind(this, item.id)}
                            key={`key_${item.id}`}
                            style={[styles.item.base, this.state.value === item.id ?
                                styles.item.selected : {}]}>
                            <Text style={[styles.item_text.base,
                                this.state.value === item.id ?
                                    styles.item_text.selected : {}]}>
                                {item.title}
                            </Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    }
};


export {Segment};
