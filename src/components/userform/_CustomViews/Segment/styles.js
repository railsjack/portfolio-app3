import theme from "../../../../theme/default";
export default {
    item: {
        base: {
            width: 35, height: 30, marginLeft: 5,

        },
        selected: {
        },
    },
    item_text: {
        base: {
            borderRadius: 5, borderWidth: theme.borderWidth,
            borderColor: theme.defaultBgColor,
            color: theme.defaultBgColor,
            lineHeight: 30, height: 30,
            textAlign: "center", fontWeight: "bold", fontSize: 15,
        },
        selected: {
            borderRadius: 5, backgroundColor: theme.defaultBgColor, //"rgb(0,122,255)",
            color: "white", lineHeight: 30, height: 30,
            textAlign: "center", fontWeight: "bold", fontSize: 15,
        }
    }
};
