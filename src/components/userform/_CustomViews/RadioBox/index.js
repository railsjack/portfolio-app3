import React, {Component} from 'react';
import {View} from 'react-native';
import {CheckBox} from 'react-native-elements';
import theme from '../../../../theme/default';

class RadioBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: ''
        };
        this.dataReceived = false;
        this.updateValue = this.updateValue.bind(this);
    }

    componentWillMount(): void {
        this.setState({selectedValue: this.props.selectedValue});
    }

    componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
        if (this.dataReceived) {
            return;
        }
        if(nextProps.selectedValue) this.dataReceived = true;

        this.setState({selectedValue: nextProps.selectedValue});
    }

    updateValue(value) {
        this.setState({selectedValue: value});
        this.props.onSelect && this.props.onSelect(value);
    }

    render() {
        const {
            checkedIcon = 'circle',
            uncheckedIcon = 'circle-o',
            checkedColor = 'rgb(0,165,153)',
            uncheckedColor = 'darkgray',
            title = 'Radio'
        } = this.props;
        return (
            <View style={[this.props.radioStyle, {flexDirection: 'row'}]}>
                {this.props.items.map((item, index) => (
                    <CheckBox
                        checked={this.state.selectedValue === item.key}
                        onPress={this.updateValue.bind(this, item.key)}
                        key={item.key}
                        checkedIcon={checkedIcon}
                        uncheckedIcon={uncheckedIcon}
                        checkedColor={checkedColor}
                        uncheckedColor={uncheckedColor}
                        title={item.label || title}
                        textStyle={{
                            fontFamily: theme.fontFamily,
                            fontWeight: theme.fontWeightNormal
                        }}
                        containerStyle={[
                            {
                                backgroundColor: 'transparent',
                                borderWidth: 0,
                                marginLeft: 30
                            },
                            this.props.containerStyle
                        ]}
                    />
                ))}
            </View>
        );
    }
}

export {RadioBox};
