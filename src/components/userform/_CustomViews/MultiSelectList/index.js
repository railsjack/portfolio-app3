import React from "react";
import {FlatList, Text, TouchableOpacity, View} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import styles from "./styles";

class MyListItem extends React.PureComponent {
    _onPress = () => {
        this.props.onPressItem(this.props.id);
    };

    render() {
        return (
            <TouchableOpacity
                onPress={this._onPress}
                style={styles.item_base}>
                {this.props.selected &&
                <Icon name="ios-checkmark" size={30} color="blue"
                      style={styles.item_icon}
                />
                }
                <View style={styles.item_text_container}>
                    <Text style={styles.item_text}>
                        {this.props.title}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const NUM_DATA = 10;

class MultiSelectList extends React.PureComponent {
    isLoading = false;
    _preSelectedID = null;

    componentWillMount(): void {
        let _map = (new Map(): Map<string, boolean>);
        if (this.props.selectedValue){
            _map.set(this.props.selectedValue, true);
            this._preSelectedID = this.props.selectedValue;
        }
        if (this.props.selectedValues){
            this.props.selectedValues.map((selectedValue)=>{
                _map.set(selectedValue, true);
                this._preSelectedID = selectedValue;
            });
        }
        this.setState({
            multiselectable: (this.props.multiselectable !== false),
            data: this.getData(0, NUM_DATA),
            selected: _map
        });
    }

    componentDidMount(): void {
        //this.setState({data: this.getData(0, NUM_DATA)});
    }

    _keyExtractor = (item, index) => item.id.toString();

    _onPressItem = async (id: string) => {
        if (!this.state.multiselectable && this._preSelectedID !== null) {
            await this.setState((state) => {
                const selected = new Map(state.selected);
                selected.set(this._preSelectedID, false); // toggle
                return {selected};
            });
        }

        // updater functions are preferred for transactional updates
        await this.setState((state) => {
            // copy the map rather than modifying state.
            const selected = new Map(state.selected);
            selected.set(id, !selected.get(id)); // toggle
            !this.state.multiselectable && (this._preSelectedID = id);
            return {selected};
        });

        this.props.onChanged && this.props.onChanged(this._getSelectedValues());
    };

    _getSelectedValues = () => {
        let _ret = [];
        Array.from(this.state.selected).map(item=>{
            (item[1] && _ret.push(item[0]));
        });
        return _ret;
    };

    _renderItem = ({item}) => (
        <MyListItem
            style={styles.list_item}
            id={item.id}
            onPressItem={this._onPressItem}
            selected={!!this.state.selected.get(item.id)}
            title={item.title}
        />
    );

    getData = (start, num) => {
        return this.props.data? this.props.data.slice(start, start + num) : [];
    };

    _onEndReached = async () => {
        if (this.isLoading) {
            return;
        }
        this.isLoading = true;
        let data = this.state.data;
        let newData = data.concat(this.getData(data.length, NUM_DATA));
        await this.setState({data: newData});
        setTimeout(() => this.isLoading = false, 100);
    };

    render() {
        return (
            <FlatList
                style={[styles.list]}
                containerStyle={this.props.style}
                data={this.state.data}
                keyExtractor={this._keyExtractor}
                renderItem={this._renderItem}
                onEndReached={this._onEndReached}
                onEndReachedThreshold={0.2}
            />
        );
    }
}

export {MultiSelectList};

