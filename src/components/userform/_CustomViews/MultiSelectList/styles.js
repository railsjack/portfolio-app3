import theme from "../../../../theme/default";

export default {
    list: {
        paddingLeft: 10,
        paddingRight: 20,
        width: "100%",
    },
    list_item: {
        width: "100%",
        height: 30,
        lineHeight: 22,
        textAlign: "center"
    },
    item_base: {
        width: "100%", height: 40,
        flex: 1, flexDirection: "row",
    },
    item_icon: {
        position: "absolute", left: 5, top: 5
    },
    item_text_container: {
        width: "100%",
        height: "100%",
        marginLeft: 30,
        borderBottomWidth: theme.borderWidth,
        borderBottomColor: theme.borderColor,
    },
    item_text: {
        width: "100%",
        lineHeight: 40,
        fontSize: theme.fontSizeSmall,
        paddingLeft: 5,
    }
};
