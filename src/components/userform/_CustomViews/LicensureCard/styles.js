export default {
    license_cards: {
        base: {},
        card_view: {
            base: {
                borderWidth: 1,
                borderColor: "lightgray",
                padding: 10,
                width: "100%",
                marginLeft: "auto",
                marginRight: "auto",
                marginBottom: 10
            },
            picker: {
                color: "rgb(125, 125, 125)",
                borderColor: "darkgray",
                borderWidth: 1,
                height: 35,
                fontSize: 10
            },
            text_input: {
                color: "rgb(125, 125, 125)",
                height: 35,
                paddingVertical: 0,
                lineHeight: 20,
                fontSize: 16,
                paddingLeft: 5,
                backgroundColor: "white"
            },
            title: {
                fontSize: 16,
                color: "green"
            },
            line1: {
                base: {
                    width: "100%",
                    fontSize: 12
                },
                state: {
                    marginTop: 10,
                    fontSize: 10,
                    flexDirection: "column",
                    backgroundColor: "transparent"
                }
            },
            line2: {
                base: {
                    flexDirection: "row"
                },
                designation: {
                    marginRight: "auto",
                    width: "49%",
                    fontSize: 12,
                    flexDirection: "column",
                    backgroundColor: "transparent"
                },
                license_no: {
                    marginLeft: "auto",
                    width: "49%",
                    marginTop: 4.65,
                    backgroundColor: "transparent",
                }
            }
        },
        mandatory: {
            color: "red",
            fontSize: 12
        }
    },
    label: {
        base: {
            fontSize: 14,
            color: "darkgray",
            backgroundColor: "transparent"
        }
    }

};
