import React, {Component} from 'react';
import {DeviceEventEmitter, Platform, Text, View, TouchableOpacity} from 'react-native';
import {CheckBox} from 'native-base';
import StateStore from '../../../../store/StateStore';
import LicensureCardStore from '../../../../store/LicensureCardStore';
import {ListModal} from '../ListModal';
import styles from './styles';
import form_styles from '../../Step1/styles';
import {FloatingLabelText} from '../FloatingLabelText';
import theme from '../../../../theme/default';

class LicensureCard extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount(): void {
        this.setState({
            // key: "key_" + this.props.licensure_card.key,
            index: this.props.licensure_card.index,
            state: this.props.licensure_card.state,
            designation: this.props.licensure_card.designation,
            license_no: this.props.licensure_card.license_no
        });
    }

    componentDidMount(): void {
        /**
         * Handles an event about OnValidationRequired
         * In parent component, if the user clicks "Next" without entering valid data,
         * an event named "OnValidationRequired" will be fired.
         * Here we can capture the event.
         * */
        this.eventListener = DeviceEventEmitter.addListener('OnValidationRequired', this.onValidationRequired);
    }

    componentWillUnmount(): void {
        /**
         * Removes an event listener when the component is unmounted.
         * */
        this.eventListener.remove();
    }

    /**
     * Handles an event when the validation is required or not
     * */
    onValidationRequired = () => {
        this.setState({showMandatoryFields: true});
    };

    onStateChanged = async (itemValue, itemIndex) => {
        await this.setState({state: itemValue});
        this.props.onChangeLicensureCard && this.props.onChangeLicensureCard(this.state, this.props.index);
    };

    onDesignChanged = async (itemValue, itemIndex) => {
        await this.setState({designation: itemValue});
        this.props.onChangeLicensureCard && this.props.onChangeLicensureCard(this.state, this.props.index);
    };

    onNoChanged = async (itemValue) => {
        await this.setState({license_no: itemValue});
        this.props.onChangeLicensureCard && this.props.onChangeLicensureCard(this.state, this.props.index);
    };

    onCompensationChanged = async (itemValue) => {
        await this.setState({compensation_check: !this.state.compensation_check})
        this.props.onChangeLicensureCard && this.props.onChangeLicensureCard(this.state, this.props.index);
    };

    render() {
        return (
            <View style={styles.license_cards.card_view.base}>
                <Text style={styles.license_cards.card_view.title}>
                    State {this.props.index + 1}
                </Text>
                <View style={styles.license_cards.card_view.line1.state}>
                    <ListModal
                        placeholder="State"
                        // floatingTextStyle={form_styles.form.state_label_floating}
                        labelContainerStyle={form_styles.form.state_label}
                        labelStyle={{fontSize: 13.5, color: '#444', opacity: 1}}
                        data={StateStore.allStates()}
                        multiselectable={false}
                        selectedValue={this.state.state}
                        onConfirm={async (itemValue) => {
                            itemValue[0] && this.onStateChanged(itemValue[0]);
                        }}
                    />
                    {this.state.showMandatoryFields && !this.state.state &&
                    <Text style={styles.license_cards.mandatory}>
                        This field is required.
                    </Text>
                    }
                </View>
                <View style={styles.license_cards.card_view.line2.base}>
                    <View style={
                        [styles.license_cards.card_view.line2.designation,
                            {marginTop: Platform.OS === 'ios' ? 18 : 11}]
                    }>
                        <ListModal
                            style={{height: 200}}
                            placeholder="Designation"
                            // floatingTextStyle={form_styles.form.state_label_floating}
                            labelContainerStyle={form_styles.form.state_label}
                            labelStyle={{fontSize: 13.5, color: '#444', opacity: 1}}
                            data={LicensureCardStore.getDesignations()}
                            multiselectable={false}
                            selectedValue={this.state.designation}
                            onConfirm={async (itemValue) => {
                                itemValue[0] && this.onDesignChanged(itemValue[0]);
                            }}
                        />
                        {this.state.showMandatoryFields && !this.state.designation &&
                        <Text style={styles.license_cards.mandatory}>
                            This field is required.
                        </Text>
                        }
                    </View>
                    <View style={styles.license_cards.card_view.line2.license_no}>
                        <FloatingLabelText
                            style={form_styles.form.text_input}
                            label="License No."
                            returnKeyType="next"
                            autoCorrect={false}
                            onChangeText={async license_no => {
                                this.onNoChanged(license_no);
                            }}
                            value={this.state.license_no}
                        />
                        {this.state.showMandatoryFields && !this.state.license_no &&
                        <Text style={styles.license_cards.mandatory}>
                            This field is required.
                        </Text>
                        }
                    </View>
                </View>
                {this.state.designation && this.state.designation !== 'CNA' &&
                <View>
                    <View style={{padding: 5, marginTop: 5, flex: 1, flexWrap: 'wrap'}}>
                        <Text
                            style={{color: theme.redColor, fontSize: theme.fontSizeMedium, marginBottom: 10}}>
                            Compensation
                        </Text>
                        <Text>
                            Patients have differing levels of medication intake requirements. In this system, they are
                            classified as
                            Level 1 and Level 2. Level 2 requires advanced licensure and assistance. Accordingly, the
                            compensation
                            for a Level 2 encounter is greater than Level 1.
                            Since you hold an elevated health care credential, your profile will automatically appear
                            for
                            patients in
                            your territory who need Level 2 assistance. If you would like your profile to also appear
                            for
                            patients that
                            only require Level 1 assistance, please check the box below. This acknowledges you will make
                            yourself
                            available to Level 1 patients and accept the compensation associated with that category of
                            patients. <Text style={{color: theme.redColor}}>If you do not check the box, you will not
                            appear in Level 1 patient searches.</Text>
                        </Text>
                    </View>
                    <TouchableOpacity
                        style={{flex: 1, flexDirection: 'row', marginTop: 10}}
                        onPress={this.onCompensationChanged} >
                        <CheckBox
                            disabled
                            style={{marginLeft: -10, marginRight: 15}}
                            checked={this.state.compensation_check}
                        />
                        <Text>Yes, I would like to appear in Level 1 patient searches.</Text>
                    </TouchableOpacity>
                </View>
                }
            </View>
        );
    }
}

export {LicensureCard};
