import theme from "../../../../theme/default";

const FLOAT_INPUT = {
  HEIGHT: 30,
  LINE_HEIGHT: 18,
};

const styles = {
  textStyle: {
    height: FLOAT_INPUT.HEIGHT,
    lineHeight: FLOAT_INPUT.LINE_HEIGHT,
    fontSize: theme.fontSizeSmall,
    color: theme.defaultTextColor,
    paddingVertical: 0,
    paddingHorizontal: 0,
  }
};

export default styles;
