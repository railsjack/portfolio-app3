import React, {Component} from "react";
import {Animated, TextInput, View} from "react-native";
import TextInputMask from "react-native-text-input-mask";
import styles from "./styles";
import theme from "../../../../theme/default";

class FloatingLabelText extends Component {
  state = {
    isFocused: false,
    value: this.props.value || ""
  };

  componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
    this.setState({value: this.props.value});
  }

  componentWillMount(): void {
    this._animatedIsFocused = new Animated.Value(0);
  }

  componentDidUpdate() {
    Animated.timing(this._animatedIsFocused, {
      toValue: (this.state.isFocused) ? 1 : 0,
      duration: 200,
    }).start();
  }

  handleFocus = () => this.setState({isFocused: true});
  handleBlur = () => this.setState({isFocused: false});

  render() {
    const {label, ...props} = this.props;
    const animationStyle = {
      position: "absolute",
      color: this.state.isFocused ? '#00A599' : theme.defaultTextColor,
      left: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [(!this.props.value ? (this.props.animationLeft || 0) : 0), 0],
      }),
      top: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [(!this.props.value ? (this.props.animationTop || 20) : 0), 0],
      }),
      fontSize: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [(!this.props.value ? 13.5 : 14), 14],
      }),
      opacity: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [(!this.props.value ? 1 : 0.6), 0.6],
      }),
    };

    return (
      <View style={[{paddingTop: 15, height: props.containerHeight || 45}, props.containerStyle]}>
        <Animated.Text style={animationStyle}>
          {label}
        </Animated.Text>
        {props.mask &&
        <TextInputMask
          refInput={ref => {
            this._ref = ref;
          }}
          {...props}
          style={[styles.textStyle, props.style]}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          onChangeText={value => {
            this.setState({value: value});
            this.props.onChangeText && this.props.onChangeText(value);
          }}
          blurOnSubmit

        />
        }
        {!props.mask &&
        <TextInput
          {...props}
          style={[styles.textStyle, props.style]}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          onChangeText={value => {
            this.setState({value: value});
            this.props.onChangeText && this.props.onChangeText(value);
          }}
          blurOnSubmit={props.multiline !== true}
        />}
      </View>
    );
  }
}

export {FloatingLabelText};
