import React, {Component} from "react";
import {Picker, Platform, Text, TouchableOpacity, View} from "react-native";
import Modal from "react-native-modal";
import {MultiSelectList} from "../MultiSelectList";
import styles from "./style";

export default class ListModal extends Component {
    state = {
        isModalVisible: false,
    };

    values = [];


    _showModal = () =>
        this.setState({isModalVisible: true});

    showModal = () => this._showModal();


    _hideModal = () =>
        this.setState({isModalVisible: false});

    _confirmModal = async () => {
        await this.props.onConfirm && this.props.onConfirm(this.values);
        await this._hideModal();
    };

    _onChanged = (values) => {
        this.values = values;
    };

    render() {
        return (
            <View style={{
                flex: 1
            }}>
                {<Text style={[this.props.floatingTextStyle]}>
                    {this.props.selectedValue ? this.props.placeholder : ""}
                </Text>}
                <TouchableOpacity
                    style={[this.props.labelContainerStyle]}
                    onPress={this._showModal}>
                    <Text style={[this.props.labelStyle]}>
                        {this.props.selectedValue || this.props.placeholder}
                    </Text>
                </TouchableOpacity>
                <Modal
                    style={{
                        backgroundColor: (
                        Platform.OS === "ios" && !this.props.isAndroidMode ?
                        "transparent" : "white"),
                        justifyContent: "flex-end", borderRadius: 2,}}
                    isVisible={this.state.isModalVisible === true}>
                    {
                        Platform.OS === "ios" && !this.props.isAndroidMode ?
                            <View style={{
                                backgroundColor: "rgba(248,248,248,0.8)", height: "40%",
                                minHeight: 300,
                                borderRadius: 10, justifyContent: "space-between"
                            }}>
                                <Picker
                                    style={{padding: 10}}
                                    selectedValue={this.state.value}
                                    onValueChange={(itemValue, itemIndex) => {
                                        this.setState({value: itemValue});
                                        this._onChanged([itemValue]);
                                    }}
                                    textStyle={{color: "#5cb85c"}}
                                    itemStyle={{
                                        backgroundColor: "#d3d3d3",
                                        marginLeft: 0,
                                        paddingLeft: 10,
                                    }}
                                >
                                    <Picker.Item value={""} label={this.props.title || 'Select a County'} />
                                    {
                                        (this.props.data &&
                                            this.props.data.map(item => {
                                                return <Picker.Item key={item.id} label={item.name} value={item.id}/>;
                                            })) || []
                                    }
                                </Picker>
                            </View>
                            :
                            <MultiSelectList
                                selectedValue={this.props.selectedValue}
                                selectedValues={this.props.selectedValues}
                                multiselectable={this.props.multiselectable}
                                style={styles.scrollableModalContent1}
                                data={this.props.data}
                                onChanged={this._onChanged}
                            />
                    }
                    <View style={styles.scrollableModalButtons}>
                        <TouchableOpacity
                            onPress={this._confirmModal}
                            style={styles.button}>
                            <Text style={styles.button_text}>Confirm</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this._hideModal}
                            style={[styles.button, styles.cancelButton]}>
                            <Text style={styles.button_text}>Cancel</Text>
                        </TouchableOpacity>

                    </View>
                </Modal>
            </View>
        );
    }
}

export {ListModal};
