import theme from "../../../../theme/default";

export default {
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    modalContent: {
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)",
    },
    bottomModal: {
        justifyContent: "flex-end",
        margin: 0,
    },
    scrollableModal: {
        height: 400,
        backgroundColor: "white",
        paddingTop: 40,
        paddingBottom: theme.btnHeight + 10,
    },
    scrollableModalTitle: {
        width: "100%",
        height: 40,
        lineHeight: 40,
        position: "absolute",
        textAlign: "center",
        top: 0, left: 0, zIndex: 10
    },
    scrollableModalButtons: {
        flexDirection: "row",
        height: theme.btnHeight + 10,
        position: "absolute",
        bottom: 0, left: 0, zIndex: 11
    },
    button: {
        width: "50%",
        backgroundColor: theme.defaultBgColor,
        height: theme.btnHeight + 10,
    },
    cancelButton: {
        backgroundColor: 'red'
    },
    button_text: {
        width: "100%",
        color: theme.inverseTextColor,
        lineHeight: theme.btnHeight + 10,
        textAlign: "center"
    },
    scrollableModalContent1: {

    },
    scrollableModalContent2: {
        height: 50,
        alignItems: "center",
        justifyContent: "center",
    },

    listItem: {
        height: 30,
        lineHeight: 22,
        textAlign: "center",
    },
};
