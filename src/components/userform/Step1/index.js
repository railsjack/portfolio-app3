/**
 * Component for the Signup Form
 * @WhatToDo
 * - Creates a Component for the Signup
 * - This form includes the following inputs
 *   |--------------------------------------------------------------------+
 *   |   MedNurse Enrollment Form (Step 1 - Personal Information)         |
 *   |--------------------------------------------------------------------+
 *   |   1.  First Name          |   11.  Mobile Phone                    |
 *   |   2.  Last Name           |   12.  Home Phone                      |
 *   |   3.  Sex                 |   13.  Bio                             |
 *   |   4.  Date of Birth       |   14.  LicensureCards                  |
 *   |   5.  Address 1           |   15.  Upload Resume                   |
 *   |   6.  Address 2           |   16.  Upload Picture                  |
 *   |   7.  State               |   17.  Create Login Credentials        |
 *   |   8.  County              |   18.  Password                        |
 *   |   9.  City                |   19.  Password Confirmation           |
 *   |  10.  Zip                 |                                        |
 *   |--------------------------------------------------------------------+
 * @Notes
 *   This component needs to be reorganized by Redux in future
 * */

import React, {Component} from 'react';
import {
    Alert,
    DeviceEventEmitter,
    FlatList,
    Keyboard,
    ScrollView,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {Picker} from 'native-base';
import {Button} from '../../shared';
import {DocumentPicker, DocumentPickerUtil} from 'react-native-document-picker';
import constants from '../../_base_data/constants';
import styles from './styles';

import {LicensureCardStore, StateStore, ValidationStore as V} from '../../../store';
import {FloatingLabelText, LicensureCard, ListModal, RadioBox} from '../_CustomViews';

String.prototype.Capitalize = function () {
    return this[0].toUpperCase() + this.substr(1);
};

class Step1 extends Component {
    _scrollView = null;
    _isMounted = false;

    constructor(props) {
        super(props);
        this._scrollView = null;

    }

    async componentWillMount(): void {

        this._isMounted = false;
        if ( this.props.mode === constants.APP.USERFORM_MODE.SIGNUP){
            await this._setFormData();
        }
    }

    componentWillUnmount(): void {
        this.setState({first_name: ''});
    }

    _setFormData = async () => {
        await this.setState({showMandatoryFields: false});
        await this.setState(this.props.formData);
        await this.changeLicensureCardsCount(this.props.formData.licensure_cards.length || 1);
    };

    async componentDidMount(): void {
        if (!this._isMounted && this.props.formData && this.props.formData.first_name) {
            this._isMounted = true;
            this._setFormData();
        }
    }

    async componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {

        if (!this._isMounted && nextProps && nextProps.formData && nextProps.formData.first_name) {
            this._isMounted = true;
            this._setFormData();
        }
    }

    /**
     * changeLicensureCardsCount (itemValue, itemIndex)
     * Called when
     *   - the screen loads at first (with 1)
     *   - the user change the count of licensure cards.
     * @params
     *   itemValue: [1..3], the count of the licensure cards
     *   itemCount: not used for now
     * @WhatToDo
     *   Initialize the state variables
     *     this.state.licensure_cards  =>  {index: 1, key: "key_", name: "State 1"}
     *     this.state.licensure_cardsCount => 1
     * @return
     *   Nothing
     * */
    changeLicensureCardsCount = (itemValue, itemIndex) => {
        let _temp = [];
        let _origin_licensure_cards = this.props.formData.licensure_cards;

        for (let i = 0; i < itemValue; i++) {
            if (i < _origin_licensure_cards.length) {
                _temp.push(LicensureCardStore.createWithOriginalData(_origin_licensure_cards[i]));
            } else {
                _temp.push(LicensureCardStore.createNewByIndex(i + 1));
            }
        }

        this.setState({
            licensure_cards: _temp,
            licensure_cardsCount: itemValue
        });
    };

    /**
     * Checks if the entries are valid or not.
     * It uses validate(fieldName, values, callback)
     *   @params:
     *    fiendName: name of the field to be checked
     *    values: object including values need to be checked
     *    callback: fires when the entered data is not valid
     *   @return: true/false
     *     It returns true if the entry is invalid, otherwise false
     * */

    validateForm = () => {
        let _validated;
        return (
            (_validated = V.validate('first_name', this.state, () => {
                    this._scrollView.scrollTo({y: 0});
                }) &&
                V.validate('last_name', this.state, () => {
                    this._scrollView.scrollTo({y: 180});
                }) &&
                V.validate('mobile_phone', this.state, () => {
                    this._scrollView.scrollTo({y: 650});
                }) &&
                V.validate('home_phone', this.state, () => {
                    this._scrollView.scrollTo({y: 730});
                }) &&
                V.validate_collection(['state', 'designation', 'license_no'],
                    this.state.licensure_cards, (index) => {
                        this._scrollView.scrollTo({y: 880 + 200 * index});
                    }) &&
                V.validate('upload_picture_valid', this.state, () => {
                    this._scrollView.scrollTo({y: 800 + this.state.licensure_cards.length * 200});
                }) &&
                V.validate('upload_resume_valid', this.state, () => {
                    this._scrollView.scrollTo({y: 800 + this.state.licensure_cards.length * 200});
                }) &&
                V.validate('email', this.state, () => {
                    this._scrollView.scrollTo({y: 2000});
                }) &&
                V.validate('password', this.state, () => {
                    this._scrollView.scrollTo({y: 2000});
                }) &&
                V.validate_with('password_confirmation', 'password', this.state, () => {
                    this._scrollView.scrollTo({y: 2000});
                })),
                (!_validated && this.indicateMandatoryFields()),
                _validated
        );
    };

    /**
     * Let users know which fields are mandatory
     * */
    indicateMandatoryFields = () => {
        this.setState({showMandatoryFields: true});
        /**
         * Fires an Event when we need to indicate Mandatory fields to the user.
         * This is necessary for the child component
         * ex: this event will be handeld in LicensureCard Component
         * //DeviceEventEmitter.addListener("OnValidationRequired", this.onValidationRequired);
         * */
        DeviceEventEmitter.emit('OnValidationRequired', {});

    };


    /**
     * When the user changes the licensure cards after signed up,
     *   we need to reflect the cards to nurse_state
     * */
    reflectNurseDataFromLicensureCards = async () => {
        let _nurse_state = [];
        this.state.licensure_cards.map(licensure_card => {
            let _licensure_card = licensure_card;
            _licensure_card.state = (_licensure_card.state);
            _nurse_state.push(licensure_card);
        });
        let _nurse = this.state.nurse || {};
        _nurse.nurse_state = _nurse_state;
        await this.setState({nurse: _nurse});
    };

    /**
     * Builds form data from the this.state
     *   @params:
     *
     *   @return: object
     *     {
     *         nurse: {
     *             first_name,
     *             last_name,
     *             ...,
     *             licensure_state: { "1": ... , "2": ... },
     *             licensure_designs: { "1": ... , "2": ... },
     *             licensure_nos: { "1": ... , "2": ... }
     *         },
     *         user: {
     *             email,
     *             password,
     *             password_confirmation,
     *         }
     *     }
     * */
    buildFormData = () => {
        //let data = new FormData();
        let licensure_states = {};
        let licensure_designs = {};
        let licensure_nos = {};
        let allow_compensation = {};
        this.state.licensure_cards.map((item, index) => {
            let item_index = '' + (index + 1);
            licensure_states[item_index] = StateStore.nameToAbbr(item.state) || '';
            licensure_designs[item_index] = item.designation || '';
            licensure_nos[item_index] = item.license_no || '';
            allow_compensation[item_index] = (item.designation !== 'CNA' && item.compensation_check) ? 'true' : 'false';

        });

        return {
            nurse: {
                first_name: this.state.first_name,
                last_name: this.state.last_name,
                gender: this.state.gender,
                dob: this.state.dob,
                address1: this.state.address1,
                address2: this.state.address2,
                state: StateStore.nameToAbbr(this.state.state),
                county: this.state.county,
                city: this.state.city,
                zip: this.state.zip,
                mobile_number: this.state.mobile_phone,
                phone_number: this.state.home_phone,
                bio: this.state.bio,
                licensure_state: licensure_states,
                designation: licensure_designs,
                license_no: licensure_nos,
                allow_compensation: allow_compensation,
            },
            user: {
                email: this.state.email,
                password: this.state.password,
                password_confirmation: this.state.password_confirmation
            }
        };
    };

    /**
     * Fires when the user clicks "NEXT" button in the Step 1
     * */
    handleSaveStep1 = async () => {


        if (this.props.formData.formSavedToServer) {
            this.props.onReceivedNurse &&
            await this.props.onReceivedNurse(
                this.state.nurse
            );
            this.props.onNextStep && await this.props.onNextStep();
        }
        if (this.validateForm()) {

            /**
             IMPORTANT
             This is a part for the Uploading with JSON and Multipart/Form-Data
             RNFetchBlob.fetch("POST", constants.APP.END_POINT.SIGNUP.STEP1, {
                //Authorization: "Bearer access-token",
                //otherHeader: "foo",
                // this is required, otherwise it won't be process as a multipart/form-data request
                "Content-Type": "multipart/form-data",
                //'Content-Type': 'application/json',
            }, [
             /!*!// append field data from file path for the resume
             {
                    name: "nurse[upload_resume]",
                    filename: this.state.upload_resume.fileName,
                    data: this.state.upload_resume.uri && RNFetchBlob.wrap(this.state.upload_resume.uri)
                },
             // append field data from file path for the picture
             {
                    name: "nurse[upload_picture]",
                    filename: this.state.upload_picture.fileName,
                    data: this.state.upload_picture.uri cancelButton&& RNFetchBlob.wrap(this.state.upload_picture.uri)
                },*!/
             {
                    name: "nurse", data: JSON.stringify(data.nurse)
                },
             {
                    name: "user", data: JSON.stringify(data.user)
                },
             ]).then((resp) => {

            }).catch((err) => {

            });*/

            if (constants.APP.USE_PRE_DATA.IN_SIGNUP) {
                /*this.props.onSaveStepData && this.props.onSaveStepData(this.state);
                await this.props.onReceivedNurse &&
                this.props.onReceivedNurse(constants.PRE_DATA.RESPONSE_WHEN_STEP1_SAVED);
                this.props.onNextStep && this.props.onNextStep();*/
            } else {
                let data = this.buildFormData();
                console.log('data on Step 1:', data);
                this.setState({inProcessing: true});

                /**
                 * If the form works in edit mode, here we need to handle the process.
                 *
                 * */
                if (this.props.mode === constants.APP.USERFORM_MODE.EDIT) {
                    await this.reflectNurseDataFromLicensureCards();
                    await this.setState({inProcessing: false});
                    this.props.onSaveState && await this.props.onSaveState(this.state);
                    this._isMounted = false;
                    this.props.onNextStep && await this.props.onNextStep();
                    return;
                }

                /**
                 * It's used when the API Server is down
                 * this.props.onReceivedNurse &&
                 * await this.props.onReceivedNurse(
                 *     {nurse_state: _nurse_state}
                 * );
                 * this.props.onNextStep && this.props.onNextStep();
                 * return;
                 * */


                /**
                 * Start sending Request to Server API for Step 1
                 * */
                fetch(constants.APP.END_POINT.SIGNUP.STEP1, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data)
                })
                    .then(response => response.json())
                    .then(async (response) => {

                        /**
                         * If 200 Response is arrived from the server
                         * This means that the server is online at least
                         * */

                        this.setState({inProcessing: false});
                        if (response.error) {

                            /**
                             * In case the user already exists there.
                             * */
                            if (response.error.indexOf('exists') > -1) {
                                /**
                                 * If the user already exists there,
                                 *  this means that user already entered data in Step 2, 3, 4,...
                                 *  So, we send the user to the next screen, before we need to
                                 *    1) reflect nurse data from licensure_cards
                                 *    2) _Store the state data to the parent Component
                                 * */
                                if (this.props.formData.stepPassed === true) {
                                    await this.reflectNurseDataFromLicensureCards();
                                    this.props.onSaveState && await this.props.onSaveState(this.state);
                                    this.props.onNextStep &&
                                    this.props.onNextStep && this.props.onNextStep();
                                } else {
                                    Alert.alert(constants.APP.NAME, response.error);
                                }
                            } else {
                                /**
                                 * In any other case, we stop here after showing error message
                                 * */
                                Alert.alert(constants.APP.NAME, response.error);
                            }

                        } else {

                            /**
                             * If the user has been successfully registered to server,
                             *  we need to _Store the response from the server. (nurse data)
                             * */

                            this.setState({nurse: response.nurse});
                            this.props.onSaveState && await this.props.onSaveState(this.state);
                            this.props.onNextStep && await this.props.onNextStep();
                        }

                    })
                    .catch(error => {

                        /**
                         * In any other case when there are some issues on connect to server,
                         * we stop here after showing error message to the user.
                         * */
                        this.setState({inProcessing: false});
                        Alert.alert(constants.APP.NAME, 'Email Already Exists!');


                    });
            }

        }
    };

    putDemoData = async () => {

        this.changeLicensureCardsCount(0);
        await this.setState(constants.PRE_DATA.STEP1_INFORMATION);
        await this.setState({
            licensure_cards: [
                {
                    designation: LicensureCardStore.getDesignationName(1),
                    license_no: '111',
                    state: 'Arizona',
                },
                {
                    designation: LicensureCardStore.getDesignationName(2),
                    license_no: '222',
                    state: 'Wisconsin',
                }
            ]
        });
        await this.setState({
            first_name: this.shuffle(6).Capitalize(),
            last_name: this.shuffle(8).Capitalize(),
            email: this.shuffle(7) + '@abc.com'
        });
    };

    shuffle = (count) => {
        return [...Array(count).keys()]
            .map(char =>
                String.fromCharCode(parseInt(Math.random() * 26) + 97)).join('');
    };

    /**
     * renders UI for Step 1
     * */
    render() {
        return (
            <View>
                <TouchableOpacity
                    style={{
                        position: 'absolute',
                        right: 0, top: 50,
                        backgroundColor: 'rgba(0,0,0,0.05)',
                        width: 30, height: 30,
                        borderRadius: 30,
                        zIndex: 1
                    }}
                    onPress={this.putDemoData}
                />
                {this.state &&
                <ScrollView
                    style={styles.form.container}
                    ref={view => (this._scrollView = view)}
                >
                    <View style={[styles.form.desc.base, styles.form.card_line]}>
                        <Text style={[styles.form.base, styles.form.desc.content]}>
                            {this.props.mode === constants.APP.USERFORM_MODE.SIGNUP &&
                            'Registraion to become a Mobile MedNurse is a four-step process.' +
                            'Please begin here by entering your information.'
                            }
                            {this.props.mode === constants.APP.USERFORM_MODE.EDIT &&
                            'You can change any information whatever you want here'
                            }
                        </Text>
                    </View>


                    {/** Personal Information **/}
                    <View style={styles.section_block}>
                        {/** Personal Information Title **/}
                        <Text style={[styles.form.base, styles.form.desc.title]}>
                            Personal Information
                        </Text>

                        {/** First name **/}
                        <View style={styles.form.card_line}>
                            <FloatingLabelText
                                style={styles.form.text_input}
                                label="First Name"
                                returnKeyType="next"
                                autoCorrect={false}
                                onChangeText={first_name => {
                                    this.setState({first_name: first_name});
                                }}
                                value={this.state.first_name}
                            />
                            {this.state.showMandatoryFields && !this.state.first_name &&
                            <Text style={styles.form.mandatory}>
                                This field is required.
                            </Text>
                            }
                        </View>

                        {/** Last name **/}
                        <View style={styles.form.card_line}>
                            <FloatingLabelText
                                style={styles.form.text_input}
                                label="Last Name"
                                returnKeyType="next"
                                autoCorrect={false}
                                onChangeText={last_name => {
                                    this.setState({last_name: last_name});
                                }}
                                value={this.state.last_name}
                            />
                            {this.state.showMandatoryFields && !this.state.last_name &&
                            <Text style={styles.form.mandatory}>
                                This field is required.
                            </Text>
                            }
                        </View>

                        {/** Gender **/}
                        <View style={styles.form.card_line}>
                            <RadioBox
                                radioStyle={[styles.form.gender]}
                                items={constants.GENDERS}
                                selectedValue={this.state.gender}
                                onSelect={key => {
                                    this.setState({gender: key});
                                }}
                            />
                        </View>

                        {/** DOB **/}
                        <View style={styles.form.card_line}>
                            <FloatingLabelText
                                label="Date of Birth"
                                refInput={ref => {
                                    this.dob = ref;
                                }}
                                mask={'[00]/[00]/[0000]'}
                                placeholderTextColor="rgb(125,125,125)"
                                returnKeyType="next"
                                keyboardType="phone-pad"
                                underlineColorAndroid="transparent"
                                autoCorrect={true}
                                style={[
                                    styles.form.base,
                                    styles.form.text_input,
                                    styles.form.dob.text
                                ]}
                                onChangeText={dob => {
                                    this.setState({dob: dob});
                                }}
                                value={this.state.dob}
                            />
                        </View>

                        {/** Address 1 **/}
                        <View style={styles.form.card_line}>
                            <FloatingLabelText
                                label="Address 1"
                                _placeholder="Address 1"
                                placeholderTextColor="rgb(125,125,125)"
                                returnKeyType="next"
                                underlineColorAndroid="transparent"
                                autoCorrect={false}
                                style={[
                                    styles.form.base,
                                    styles.form.text_input,
                                    styles.form.address1
                                ]}
                                onChangeText={address1 => {
                                    this.setState({address1: address1});
                                }}
                                value={this.state.address1}
                            />
                        </View>

                        {/** Address 2 **/}
                        <View style={styles.form.card_line}>
                            <FloatingLabelText
                                label="Address 2"
                                placeholderTextColor="rgb(125,125,125)"
                                returnKeyType="next"
                                underlineColorAndroid="transparent"
                                autoCorrect={false}
                                style={[
                                    styles.form.base,
                                    styles.form.text_input,
                                    styles.form.address2
                                ]}
                                onChangeText={address2 => {
                                    this.setState({address2: address2});
                                }}
                                value={this.state.address2}
                            />
                        </View>

                        {/** State **/}
                        <View style={styles.form.card_line}>
                            <ListModal
                                placeholder="Select State"
                                // floatingTextStyle={{fontSize: 12}}
                                labelContainerStyle={styles.form.state_label}
                                labelStyle={{fontSize: 13.5, color: '#444', opacity: 1}}
                                data={StateStore.allStates()}
                                multiselectable={false}
                                selectedValue={this.state.state}
                                onConfirm={async (itemValue) => {
                                    itemValue[0] &&
                                    await this.setState({
                                        state: itemValue[0],
                                        county: ''
                                    });
                                }}
                            />
                        </View>

                        {/** County **/}
                        <View style={styles.form.card_line}>
                            <ListModal
                                placeholder="Select County - Select State First"
                                // floatingTextStyle={{fontSize: 12}}
                                labelContainerStyle={styles.form.state_label}
                                labelStyle={{fontSize: 13.5, color: '#444', opacity: 1}}
                                data={StateStore.allCounties(this.state.state)}
                                multiselectable={false}
                                selectedValue={this.state.county}
                                onConfirm={async (itemValue) => {
                                    itemValue[0] && await this.setState({county: itemValue[0]});
                                }}
                            />
                        </View>

                        {/** City **/}
                        <View style={styles.form.card_line}>
                            <FloatingLabelText
                                label="City"
                                placeholderTextColor="rgb(125,125,125)"
                                returnKeyType="next"
                                underlineColorAndroid="transparent"
                                autoCorrect={false}
                                style={[
                                    styles.form.base,
                                    styles.form.text_input,
                                    styles.form.city
                                ]}
                                onChangeText={city => {
                                    this.setState({city: city});
                                }}
                                value={this.state.city}
                            />
                        </View>

                        {/** Zip **/}
                        <View style={styles.form.card_line}>
                            <FloatingLabelText
                                label="Zip Code"
                                placeholderTextColor="rgb(125,125,125)"
                                returnKeyType="next"
                                underlineColorAndroid="transparent"
                                autoCorrect={false}
                                style={[
                                    styles.form.base,
                                    styles.form.text_input,
                                    styles.form.zip
                                ]}
                                onChangeText={zip => {
                                    this.setState({zip: zip});
                                }}
                                value={this.state.zip}
                            />
                        </View>

                        {/** Mobile Phone **/}
                        <View style={styles.form.card_line}>
                            <FloatingLabelText
                                label="Mobile Phone"
                                refInput={ref => {
                                    this.mobile_phone = ref;
                                }}
                                mask={'([000]) [000]-[0000]'}
                                placeholderTextColor="rgb(125,125,125)"
                                returnKeyType="next"
                                keyboardType="phone-pad"
                                underlineColorAndroid="transparent"
                                autoCorrect={true}
                                style={[
                                    styles.form.base,
                                    styles.form.text_input,
                                    styles.form.mobile_phone
                                ]}
                                onChangeText={mobile_phone => {
                                    this.setState({mobile_phone: mobile_phone});
                                }}
                                value={this.state.mobile_phone}
                            />
                            {this.state.showMandatoryFields && !this.state.mobile_phone &&
                            <Text style={styles.form.mandatory}>
                                This field is required.
                            </Text>
                            }
                        </View>

                        {/** Home Phone **/}
                        <View style={styles.form.card_line}>
                            <FloatingLabelText
                                label="Home Phone"
                                refInput={ref => {
                                    this.home_phone = ref;
                                }}
                                mask={'([000]) [000]-[0000]'}
                                textContentType="telephoneNumber"
                                placeholderTextColor="rgb(125,125,125)"
                                returnKeyType="next"
                                keyboardType="phone-pad"
                                underlineColorAndroid="transparent"
                                autoCorrect={false}
                                style={[
                                    styles.form.base,
                                    styles.form.text_input,
                                    styles.form.home_phone
                                ]}
                                onChangeText={home_phone => {
                                    this.setState({home_phone: home_phone});
                                }}
                                value={this.state.home_phone}
                            />
                            {this.state.showMandatoryFields && !this.state.home_phone &&
                            <Text style={styles.form.mandatory}>
                                This field is required.
                            </Text>
                            }
                        </View>

                        {/** Bio **/}
                        <View style={[styles.form.card_line, {height: 80}]}>
                            <FloatingLabelText
                                label="Bio(Enter background and interests - Optional)"
                                placeholderTextColor="rgb(125,125,125)"
                                underlineColorAndroid="transparent"
                                multiline={true}
                                numberOfLines={3}
                                editable={true}
                                containerHeight={60}
                                style={[styles.form.base, styles.form.bio]}
                                onChangeText={bio => {
                                    this.setState({bio: bio});
                                }}
                                value={this.state.bio}
                            />
                        </View>
                    </View>

                    {/** Licensure cards **/}
                    <View style={[styles.license_form.base, styles.section_block]}>

                        {/** Licensure Cards Title **/}
                        <View style={[styles.license_form.desc.base, styles.form.base, styles.form.desc.title,
                            {flexDirection: 'row'}]}>
                            <Text style={[styles.form.base, styles.form.desc.title]}>
                                Enter Licensure Data
                            </Text>
                        </View>

                        <View style={[styles.license_form.desc.base,
                            {flexDirection: 'row'}]}>
                            <Text style={styles.license_form.desc.title}>
                                How many states you want to work in?
                            </Text>

                            <Picker
                                style={styles.license_form.desc.picker}
                                itemStyle={{fontSize: 12}}
                                selectedValue={this.state.licensure_cardsCount}
                                onValueChange={this.changeLicensureCardsCount}
                                iosHeader="Select Count"
                            >
                                <Picker.Item label="1" value={1}/>
                                <Picker.Item label="2" value={2}/>
                                <Picker.Item label="3" value={3}/>
                            </Picker>
                        </View>
                        {<FlatList
                            style={styles.license_cards.base}
                            data={this.state.licensure_cards}
                            renderItem={({item, index}) => {
                                return (
                                    <LicensureCard
                                        key={`LicensureCard_${index}`}
                                        licensure_card={item}
                                        index={index}
                                        onChangeLicensureCard={async (item1, index1) => {
                                            let _cards = this.state.licensure_cards;
                                            _cards[index1] = item1;
                                            await this.setState({
                                                licensure_cards: _cards
                                            });

                                        }}
                                    />
                                );
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />}
                    </View>

                    {/** Upload picture/resume **/}
                    <View style={[styles.form.upload_view.base, styles.section_block]}>
                        <View style={styles.form.upload_view.buttons}>
                            <Text style={styles.form.upload_view.title}>
                                Upload Documents
                            </Text>
                        </View>
                        <View style={{backgroundColor: 'white', padding: 10}}>
                            <View>
                                <View style={styles.form.upload_view.buttons}>
                                    <Button
                                        buttonStyle={styles.form.upload_view.button_picture}
                                        textStyle={styles.form.upload_view.button_picture_text}
                                        onPress={() => {

                                            // iPhone/Android
                                            DocumentPicker.show(
                                                {
                                                    filetype: [DocumentPickerUtil.images()]
                                                },
                                                (error, res) => {
                                                    // Android
                                                    res && this.setState({
                                                        upload_picture_valid: res.fileName,
                                                        upload_picture: {
                                                            uri: res.uri,
                                                            type: res.type,
                                                            fileName: res.fileName,
                                                            fileSize: res.fileSize
                                                        }
                                                    });

                                                }
                                            );
                                        }}
                                    >
                                        UPLOAD PICTURE
                                    </Button>
                                    <Text style={styles.form.upload_view.text_picture}>
                                        {this.state.upload_picture && this.state.upload_picture.fileName}
                                    </Text>
                                </View>
                                <View>
                                    {this.state.showMandatoryFields && !this.state.upload_picture.fileName &&
                                    <Text style={styles.form.mandatory}>
                                        This field is required.
                                    </Text>
                                    }
                                </View>
                            </View>

                            <View>
                                <View style={styles.form.upload_view.buttons}>
                                    <Button
                                        buttonStyle={styles.form.upload_view.button_resume}
                                        textStyle={styles.form.upload_view.button_resume_text}
                                        onPress={() => {
                                            // iPhone/Android
                                            DocumentPicker.show(
                                                {
                                                    filetype: [DocumentPickerUtil.pdf()]
                                                },
                                                (error, res) => {
                                                    // Android
                                                    res && this.setState({
                                                        upload_resume_valid: res.fileName,
                                                        upload_resume: {
                                                            uri: res.uri,
                                                            type: res.type,
                                                            fileName: res.fileName,
                                                            fileSize: res.fileSize
                                                        }
                                                    });

                                                }
                                            );
                                        }}
                                    >
                                        UPLOAD RESUME
                                    </Button>
                                    <Text style={styles.form.upload_view.text_resume}>
                                        {this.state.upload_resume && this.state.upload_resume.fileName}
                                    </Text>
                                </View>
                                <View>
                                    {this.state.showMandatoryFields && !this.state.upload_resume.fileName &&
                                    <Text style={styles.form.mandatory}>
                                        This field is required.
                                    </Text>
                                    }

                                </View>
                            </View>
                        </View>
                    </View>

                    {/** Login credentials **/}
                    <View style={styles.section_block}>
                        <View style={styles.form.card_line}>
                            <Text style={styles.login_title}>Create Login Credentials</Text>
                        </View>
                        <View style={styles.form.card_line}>
                            <FloatingLabelText
                                label="Email"
                                style={[
                                    styles.form.base,
                                    styles.form.text_input,
                                    styles.form.email
                                ]}
                                placeholderTextColor="rgb(125,125,125)"
                                returnKeyType="next"
                                keyboardType="email-address"
                                underlineColorAndroid="transparent"
                                autoCorrect={false}
                                onChangeText={email => {
                                    this.setState({email: email});
                                }}
                                value={this.state.email}
                            />
                            {this.state.showMandatoryFields && !this.state.email &&
                            <Text style={styles.form.mandatory}>
                                This field is required.
                            </Text>
                            }
                        </View>
                        <View style={styles.form.card_line}>
                            <FloatingLabelText
                                label="Password"
                                style={[
                                    styles.form.base,
                                    styles.form.text_input,
                                    styles.form.password
                                ]}
                                _placeholder="Password"
                                placeholderTextColor="rgb(125,125,125)"
                                returnKeyType="next"
                                underlineColorAndroid="transparent"
                                autoCorrect={false}
                                onChangeText={password => {
                                    this.setState({password: password});
                                }}
                                value={this.state.password}
                                secureTextEntry
                            />
                            {this.state.showMandatoryFields && !this.state.password &&
                            <Text style={styles.form.mandatory}>
                                This field is required.
                            </Text>
                            }
                        </View>
                        <View style={styles.form.card_line}>
                            <FloatingLabelText
                                label="Confirm Password"
                                style={[
                                    styles.form.base,
                                    styles.form.text_input,
                                    styles.form.password_confirmation
                                ]}
                                _placeholder="Password Confirmation"
                                placeholderTextColor="rgb(125,125,125)"
                                returnKeyType="next"
                                underlineColorAndroid="transparent"
                                autoCorrect={false}
                                onChangeText={password_confirmation => {
                                    this.setState({
                                        password_confirmation: password_confirmation
                                    });
                                }}
                                value={this.state.password_confirmation}
                                secureTextEntry
                            />
                            {this.state.showMandatoryFields && !this.state.password_confirmation &&
                            <Text style={styles.form.mandatory}>
                                This field is required.
                            </Text>
                            }
                        </View>
                        <View style={styles.form.card_line}>
                            <Text style={styles.login_desc}>
                                Minimum 8 Characters - Case sensitive
                            </Text>
                        </View>
                    </View>
                    <View style={{marginTop: 20, marginBottom: 150, flexDirection: 'row'}}>
                        <Button
                            disabled={true}
                            buttonStyle={[
                                styles.nav_button,
                                styles.disabled
                            ]}
                            textStyle={[styles.nav_button_text]}
                        >
                            Previous
                        </Button>
                        <Button
                            disabled={this.state.inProcessing}
                            loading={this.state.inProcessing}
                            buttonStyle={[
                                styles.nav_button
                            ]}
                            textStyle={[styles.nav_button_text]}
                            onPress={() => {
                                this.handleSaveStep1();
                                Keyboard.dismiss();
                            }}
                        >
                            Next
                        </Button>
                        <Button
                            disabled={true}
                            buttonStyle={[
                                styles.nav_button,
                                styles.disabled
                            ]}
                            textStyle={[styles.nav_button_text]}
                        >
                            Finish
                        </Button>
                    </View>
                    {/**/}
                </ScrollView>
                }
            </View>
        );
    }
}

export default Step1;

