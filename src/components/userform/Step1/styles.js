import theme from '../../../theme/default';

const styles = {
    form: {

        /**
         * Common Styles for the form
         * */
        base: {
            color: theme.defaultTextColor,
        },
        card_line: {
            flexDirection: 'column',
            marginBottom: 10,
            paddingHorizontal: 6,
        },
        text_input: {
            borderBottomWidth: theme.borderWidth,
            borderBottomColor: theme.borderColor,
        },

        container: {
            backgroundColor: 'white',
            marginTop: 40,
            padding: 10
        },
        desc: {
            base: {
                flexDirection: 'row',
            },
            content: {
                fontSize: theme.fontSizeMedium,
                flex: 1, flexWrap: 'wrap',
            },
            title: {
                fontSize: theme.fontSizeMedium,
                fontWeight: theme.fontWeightBold,
                color: theme.defaultTextColor,
                lineHeight: theme.lineHeightMedium,
                textAlign: 'justify',
            }
        },
        first_name: {},
        last_name: {},
        gender: {
            height: 60
        },
        dob: {
            container: {},
            text: {
                fontSize: theme.fontSizeSmall,
                color: theme.defaultTextColor,
            },
            smallLabel: {
                opacity: 0.6,
                fontSize: theme.fontSizeMini,
            }
        },
        address1: {},
        address2: {},
        state: {
            opacity: 0.6,
            fontSize: theme.fontSizeMini,
        },
        state_label: {
            borderBottomWidth: theme.borderWidth,
            borderBottomColor: theme.borderColor,
        },
        state_label_text: {
            fontSize: theme.fontSizeSmall,
            color: theme.defaultTextColor,
        },
        state_label_floating: {
            opacity: 0.6,
            fontSize: theme.fontSizeMini,
        },
        county: {
            color: theme.defaultTextColor,
            borderColor: theme.borderColor,
            borderWidth: theme.borderWidth,
            height: 35,
            fontSize: theme.fontSizeSmall,
        },
        city: {},
        zip: {},
        mobile_phone: {},
        home_phone: {},
        bio: {
            paddingTop: 5,
            height: 60,
            lineHeight: 18,
            textAlignVertical: 'top',
            borderBottomWidth: theme.borderWidth,
            borderBottomColor: theme.borderColor,
        },
        licensure_card: {},
        upload_view: {
            base: {
                flexDirection: 'column',
                backgroundColor: 'transparent'
            },
            header: {},
            title: {
                fontSize: theme.fontSizeMedium,
                fontWeight: theme.fontWeightBold,
                color: theme.defaultTextColor,
                lineHeight: theme.lineHeightMedium,
                textAlign: 'justify',
            },
            buttons: {
                width: '100%',
                flexDirection: 'row',
                backgroundColor: 'transparent'
            },
            button_picture: {
                width: '40%',
                height: 25,
                paddingTop: 2,
                paddingBottom: 2,
                borderWidth: 0,
                flex: 1,
                alignSelf: 'stretch',
                backgroundColor: 'rgb(65, 178, 171)'
            },
            button_picture_text: {
                height: 25,
                lineHeight: 20,
                textAlign: 'center',
                fontWeight: '500',
                fontSize: 12,
                color: '#fff'
            },
            text_picture: {
                width: '60%',
                borderBottomWidth: 1,
                borderBottomColor: '#6b6b6b',
                padding: 0,
                marginLeft: 5
            },
            button_resume: {
                width: '40%',
                height: 25,
                marginTop: 10,
                paddingTop: 2,
                paddingBottom: 2,
                borderWidth: 0,
                flex: 1,
                alignSelf: 'stretch',
                marginLeft: 'auto',
                backgroundColor: 'rgb(65, 178, 171)'
            },
            button_resume_text: {
                height: 25,
                lineHeight: 20,
                textAlign: 'center',
                fontWeight: '500',
                fontSize: 12,
                color: '#fff'
            },
            text_resume: {
                width: '60%',
                marginTop: 10,
                borderBottomWidth: 1,
                borderBottomColor: '#6b6b6b',
                padding: 0,
                marginLeft: 5
            }
        },
        email: {},
        password: {},
        password_confirmation: {},
        mandatory: {
            color: 'red',
            fontSize: 12
        }
    },
    input: {
        base: {
            backgroundColor: 'rgba(255,255,255,0.5)',
            height: 40,
            marginBottom: 10,
            color: 'rgba(0,0,0,0.75)',
            paddingHorizontal: 10
        },
        first_name: {}
    },
    login_title: {
        fontSize: theme.fontSizeMedium,
        fontWeight: theme.fontWeightBold,
        color: theme.defaultTextColor,
        lineHeight: theme.lineHeightMedium,
        textAlign: 'justify',
    },
    login_desc: {
        height: 30,
        lineHeight: 30,
        fontSize: 12
    },
    license_form: {
        base: {
            width: '100%',
            marginTop: 2,
        },
        desc: {
            base: {
                flexDirection: 'row',
                marginBottom: 5
            },
            title: {
                height: 35,
                lineHeight: 35,
                marginRight: 'auto'
            },
            picker: {
                width: 80,
                height: 35,
            }
        }
    },
    license_cards: {
        base: {},
        card_view: {
            base: {
                width: '100%',
                marginLeft: 'auto',
                marginRight: 'auto'
            },
            title: {fontSize: 16, color: 'green'},
            line1: {
                base: {
                    width: '50%'
                }
            },
            line2: {
                base: {
                    flexDirection: 'row'
                },
                state: {
                    marginLeft: 'auto',
                    width: '50%'
                },
                license_no: {
                    marginTop: 10,
                    width: '50%',
                    height: 30,
                    paddingVertical: 0,
                    borderBottomWidth: 1,
                    borderColor: 'lightgray'
                }
            }
        }
    },

    multi_counties: {
        backgroundColor: 'red',
        height: 50
    },

    section_block: {
        borderWidth: theme.borderWidth,
        borderColor: theme.borderColor,
        borderRadius: 2,
        /*shadowColor: '#444',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 1,*/
        elevation: 2,
        padding: 10,
        marginTop: 20,
    },

    step2_states: {
        base: {
            borderRadius: 4,
            borderWidth: 1,
            borderColor: 'lightgray',
            marginTop: 10,
            backgroundColor: 'transparent',
        },
        title: {
            paddingHorizontal: 5,
            paddingVertical: 5,
            flexDirection: 'row',
        },
        right_arrow: {
            marginLeft: 'auto',
            width: 20,
            height: 20,
            borderRadius: 20,
            color: 'darkgray',
            textAlign: 'center',
            backgroundColor: 'rgba(192,192,192, 0.2)'
        },
        selected_counties: {
            paddingHorizontal: 5,
            paddingVertical: 5,
        },
        select_states_button: {
            container: {
                width: '100%',
                height: 30,
                flex: 1,
                flexDirection: 'row',
                position: 'absolute',
                bottom: 17,
                left: 0,
                zIndex: 1000,
                paddingHorizontal: 5
            },
            base: {
                width: '50%',
                height: 30,
                backgroundColor: 'rgb(78, 179, 172)',
                paddingHorizontal: 10,
                marginLeft: 'auto'
            },
            text: {
                lineHeight: 30,
                color: 'white',
                textAlign: 'center',
                fontSize: 14,
                paddingVertical: 0,
                paddingHorizontal: 0
            }
        }
    },


    nav_button: {
        backgroundColor: 'rgb(78, 179, 172)',
        width: '33.3333%',
        height: 30,
    },
    nav_button_text: {
        lineHeight: 30,
        color: 'white',
        textAlign: 'center',
        fontSize: 14,
        paddingHorizontal: 0
    },
    disabled: {
        backgroundColor: 'rgb(192, 192, 192)'
    },
};

export default styles;
