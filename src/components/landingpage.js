/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {ActivityIndicator, Platform, StatusBar, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import SessionStore from '../store/SessionStore';



export class LandingPage extends Component {

    componentDidMount(): void {
        SessionStore.isLoggedIn()
            .then(async (loggedData) => {
                if (!loggedData.isLoggedIn) {
                    Actions.login({userAction: true});
                }
            });
    }

    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignSelf: 'center',
            }}>

                <ActivityIndicator animating={true} size='large'/>

            </View>

        );
    }

}

module.exports = LandingPage;
