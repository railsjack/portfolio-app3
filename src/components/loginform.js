import React, {Component} from 'react';
import {loginUser} from '../actions/AuthActions';
import {FloatingLabelText} from './userform/_CustomViews';
import {Dimensions, Image, Keyboard, Platform, StatusBar, TouchableOpacity, View} from 'react-native';
import {Button, Container, Icon, Text} from 'native-base';
import theme from '../theme/default';
import {Actions} from 'react-native-router-flux';

const cutCover = ((Dimensions.get('window').width * 1.7777777777) - Dimensions.get('window').height) / 2;

const launchscreenBg = require('../assets/images/All-Link-Landing.jpg');

class LoginForm extends Component {
    state = {email: null, password: null};

    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            keyboardShown: false
        };
        if (Platform.OS === 'android') {
            StatusBar.setHidden(true);
        }
    }

    componentDidMount() {
        if (Platform.OS === 'ios') {
            this.keyboardDidShowListener = Keyboard.addListener(
                'keyboardDidShow',
                this._keyboardDidShow.bind(this),
            );
            this.keyboardDidHideListener = Keyboard.addListener(
                'keyboardDidHide',
                this._keyboardDidHide.bind(this),
            );
        }
    }

    componentWillUnmount() {
        if (Platform.OS === 'ios') {
            this.keyboardDidShowListener.remove();
            this.keyboardDidHideListener.remove();
        }
    }

    async _keyboardDidShow() {
        await this.setState({keyboardShown: true});
    }

    async _keyboardDidHide() {
        await this.setState({keyboardShown: false});
    }


    onLogin() {
        loginUser(this.state);
    }


    render() {

        const {email, password} = this.state;
        const floatingInputHeight = 65;
        return (
            <Container>
                <StatusBar barStyle="light-content"/>
                <Image
                    style={{
                        flex: 1,
                        width: '100%',
                        height: '100%',
                        position: 'absolute',
                        top: cutCover,
                        zIndex: -1,
                        resizeMode: 'cover',
                    }}
                    source={launchscreenBg}/>
                <View>
                    <View style={{
                        width: 280,
                        height: (this.state.keyboardShown ? '65%' : '100%'),
                        flexDirection: 'column',
                        justifyContent: 'flex-end',
                        alignSelf: 'center',
                    }}>
                        <View style={{
                            borderColor: 'green', borderWidth: 0,
                            width: '100%', height: floatingInputHeight,
                        }}>
                            <View style={{
                                width: '100%', height: floatingInputHeight - 30,
                                backgroundColor: 'white',
                                borderRadius: 5,
                                borderWidth: 1,
                                borderColor: theme.borderColor,
                                position: 'absolute', zIndex: 1, bottom: 0
                            }}/>
                            <Icon name='ios-mail' style={{
                                position: 'absolute', zIndex: 2,
                                left: 7, bottom: 7,
                                fontSize: 22, color: theme.lightGrayBgColor,
                            }}/>
                            <FloatingLabelText
                                label="Email Address"
                                autoCorrect={false}
                                autoCapitalize='none'
                                keyboardType='email-address'
                                placeholderTextColor="black"
                                returnKeyType='next'
                                onChangeText={async text => {
                                    await this.setState({email: text});
                                }}
                                value={email}
                                containerStyle={{
                                    width: '100%', height: floatingInputHeight - 10,
                                    position: 'absolute', zIndex: 3, bottom: 0
                                }}
                                style={{
                                    width: '100%', height: floatingInputHeight - 18,
                                    paddingLeft: 30,
                                }}
                                animationTop={floatingInputHeight - 37}
                                animationLeft={33}
                            />
                        </View>
                        <View style={{
                            width: '100%', height: floatingInputHeight - 5,
                        }}>
                            <View style={{
                                width: '100%', height: floatingInputHeight - 30,
                                backgroundColor: 'white',
                                borderRadius: 5,
                                borderWidth: 1,
                                borderColor: theme.borderColor,
                                position: 'absolute', zIndex: 1, bottom: 0
                            }}/>
                            <Icon name='ios-key' style={{
                                position: 'absolute', zIndex: 2,
                                left: 7, bottom: 7,
                                fontSize: 22, color: theme.lightGrayBgColor,
                            }}/>
                            <FloatingLabelText
                                autoCorrect={false}
                                label="Password"
                                placeholderTextColor="black"
                                secureTextEntry
                                returnKeyType='next'
                                onChangeText={async text => this.setState({password: text})}
                                value={password}
                                containerStyle={{
                                    width: '100%', height: floatingInputHeight - 10,
                                    position: 'absolute', zIndex: 3, bottom: 0
                                }}
                                style={{
                                    width: '100%', height: floatingInputHeight - 18,
                                    paddingLeft: 30,
                                }}
                                animationTop={floatingInputHeight - 37}
                                animationLeft={33}
                            />
                        </View>

                        <View style={{
                            flexDirection: 'row', justifyContent: 'space-between',
                            paddingHorizontal: 5,
                            width: '100%',
                            height: 30,
                            paddingTop: 10,
                        }}>
                            <TouchableOpacity
                                onPress={() => Actions.signup()}
                            >
                                <Text style={{
                                    fontSize: 14
                                }}>Signup</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => Actions.forgotPassword()}
                            >
                                <Text style={{
                                    fontSize: 14
                                }}>Forgot Password?</Text>
                            </TouchableOpacity>
                        </View>

                        <Button
                            block onPress={this.onLogin.bind(this)}
                            style={{
                                backgroundColor: '#444', height: 33, marginTop: 25,
                                marginBottom: 30,
                            }}
                        >
                            <Text>Login</Text>
                        </Button>
                    </View>
                </View>
            </Container>


        );
    }
}


export default LoginForm;
