import React, {Component} from 'react';
import {Platform} from 'react-native';
import {Button, Container, Content, Header, Icon, Input, Item, Left, ListItem, Right, Text} from 'native-base';
import {FlatList} from 'react-native';
import styles from './styles';
import {Actions} from 'react-native-router-flux';
import theme from '../../theme/default';
import HistoryStore from '../../store/HistoryStore';

const logs = [];

class History extends Component {
    state = {logs, loaded: false};
    baseLogs = logs;

    constructor(props) {
        super(props);
    };

    componentDidMount(): void {
        HistoryStore.getLogs()
            .then(logs => {
                this.setState({logs: logs, loaded: true});
                this.baseLogs = logs;
            })
            .catch(error => {
            });
    }

    _handlePress = (log) => {
        Actions.historyDetail({log: log, title: `Detail for ${log.patient_name}`});
    };

    searchFilterFunction = text => {
        let logs = HistoryStore.search(text, this.baseLogs);
        this.setState({logs: logs});
    };

    render() {
        return (
            <Container style={styles.container}>
                <Header androidStatusBarColor={theme.defaultBgColor}
                        iosBarStyle="light-content"
                        style={styles.header} searchBar rounded>
                    <Item>
                        <Icon active name="search"/>
                        <Input placeholder="Search by name or location" autoCorrect={false}
                               style={Platform.OS === 'android' ? {
                                   borderWidth: theme.borderWidth,
                                   borderColor: theme.borderColorLight,
                                   borderRadius: 50,
                                   height: 35, padding: 0,
                                   paddingLeft: 15,
                               }: {} }
                               onChangeText={text => this.searchFilterFunction(text)}
                        />
                        <Icon active name="people"/>
                    </Item>
                    <Button transparent>
                        <Text>Search</Text>
                    </Button>
                </Header>
                <Content>
                    {this.state.loaded && this.state.logs &&
                    <FlatList
                        data={this.state.logs}
                        extraData={this.state}
                        keyExtractor={(item, index) => String(index)}
                        renderItem={({item}) => {
                            return (
                                <ListItem
                                    onPress={this._handlePress.bind(this, item)}
                                >
                                    <Left style={{flexDirection: 'column'}}>
                                        <Text style={{textAlign: 'left', width: '100%'}}>
                                            Patient name: {item.patient_name}
                                        </Text>
                                        <Text style={{textAlign: 'left', width: '100%'}}>
                                            Visited at: {item.visited_at}
                                        </Text>
                                        <Text style={{textAlign: 'left', width: '100%'}}>
                                            Location: {item.location}
                                        </Text>
                                    </Left>
                                    <Right>
                                        <Icon name="arrow-forward"/>
                                    </Right>
                                </ListItem>
                            );
                        }}
                    />
                    }
                </Content>
            </Container>
        );
    }
}

export default History;
