import React, {Component} from 'react';
import {Body, Card, CardItem, Container, Content, Left, Text} from 'native-base';

import styles from './styles';

class HistoryDetail extends Component {

    constructor(props){
        super(props);
        this.props = props;
    }

    render() {
        const {log} = this.props;
        return (
            <Container style={styles.container}>
                <Content padder>
                    <Card style={styles.mb}>
                        <CardItem bordered>
                            <Left>
                                <Body>
                                <Text>Patient name: {log.patient_name}</Text>
                                <Text note>Visited at: {log.visited_at}</Text>
                                </Body>
                            </Left>
                        </CardItem>

                        <CardItem bordered>

                            <Body>
                            <Text>
                                Location: {log.location}
                            </Text>
                            </Body>

                        </CardItem>

                        <CardItem bordered>
                            <Body>
                            <Text>
                                Note:
                            </Text>
                            <Text>
                                {log.note}
                            </Text>
                            </Body>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}

export default HistoryDetail;
