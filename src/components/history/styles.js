import theme from '../../theme/default';

export default {
  container: {
    backgroundColor: "#FFF"
  },
  header: {
    backgroundColor: "#FFF"
  },
  item: {
    flexDirection: "row"
  }
  ,
  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  mb: {
    marginBottom: 15
  }
};
