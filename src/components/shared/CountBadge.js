// Import libraries for making a component
import React from 'react';
import { Text, View } from 'react-native';

// Make a component
const CountBadge = ({icon, count}) => {
  const { container, iconStyle,countStyle,countTextStyle } = styles;
  return (
    <View style={container}>
    {icon && <View style={iconStyle} >
         {icon}
    </View>}
    {count>0 && <View style={countStyle} >
    <Text style={countTextStyle}> {count}</Text>
    </View>}
      </View>
  );
};

const styles = {
  container: {
    height: 50, width: 50, 
    flexDirection: 'column',
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: 'red'
  },
  iconStyle:{
    width: 45, height: 40,marginTop:0
  },
  countStyle:{
    borderRadius: 10, width: 20, height: 20,  backgroundColor: '#2ecc71', position: 'absolute', top: 1,left: 33
  },
  countTextStyle: {
    textAlign: 'center',fontSize: 12, color: '#fff', fontWeight: 'bold'
  }
};

// Make the component available to other parts of the app
export { CountBadge };
