import React from 'react';
import { Icon } from "react-native-elements";
const CustomIcon = ({ name, type,size,color,containerStyle,reverseColor}) => {
  return (
    <Icon
        size={size}
        name={name}
        type={type}
        color={color}
        containerStyle={containerStyle}
        reverseColor={reverseColor}
      />  
  );
};


export { CustomIcon };
