import React from "react";
import { Text, View, Modal } from "react-native";
import { CardSection } from "./CardSection";
import { Button } from "./Button";
import Icon from "react-native-vector-icons/Ionicons";
const CustomMenu = ({ children, visible, onClose }) => {
  const { containerStyle, textStyle, cardSectionStyle } = styles;
  return (
    <Modal
      visible={visible}
      transparent
      animationType="fade"
      onRequestClose={() => {}}
    >
      <View style={containerStyle}>
        <CardSection style={cardSectionStyle}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <View style={{flex:1,marginBottom:0}} >
          {children}
          </View>
           <View style={{width: 20}} >
          <Icon
                name="md-close"
                size={15}
                style={{ marginLeft: 5 }}
                onPress={onClose}
              />
          </View>
      </View>
        </CardSection>
      </View>
    </Modal>
  );
};

const styles = {
  cardSectionStyle: {
    justifyContent: "center"
  },
  textStyle: {
    flex: 1,
    fontSize: 18,
    textAlign: "center",
    lineHeight: 40
  },
  containerStyle: {
    backgroundColor: "rgba(0, 0, 0, 0.75)",
    // backgroundColor: "#fff",
    position: 'relative',
    flex: 1,
    justifyContent: "flex-end",
  }
};

export { CustomMenu };
