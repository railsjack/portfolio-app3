import React from "react";
import { Button } from "react-native-elements";
var _ = require('lodash');
const LikeButton = ({ likeable, currentUser,onPress }) => {
  let likers= _.map(likeable.user_likes, (user_like)=>user_like.liker.id)
  let liked= _.includes(likers, currentUser.id);
  return (
    <Button
      buttonStyle={{ margin: 0, padding: 1 }}
      color={liked ? '#0abde3' : 'gray'}
      fontSize={14}
      icon={{ color: liked ? '#0abde3' : 'gray', size: 12, name: 'thumbs-up', type: 'font-awesome' }}
      title={liked ? 'Liked' : 'Like'}
      transparent
      large={false}
      onPress={onPress} />
  );
};
export { LikeButton };
