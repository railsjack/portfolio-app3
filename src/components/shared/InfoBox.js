import React from "react";
import { Text, View, Modal } from "react-native";
import { CardSection } from "./CardSection";
import { Button } from "./Button";
import Icon from "react-native-vector-icons/Ionicons";
class InfoBox extends React.Component {
  render() {
    const { containerStyle, textStyle, cardSectionStyle } = styles;
    const { children, visible, onClose } = this.props;
    return (
      <Modal
        visible={visible}
        transparent
        animationType="fade"
        onRequestClose={() => {}}
      >
        <View style={styles.content}>
          {/* START NEW CODE */}
          <View style={styles.messageBox}>
            <View style={{ marginRight: 5, flex:1 }}>
              <Text style={styles.messageBoxBodyText}>{children}</Text>
            </View>
            <View style={{ alignItems: "flex-end", padding: 5 }}>
              <Icon
                name="md-close"
                size={15}
                containerStyle={{
                  marginTop: 0,
                  marginLeft: 5,
                  marginRight: 2
                }}
                color="#fff"
                onPress={onClose}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
const styles = {
  messageBox: {
    backgroundColor: "rgba(0, 0, 0, 0.60)",
    width: 300,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 10,
    flexDirection: "row"
    
  },
  messageBoxTitleText: {
    fontWeight: "bold",
    color: "#fff",
    textAlign: "center",
    fontSize: 20,
    marginBottom: 10
  },
  messageBoxBodyText: {
    color: "#fff",
    fontSize: 16
  },
  content: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  }
};
export { InfoBox };
