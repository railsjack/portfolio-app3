import React from 'react';
import { TextInput, View, Text } from 'react-native';

const TextArea = ({ label, value, onChangeText, placeholder,numberOfLines }) => {
  const { inputStyle, labelStyle, containerStyle } = styles;

  return (
    <View style={containerStyle}>
      {label &&
        <Text style={labelStyle}>{label}</Text>
      }
      <TextInput
        placeholder={placeholder}
        multiline={true}
        numberOfLines= { numberOfLines || 10}
        autoCorrect={false}
        editable = {true}
        maxLength = {500}
        style={inputStyle}
        value={value}
        onChangeText={onChangeText}
        placeholderTextColor={'#D3D3D3'}
      />
    </View>
  );
};

const styles = {
  inputStyle: {
    color: '#000',
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 14,
    flex: 3,
    textAlignVertical: "top"
  },
  labelStyle: {
    fontSize: 12,
    paddingLeft: 15,
    paddingTop: 10,
    flex: 1,
    // borderRigtRadius: 4,
    // borderRightWidth: 0.5,
    // borderRightColor: '#d6d7da',
  },
  containerStyle: {
    // height: 40,
    flex: 1,
    flexDirection: 'row',
  }
};

export { TextArea };
