import React from 'react';
import { View, Text,ActivityIndicator } from 'react-native';
const Spinner = ({ size,text }) => {
  return (
    <View style={styles.spinnerStyle}>
      <ActivityIndicator size={size || 'large'} />
      <Text>{text || ''}</Text>
    </View>
  );
};

const styles = {
  spinnerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export { Spinner };
