import React, {Component} from "react";
import {View} from "react-native";
import {CheckBox} from "react-native-elements";

class MultiCheckBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValues: {}
        };
    }

    componentDidMount(): void {
        let _selectedValues={};
        (this.props.selectedValues || []).map(value=>{
            _selectedValues[value] = true;
        })
        this.setState({
            selectedValues: _selectedValues || {}
        });

    }

    updateValue = (selectedKey) => {
        this.state.selectedValues[selectedKey] = !this.state.selectedValues[selectedKey];
        this.setState({selectedValues: this.state.selectedValues});
        this.props.onSelected && this.props.onSelected(this.state.selectedValues);
    };

    render() {
        const items = this.props.getNurseItems();
        const {
            checkedIcon,
            uncheckedIcon,
            checkedColor = "rgb(0,165,153)",
            uncheckedColor = "darkgray",
            title = "CheckBox"
        } = this.props;
        return (
            <View style={this.props.checkStyle}>
                {items &&
                    items.map((item, index) => {
                        let key, label;
                        if (typeof item === "object") {
                            key = item.id;
                            label = item.title;
                        } else {
                            key = item;
                            label = item;
                        }
                        return <CheckBox
                            checked={this.state.selectedValues && this.state.selectedValues[key] === true}
                            onPress={this.updateValue.bind(this, key)}
                            key={key}
                            checkedIcon={checkedIcon}
                            uncheckedIcon={uncheckedIcon}
                            checkedColor={checkedColor}
                            uncheckedColor={uncheckedColor}
                            title={label || title}
                            containerStyle={[
                                {
                                    backgroundColor: "transparent",
                                    paddingHorizontal: 0,
                                    paddingVertical: 0,
                                    borderWidth: 0,
                                    height: 30
                                },
                                this.props.containerStyle
                            ]}
                        />;
                    })
                }
            </View>
        );
    }
}

export {MultiCheckBox};


