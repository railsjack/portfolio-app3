import React,{Component} from 'react'
import { Text, View, Modal } from 'react-native'
import { CardSection } from './CardSection'
import { Button } from './Button'
import { CheckBox } from 'react-native-elements'
class CustomCheckbox extends Component {
  constructor (props) {
    super(props);
    this.state = {
      checked: false
    };
    // this.handleState = this.handleState.bind(this);
  }
 test(checkAll, totalSelected,totalMessages){
   if(checkAll){
     if(this.state.checked){
      console.log("1--","1")
       return false
     } else{
      console.log("2--","2")
       return true
     }
  //   if (this.state.checked){
  //     if(totalSelected< totalMessages){
  //       console.log("1--","1")
  //       return false
  //     }else if(totalSelected==totalMessages){
  //       console.log("4--","4")
  //       return true
  //     }
  //     else{
  //       console.log("2--","2")
  //       return false
  //     }
  //   }else{
  //     console.log("3--","3")
  //     return true
  //   }
  //  }else{
  //   if(totalSelected==0 && this.state.checked){
  //     console.log("6--","6")
  //     return false
  //   }else{
  //     console.log("7--","7")
  //     return this.state.checked
  //   }
  }else{
    console.log("3--","3")
    return this.state.checked
  }
 }
  render(){
  const {checked,onChecked, message,checkAll, totalSelected, totalMessages } = this.props;
  // console.log("totalSelected", totalSelected)
  return (
    <CheckBox
      component={null}
      size={20}
      iconRight={true}
      checked={this.test(checkAll,totalSelected,totalMessages)}
      onPress={() => {

        this.setState({
        checked: !this.state.checked
        })
        onChecked(this.state.checked, message.id)
        }
      }
      center
      containerStyle={{ margin: 0, padding: 0, backgroundColor: 'transparent' }} />
  )
  }
}
export { CustomCheckbox }

