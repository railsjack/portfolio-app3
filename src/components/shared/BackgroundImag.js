import React, { Component } from 'react';
import {
  StyleSheet,
  ImageBackground,
} from 'react-native';
const BackgroundImage = ({children }) =>
{
    return (
        <ImageBackground source={require('../../assets/images/bg.png')}
              style={styles.backgroundImage}
              >
              {children}

        </ImageBackground>
    )
}
const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
    },

    text: {
        textAlign: 'center',
        color: 'white',
        backgroundColor: 'rgba(0,0,0,0)',
        fontSize: 32
    }
});

export { BackgroundImage };
