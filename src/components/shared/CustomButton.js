import React from 'react';
import { Text, StyleSheet, TouchableOpacity,Keyboard } from 'react-native';
import {Icon,Button } from "react-native-elements";
const CustomButton = ({ onPress, text, disabled }) => {
  const { buttonStyle, textStyle } = styles;
  return (
    <Button
    title={text}
    titleStyle={styles.buttonText}
    buttonStyle={styles.buttonStyle}
    onPress={()=>{onPress(); Keyboard.dismiss();}}
    disabledStyle={{backgroundColor: '#9D9D9D', opacity: 0.65}}
    disabled={disabled}
    outline={true}
  />
  //   <TouchableOpacity onPress={()=>{
  //     onPress();
  //     Keyboard.dismiss();
  //     }}
  //      style={styles.buttonConainer} >
  //       <Text style={styles.buttonText}>
  //       {children}
  //       </Text>
  // </TouchableOpacity>
  );
};


const styles = StyleSheet.create({
    loginForm: {
      padding: 10
    },
    buttonStyle: {
      backgroundColor: "rgb(78, 179, 172)",
      paddingVertical: 15,
      left: '175%',
      width: 125
    },
    
    buttonText: {
      color: '#FFFFFF',
      textAlign: 'center'
    }
  
  });
  export { CustomButton };