import React, { Component } from 'react'
import { Image, StyleSheet, View, Text, ActivityIndicator,ImageBackground } from 'react-native'
import {BackgroundImage} from './BackgroundImag'

const  Overlay =(props)=> {
    const message = props.message ? props.message : 'loading...'
    return (
      <BackgroundImage source={require('../../assets/images/bg_1.png')} style={s.backgroundImage}>
        <View style={s.overlay}>
        </View>
        <View style={s.messageArea}>
          <Image source={require('../../assets/images/logo.png')}></Image>
          <View style={s.progressInidicator}>
            <View style={s.loader}>
              <ActivityIndicator size='large' color='#fff' />
            </View>
            <View>
              <Text style={s.message}>
                {message}
              </Text>
            </View>
          </View>
        </View>
      </BackgroundImage>
    )
}

const s = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    width: null,
    height: null
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: 'blue',
    opacity: 0.5
  },
  messageArea:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  progressInidicator: {
    marginTop: 10,
    flexDirection: 'row',
  },
  loader:{
    justifyContent:'center',
    alignItems:'center',
    width: 35,
  },
  message: {
    color: 'white',
    textAlign: 'left',
    fontSize: 15,
    textAlignVertical:'bottom',
    paddingTop: 3,
    paddingLeft: 5,
   
  }
})
export { Overlay };