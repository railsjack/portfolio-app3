import React from "react";
import { ListItem } from "react-native-elements";
var _ = require('lodash');
import { View, Text, } from 'react-native';
import { Actions } from "react-native-router-flux";
const LikersList = ({ users, action }) => {
  return (
    <View>
      {
        users.map((u, i) => (
          <ListItem
            hideChevron={true}
            roundAvatar
            // key={i}
            key={(user, index) => `${index.toString()}-lker-${user.id.toString()}`}
            avatar={{uri:u.profile_photo}}
            title={`${u.firstname} ${u.lastname}`}
            containerStyle={{ marginVertical: 2, }}
            onPress={()=>action(u)}
          />
        ))
      }
    </View>
  )
};
export { LikersList };
