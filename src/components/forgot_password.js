import React, {Component} from 'react';
import {resetPassword} from '../actions/AuthActions';
import {FloatingLabelText} from './userform/_CustomViews';
import {ImageBackground, Keyboard, Platform, StatusBar, View} from 'react-native';
import {Button, Container, Icon, Text} from 'native-base';
import theme from '../theme/default';

const launchscreenBg = require('../assets/images/All-Link-Landing-with-header.jpg');

class ForgotPassword extends Component {
    state = {email: null, password: null};

    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            keyboardShown: false
        };
    }

    onLogin() {
        resetPassword(this.state);
    }


    componentDidMount() {
        if (Platform.OS === 'ios'){
            this.keyboardDidShowListener = Keyboard.addListener(
                'keyboardDidShow',
                this._keyboardDidShow.bind(this),
            );
            this.keyboardDidHideListener = Keyboard.addListener(
                'keyboardDidHide',
                this._keyboardDidHide.bind(this),
            );
        }
    }

    componentWillUnmount() {
        if (Platform.OS === 'ios') {
            this.keyboardDidShowListener.remove();
            this.keyboardDidHideListener.remove();
        }
    }

    async _keyboardDidShow() {
        await this.setState({keyboardShown: true});
    }

    async _keyboardDidHide() {
        await this.setState({keyboardShown: false});
    }

    render() {

        const {email} = this.state;
        const floatingInputHeight = 65;
        return (
            <Container>
                <StatusBar barStyle="light-content"/>
                <ImageBackground
                    source={launchscreenBg}
                    style={{
                        flex: 1,
                        width: null,
                        height: null,
                    }}
                >
                    <View style={{
                        width: 280,
                        height: (this.state.keyboardShown ? '65%':'100%'),
                        flexDirection: 'column',
                        justifyContent: 'flex-end',
                        alignSelf: 'center',
                    }}>
                        <View style={{
                            borderColor: 'green', borderWidth: 0,
                            width: '100%', height: floatingInputHeight,
                        }}>
                            <View style={{
                                width: '100%', height: floatingInputHeight - 30,
                                backgroundColor: 'white',
                                borderRadius: 5,
                                borderWidth: 1,
                                borderColor: theme.borderColor,
                                position: 'absolute', zIndex: 1, bottom: 0
                            }}/>
                            <Icon name='ios-mail' style={{
                                position: 'absolute', zIndex: 2,
                                left: 7, bottom: 7,
                                fontSize: 22, color: theme.lightGrayBgColor,
                            }}/>
                            <FloatingLabelText
                                label="Email Address"
                                autoCorrect={false}
                                autoCapitalize='none'
                                keyboardType='email-address'
                                placeholderTextColor="black"
                                returnKeyType='next'
                                onChangeText={async text => {
                                    await this.setState({email: text});
                                }}
                                value={email}
                                containerStyle={{
                                    width: '100%', height: floatingInputHeight - 10,
                                    position: 'absolute', zIndex: 3, bottom: 0
                                }}
                                style={{
                                    width: '100%', height: floatingInputHeight - 18,
                                    paddingLeft: 30,
                                }}
                                animationTop={floatingInputHeight - 37}
                                animationLeft={33}
                            />
                        </View>

                        <Button
                            block onPress={this.onLogin.bind(this)}
                            style={{
                                backgroundColor: '#444', height: 33, marginTop: 25,
                                marginBottom: 30,
                            }}
                        >
                            <Text>Reset Password</Text>
                        </Button>
                    </View>
                </ImageBackground>
            </Container>


        );
    }
}


export default ForgotPassword;
