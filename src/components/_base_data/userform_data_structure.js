const consts = {
    stepFormData1: {
        first_name: '',
        last_name: '',
        gender: 'female',
        dob: '',
        address1: '',
        address2: '',
        state: '',
        county: '',
        city: '',
        zip: '',
        mobile_phone: '',
        home_phone: '',
        bio: '',
        licensure_cards: [],
        upload_resume: {},
        upload_picture: {},
        email: '',
        password: '',
        password_confirmation: '',
        formSavedToServer: false
    },
    stepFormData2: {
        states: [],
        counties: {},
        stateKey: '',
        shownCountiesList: false,
        tmpSelectedCounties: []
    },
    stepFormData3: {
        shownMap: false,
        states: [],
        counties: {},
        zipcodes: {},
        countyKey: '',
        tmpSelectedZipCodes: []
    },
    stepFormData4: {}
};
export default consts;
