import {API_URL} from '../../config'
const consts = {
    APP: {
        NAME: 'AllLink',
        USE_PRE_DATA: {
            IN_SIGNUP: false,
            IN_PATIENTS_LIST: true,
            IN_HISTORY_LIST: true,
            IN_USERSTORE: false,
        },
        NO_NEED_AUTH: {
            IN_PATIENTS_LIST: false,
            IN_ACCOUNT_PAGE: false,
        },
        END_POINT: {
            SIGNUP: {
                STEP1: API_URL + '/nurses',
                STEP2: API_URL + '/nurses/:id/step_2_a_state_selection',
                STEP3: API_URL + '/nurses/:id/step_2_territory_selection',
            },
            PATIENT: {
                LIST: API_URL + '/nurses/:id/patients',
                NOTIFICATION: API_URL + '/nurses/:nurse_id/patients/:id/notify'
            },
            HISTORY: {
                LIST: API_URL + '/nurses/:id/patients',
            },
        },
        STORAGE_KEY: {
            COUNTIES: '@AllLink_opbJZ5kBI19',
            LOCATION: '@AllLink_opbJZ5kBI20',
            LOGIN:  '@AllLink_opb341ASDFI20',
        },
        USERFORM_MODE: {
            EDIT: 'EDIT_USER_INFO',
            SIGNUP: 'SIGNUP',
        },
        USERFORM_DATA_STRUCTURE: require('./userform_data_structure').default,
        NOTIFICATION: {
            SENDER_ID: 'AllLink-4pejk7rg8'
        }
    },

    GOOGLE_MAP: {
        DEBUG_MODE: true,
        USE_PRE_DATA: false,
        API_KEY: 'AIzaSyCS_1TAevxCErr7XLQPtt8jSSBfoXzLi70',
        GEOCODE_URL: {
            GET_BY_LATLNG: 'https://maps.googleapis.com/maps/api/geocode/json?address=LAT,LNG&key=API_KEY',
            GET_BY_ZIPCODE: 'https://maps.googleapis.com/maps/api/geocode/json?address=ZIPCODE+usa&sensor=false&key=API_KEY',
            GET_BY_ADDRESS: 'https://maps.googleapis.com/maps/api/geocode/json?address=ADDRESS+usa&sensor=false&key=API_KEY',
            GET_DISTANCE: 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=FROM&destinations=TO&key=API_KEY',
        },
        ADDRESS_COMPONENT: require('../../_tmp_logs/MedNurse-GeoSearchByLatLng')
    },

    GENDERS: [
        {key: 'female', label: 'Female'},
        {key: 'male', label: 'Male'}
    ],

    PRE_DATA: {
        STEP1_INFORMATION: require('../userform/_dummy_data/information_for_step1').default,
        STEP2_INFORMATION: require('../userform/_dummy_data/information_for_step2').default,
        RESPONSE_WHEN_STEP1_SAVED: require('../userform/_dummy_data/response_when_step1_saved').default,
        RESPONSE_WHEN_STEP2_SAVED: require('../userform/_dummy_data/response_when_step2_saved').default,
        RESPONSE_WHEN_LOGGED_IN: require('../userform/_dummy_data/response_when_logged_in').default,
        PATIENTS_LIST: require('../patients/_dummy_data/patients_list').default,
        MEDICINE_LIST: require('../patients/_dummy_data/medicine_list').default,
        HISTORY_LIST: require('../history/_dummy_data/history_list').default,
        SIGNED_USER: {
            ID: 1,
            EMAIL: 'munib.averlentcorp@nxvt.com',
            PWD: 'word2pass',
            AUTH_TOKEN: 'tBMzxn27xKcuHnoF3gCB'
        }
    }
};
export default consts;
