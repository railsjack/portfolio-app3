import React, {Component} from 'react';
import LoginForm from './loginform';
import {View} from 'native-base';

class Login extends Component {
    render() {
        return (
            <View style={{backgroundColor: 'white', height: '100%'}}>
                <LoginForm/>
            </View>

        );
    }
}

export default Login;
