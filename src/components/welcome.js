import React, { Component } from 'react';
import { Text,View,KeyboardAvoidingView,ImageBackground,Image, TextInput } from 'react-native';
import { AsyncStorage } from 'react-native';

import { Card, CardSection, Input, Button, Spinner,CustomButton, BackgroundImage} from './shared';
import {loginUser} from '../actions/AuthActions'

class Welcome extends Component{
  constructor(props) {
    super(props);
    this.props = props;
    this.state = { data: AsyncStorage.getItem('user') };
  }
  render(){
   const nurse = this.props.currentNurse;
    return (
        <View  style={styles.welcomeContainer}>
          <Text>This is testing text {this.props.currentNurse}</Text>
        </View>
  );
}
}

const styles = {
    welcomeContainer:{

    }
};

export default Welcome;
