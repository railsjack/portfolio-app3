import React, {Component} from 'react';
import UserForm from '../../components/userform';
import constants from '../_base_data/constants';


class Account extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <UserForm mode={constants.APP.USERFORM_MODE.EDIT} />
        );
    }
}

export default Account;
