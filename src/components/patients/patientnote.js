import React, {Component} from 'react';
import {Alert, FlatList, Platform, TouchableOpacity} from 'react-native';
import {
    Body,
    Button,
    Card,
    CardItem,
    CheckBox,
    Container,
    Content,
    Icon,
    ListItem,
    Radio,
    Text,
    Textarea,
    View
} from 'native-base';
import theme from '../../theme/default';
import StarRating from 'react-native-star-rating';
import constants from '../_base_data/constants';
import UserStore from '../../store/UserStore';
import call from 'react-native-phone-call';

const deleteByValueFromArray = (value, in_array) => {
    return in_array.filter(item => item !== value);
};

class PatientNote extends Component {
    constructor(props) {
        super(props);
        this.state = {
            starCount: 0,
            medic_issue_medicines: [],
            medic_issue_texts: {},
            show_mandatory: false,
            timerBig: '00',
            timerSmall: '00',
        };
        this.medicine_list = this._getMedicineData();
        this.counterID = 0;
        this.timerCounter = 0;
        this.dialCall = this.dialCall.bind(this);
    };

    async componentWillMount(): void {
        this.setState({summary_no_prob: false});

        try {
            let _currentUser = await UserStore.getCurrentUser();
            await this.setState({username: `${_currentUser.last_name} ${_currentUser.first_name}`});
        } catch (e) {
        }
    }

    async componentDidMount(): void {
        this.startCounter();
    }


    startCounter = () => {
        if (this.counterID) {
            clearInterval(this.counterID);
        }

        this.counterID = setInterval(async ()=>{
            this.timerCounter ++;
            if ( this.timerCounter > 60 * 15 ) {
                clearInterval(this.counterID);
                return;
            }
            const timerBig = parseInt(this.timerCounter / 60);
            const timerSmall = (this.timerCounter % 60);
            await this.setState({
                timerBig: timerBig < 10 ? '0' + timerBig : timerBig,
                timerSmall: timerSmall < 10 ? '0' + timerSmall : timerSmall,
            })
        }, 1000);
    };

    dialCall(mobile_number) {
        const args = {
            number: mobile_number, // String value with the number to call
            prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call
        };
        call(args).catch(console.error);
    }
    _getMedicineData = () => {
        return constants.PRE_DATA.MEDICINE_LIST.medicine_data;
    };

    handleRadioSummary = async (selectedSummary) => {
        await this.setState({
            summary_no_prob: false,
            summary_health_issue: false,
            summary3: false,
            summary4: false,
            summary5: false,
            summary6: false,
            summary_misc: false,
            personal_log: '',
        });
        await this.setState(selectedSummary);
        await this.setState({canSave: this.buildData(false) !== false});
    };


    async onStarRatingPress(rating) {
        await this.setState({
            starCount: rating
        });
        await this.setState({canSave: this.buildData(false) !== false});
    }

    changeQuestionText3 = async (health_issue_text) => {
        await this.setState({health_issue_text});
        await this.setState({canSave: this.buildData(false) !== false});
    };

    handleQuestion2 = async (medic_issue_yesno) => {
        await this.setState({
            medic_issue_no: false,
            medic_issue_yes: false,
        });
        await this.setState(medic_issue_yesno);
        if (medic_issue_yesno.medic_issue_no === true) {
            await this.setState({
                medic_issue_medicines: [],
                medic_issue_texts: {},
            });
        }
        await this.setState({canSave: this.buildData(false) !== false});
    };

    handleQuestion3 = async (health_issue_yesno) => {
        await this.setState({
            health_issue_no: false,
            health_issue_yes: false,
        });
        await this.setState(health_issue_yesno);
        await this.setState({canSave: this.buildData(false) !== false});
    };


    buildData = (withSave = true) => {
        const {
            starCount,
            health_issue_no, health_issue_yes, health_issue_text,
            medic_issue_no, medic_issue_yes, medic_issue_medicines,
            general_summary,
            summary_no_prob, summary_health_issue, medic_issue_texts
        } = this.state;
        if (!starCount ||  /* If not star-rated */
            (!health_issue_no && !health_issue_yes) ||  /* not selected any health issue */
            (health_issue_yes && !health_issue_text) || /* health issue is "yes" and not entered anything on text */
            (!medic_issue_no && !medic_issue_yes) || /* not selected any medication issue */
            (medic_issue_yes && medic_issue_medicines.length === 0) ||
            /* medication issue is "yes" but not selected any medicine */
            (!summary_no_prob && !summary_health_issue) /* not selected anything in summary */
        ) {
            return false;
        }

        if (medic_issue_medicines.length > 0) {
            for (let k in medic_issue_medicines) {
                const medicine_id = medic_issue_medicines[k];
                if (!medic_issue_texts['' + medicine_id]) {
                    return false;
                }
            }
        }

        if (withSave === false) {
            return true;
        }
        return {
            questions: {
                feeling: starCount,
                health_issue: health_issue_yes,
                health_issue_text: health_issue_text,
                medic_issue: medic_issue_yes,
                medic_issue_medicines: medic_issue_medicines,
                medic_issue_texts: medic_issue_texts
            },
            general_summary: general_summary || '',
            summary: {
                summary_no_problem: summary_no_prob,
                summary_health_issue: summary_health_issue,
            },
        };

    };
    saveData = async () => {

        await this.setState({show_mandatory: false});
        let saveData = this.buildData();
        if (!saveData) {
            await this.setState({show_mandatory: true});
        }
        console.log('saveData: ', saveData);
        Alert.alert(constants.APP.NAME, 'Saving data...: ' + JSON.stringify(saveData));
    };


    checkMedicine = async (medicine, checked) => {
        let medicines = this.state.medic_issue_medicines;
        let texts = this.state.medic_issue_texts;
        !checked && medicines.push(medicine.id);
        checked && (medicines = deleteByValueFromArray(medicine.id, medicines));
        checked && (delete texts['' + medicine.id]);
        await this.setState({
            medic_issue_medicines: medicines,
            medic_issue_texts: texts,
        });

        await this.setState({canSave: this.buildData(false) !== false});
    };

    renderMedicineList = ({item, index}) => {
        const {medic_issue_medicines, medic_issue_texts, show_mandatory} = this.state;
        const checked = medic_issue_medicines.indexOf(item.id) > -1;
        const medicine = item;
        return (
            <>
                <ListItem noBorder
                          onPress={() => this.checkMedicine(medicine, checked)}
                          button key={`key_${index}`}>
                    <CheckBox
                        disabled
                        checked={checked}
                        color={theme.defaultBgColor}
                    />
                    <Body>
                    <Text>{medicine.name}</Text>
                    </Body>
                </ListItem>
                {checked &&
                <Textarea
                    value={medic_issue_texts['' + medicine.id]}
                    onChangeText={async (text) => {
                        let texts = Object.assign({}, medic_issue_texts);
                        texts['' + medicine.id] = text;
                        await this.setState({medic_issue_texts: texts});
                        await this.setState({canSave: this.buildData(false) !== false});
                    }}
                    style={{
                        marginLeft: 50,
                        borderWidth: theme.borderWidth,
                        borderColor: theme.borderColorLight,
                    }}
                />
                }
                {show_mandatory && checked && !medic_issue_texts[medicine.id] &&
                <Text style={[
                    styles.mandatory, {marginLeft: 50}
                ]}>* This field is required.</Text>
                }
            </>
        );
    };

    render() {
        const {
            starCount,
            health_issue_no, health_issue_yes, health_issue_text,
            medic_issue_no, medic_issue_yes, medic_issue_medicines,
            summary_no_prob, summary_health_issue,
            general_summary, medic_issue_texts,
            show_mandatory,
            timerSmall, timerBig
        } = this.state;

        return (
            <Container style={[styles.container, {flex: 1, flexDirection: 'column', justifyContent: 'space-between',}]}>
                <Content padder style={{backgroundColor: theme.grayBgColor}}>
                    <View style={{
                        position: 'relative',
                        height: 50,
                        marginVertical: 15
                    }}>
                        {Platform.OS === 'android' &&
                        <View style={{
                            borderRadius: 50,
                            width: 55, height: 55,
                            backgroundColor: 'transparent',
                            shadowColor: '#000',
                            shadowOffset: {
                                width: 0,
                                height: 0,
                            },
                            shadowOpacity: 0.5,
                            shadowRadius: 0,
                            elevation: 5,
                            position: 'absolute',
                            left: 20,
                            zIndex: 0,
                        }}/>
                        }
                        <TouchableOpacity
                            style={{
                            borderRadius: 50,
                            width: 50, height: 50,
                            backgroundColor: theme.redColor,
                            shadowColor: '#000',
                            shadowOffset: {
                                width: 0,
                                height: 5,
                            },
                            shadowOpacity: 0.3,
                            shadowRadius: 10,
                            elevation: 25,
                            position: 'absolute',
                            left: 20,
                            zIndex: 10,
                        }}
                            onPress={()=>this.dialCall('999')}
                        >
                            <Icon name='md-call' style={{
                                width: 50, height: 50,
                                alignSelf: 'center',
                                textAlign: 'center',
                                lineHeight: 50,
                                color: 'white'
                            }}/>
                        </TouchableOpacity>
                        <View style={{
                            position: 'absolute',
                            right: 20,
                            zIndex: 0,
                            width: 130,
                            height: 65,
                        }}>
                            <Text style={{
                                color: theme.defaultTextColor,
                                fontSize: 40,
                                lineHeight: 50,
                                fontWeight: '500',
                            }}>{timerBig} : {timerSmall}</Text>
                            <Text style={{
                                position: 'absolute', bottom: 8, left: 5,
                                color: 'gray', fontSize: 12
                            }}>
                                Minutes
                            </Text>
                            <Text style={{
                                position: 'absolute', bottom: 8, right: 5,
                                color: 'gray', fontSize: 12
                            }}>
                                Seconds
                            </Text>
                        </View>
                    </View>
                    <Text style={{
                        textAlign: 'center',
                        fontSize: 20,
                        fontWeight: 'bold',
                        marginVertical: 15,
                    }}>
                        Encounter Notes
                    </Text>
                    <Card style={{paddingBottom: 10}}>
                        {/* /////// Encounter Question /////// */}
                        <CardItem itemDivider header>
                            <Text style={{fontWeight: 'bold', fontSize: theme.fontSizeMedium}}>Encounter
                                Questions</Text>
                        </CardItem>
                        {/* Question 1*/}
                        <CardItem noBorder noIndent>
                            <Text style={styles.text}>
                                How was the patient Today?
                            </Text>
                        </CardItem>
                        <CardItem style={{
                            flexDirection: 'column',
                            borderBottomWidth: theme.borderWidth,
                            borderBottomColor: theme.borderColorLight
                        }}>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between',}}>
                                <View/>
                                <View style={{flexDirection: 'row',}}>
                                    <StarRating
                                        disabled={false}
                                        emptyStar={'ios-star-outline'}
                                        fullStar={'ios-star'}
                                        halfStar={'ios-star-half'}
                                        iconSet={'Ionicons'}
                                        maxStars={10}
                                        rating={starCount}
                                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                                        fullStarColor={theme.orangeColor}
                                        starSize={25}
                                        starStyle={{marginHorizontal: 4}}
                                    />
                                </View>
                                <View/>
                            </View>
                            {show_mandatory && !starCount &&
                            <Text style={styles.mandatory}>* This field is required.</Text>}
                        </CardItem>
                        {/* End: Question 1*/}


                        {/* Question 3*/}
                        <ListItem noBorder noIndent>
                            <Text style={styles.text}>
                                Is patient having any atypical health issues today?
                            </Text>
                        </ListItem>
                        <CardItem style={{
                            flexDirection: 'column',
                            borderBottomWidth: health_issue_yes ? 0 : theme.borderWidth,
                            borderBottomColor: theme.borderColorLight,
                        }}>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between',}}>
                                <View/>
                                <View style={{flexDirection: 'row',}}>
                                    <TouchableOpacity
                                        onPress={() => this.handleQuestion3({health_issue_no: true})}
                                        style={{flexDirection: 'row', marginRight: 50}}>
                                        <Radio
                                            color={theme.borderColor}
                                            selectedColor={theme.defaultBgColor}
                                            selected={health_issue_no}
                                            disabled
                                        />
                                        <Text style={{marginLeft: 10}}>No</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => this.handleQuestion3({health_issue_yes: true})}
                                        style={{flexDirection: 'row'}}>
                                        <Radio
                                            color={theme.borderColor}
                                            selectedColor={theme.defaultBgColor}
                                            selected={health_issue_yes}
                                            disabled
                                        />
                                        <Text style={{marginLeft: 10}}>Yes</Text>
                                    </TouchableOpacity>
                                </View>
                                <View/>
                            </View>
                            {show_mandatory && (!health_issue_no && !health_issue_yes) &&
                            <Text style={styles.mandatory}>* This field is required.</Text>}
                        </CardItem>
                        {health_issue_yes &&
                        <CardItem style={{
                            flexDirection: 'column',
                            paddingTop: 0,
                            paddingBottom: 10,
                            paddingHorizontal: 0,
                            borderBottomWidth: theme.borderWidth,
                            borderBottomColor: theme.borderColorLight,
                        }}>
                            <Content style={{width: '100%'}}>
                                <Textarea onChangeText={this.changeQuestionText3}
                                          value={health_issue_text} rowSpan={3}
                                          bordered/>
                            </Content>
                            {show_mandatory && (health_issue_yes && !health_issue_text) &&
                            <Text style={styles.mandatory}>* This field is required.</Text>}
                        </CardItem>
                        }
                        {/* End: Question 3*/}


                        {/* Question 2*/}
                        <ListItem noBorder noIndent>
                            <Text style={styles.text}>
                                Have there been any issues with taking medication?
                            </Text>
                        </ListItem>
                        <CardItem style={{flexDirection: 'column'}}>
                            <View style={{
                                padding: 0,
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                height: 25,
                            }}>
                                <View/>
                                <View style={{flexDirection: 'row',}}>
                                    <TouchableOpacity
                                        onPress={() => this.handleQuestion2({medic_issue_no: true})}
                                        style={{flexDirection: 'row', marginRight: 50}}>
                                        <Radio
                                            color={theme.borderColor}
                                            selectedColor={theme.defaultBgColor}
                                            selected={medic_issue_no}
                                            disabled
                                        />
                                        <Text style={{marginLeft: 10}}>No</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => this.handleQuestion2({medic_issue_yes: true})}
                                        style={{flexDirection: 'row'}}>
                                        <Radio
                                            color={theme.borderColor}
                                            selectedColor={theme.defaultBgColor}
                                            selected={medic_issue_yes}
                                            disabled
                                        />
                                        <Text style={{marginLeft: 10}}>Yes</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {show_mandatory && (!medic_issue_no && !medic_issue_yes) &&
                            <Text style={styles.mandatory}>* This field is required.</Text>}
                        </CardItem>
                        {medic_issue_yes &&
                        <CardItem
                            style={{
                                flexDirection: 'column',
                                marginTop: 0,
                                paddingTop: 0,
                                paddingHorizontal: 0,
                                marginHorizontal: 0,
                                paddingBottom: 10,
                            }}>
                            <Text style={{marginBottom: 10}}>Which medicine is Missing?</Text>
                            {/* medicine_list */}
                            <View style={{
                                borderWidth: theme.borderWidth,
                                borderColor: theme.borderColorLight,
                                width: '100%', minHeight: 30,
                                padding: 10, paddingVertical: 5,
                                borderRadius: 5,
                            }}>
                                {this.medicine_list &&
                                <FlatList
                                    data={this.medicine_list}
                                    initialNumToRender={this.medicine_list.length}
                                    maxToRenderPerBatch={this.medicine_list.length}
                                    windowSize={this.medicine_list.length}
                                    extraData={this.state}
                                    keyExtractor={(item, index) => String(index)}
                                    renderItem={this.renderMedicineList}
                                />
                                }
                            </View>
                            {show_mandatory && (medic_issue_yes && (!medic_issue_medicines ||
                                medic_issue_medicines.length === 0)) &&
                            <Text style={styles.mandatory}>* This field is required.</Text>}
                        </CardItem>
                        }

                        {/* End: Question 2*/}


                    </Card>

                    {/* General Summary */}
                    <Card style={{paddingBottom: 10, marginTop: 30,}}>
                        <CardItem itemDivider header style={{paddingTop: 25}}>
                            <Text style={{fontWeight: 'bold', fontSize: theme.fontSizeMedium}}>
                                General Summary
                            </Text>
                        </CardItem>

                        <CardItem style={{paddingTop: 0}}>
                            <Content>
                            <Textarea
                                onChangeText={text => this.setState({general_summary: text})}
                                rowSpan={3} bordered
                                value={general_summary}
                            />
                            </Content>
                        </CardItem>
                    </Card>
                    {/* End: General Summary */}


                    <Card style={{marginTop: 30, paddingBottom: 10}}>
                        {/* /////// Encounter Summary /////// */}
                        {/* Checked in with follower to say hello - found no problems */}
                        <CardItem itemDivider style={{}}>
                            <Text style={{fontWeight: 'bold', fontSize: theme.fontSizeMedium}}>Encounter Summary</Text>
                        </CardItem>
                        <CardItem button onPress={() => this.handleRadioSummary({summary_no_prob: true})}>
                            <Radio
                                color={theme.borderColor}
                                selectedColor={theme.defaultBgColor}
                                selected={summary_no_prob}
                                disabled
                            />
                            <Text style={styles.text1}>
                                Checked in with follower to say hello - found no problems.
                            </Text>
                        </CardItem>
                        {/* End: Checked in with follower to say hello - found no problems */}

                        {/* Checked in with follower to say hello - found health-related */}
                        <CardItem button onPress={() => this.handleRadioSummary({summary_health_issue: true})}>
                            <Radio
                                color={theme.borderColor}
                                selectedColor={theme.defaultBgColor}
                                selected={summary_health_issue}
                                disabled
                            />
                            <Text style={styles.text1}>
                                Checked in with follower to say hello - found health-related issue and forwarded
                                comments
                            </Text>
                        </CardItem>
                        {/* End: Checked in with follower to say hello - found health-related */}

                        {show_mandatory && (!summary_health_issue && !summary_no_prob) &&
                        <CardItem>
                            <Text style={styles.mandatory}>* This field is required.</Text>
                        </CardItem>
                        }

                    </Card>

                </Content>
                <Button style={{
                    backgroundColor: this.state.canSave !== true ? 'rgb(191,191,191)' : theme.redColor,
                    marginVertical: 5,
                    marginHorizontal: 11,
                    fontWeight: 'bold'
                }} disabled={this.state.canSave !== true} block onPress={this.saveData}>
                    <Text>End Encounter</Text>
                </Button>
            </Container>
        );
    }
}

const styles = {
    container: {},
    mandatory: {width: '100%', textAlign: 'left', color: 'red'},
    text: {},
    text1: {
        marginHorizontal: 15
    },
    mb: {
        marginBottom: 15
    }
};

export default PatientNote;
