import theme from '../../../../theme/default';

export default {
    container: {
        backgroundColor: '#FFF'
    },
    header: {
        backgroundColor: theme.defaultBgColor
    },
    content: {
        backgroundColor: 'green',
        paddingHorizontal: 10,
    },
    mb15: {
        marginBottom: 15
    },
    mb10: {
        marginBottom: 10
    },
    mt5: {
        paddingTop: 5
    },
    mb5: {
        paddingBottom: 5
    }
};
