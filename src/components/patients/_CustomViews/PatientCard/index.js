import React, {Component} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {Button, Card, CardItem, Icon, Text} from 'native-base';
import {Col, Grid, Row} from 'react-native-easy-grid';
import styles from './styles';
import theme from '../../../../theme/default';
import StateStore from '../../../../store/StateStore';
import call from 'react-native-phone-call';
import {Actions} from 'react-native-router-flux';

class PatientCard extends Component {
    constructor(props) {
        super(props);
        this.dialCall = this.dialCall.bind(this);
        this.onLocationShow = this.onLocationShow.bind(this);
    }

    componentWillMount(): void {

    }

    dialCall(mobile_number) {
        const args = {
            number: mobile_number, // String value with the number to call
            prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call
        };
        call(args).catch(console.error);
    }

    onLocationShow(patient) {
        let location = patient.zip;
        Actions.locationMap({patient: patient, title: `Location for ${patient.last_name}, ${patient.first_name}`});

    }

    render() {
        return (
            <Card style={styles.mb10}>
                <CardItem style={{
                    borderBottomColor: 'black',
                    borderBottomWidth: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                }}>
                    <Text style={{
                        fontSize: theme.fontSizeMedium,
                        color: theme.headerbgColor,
                        fontWeight: theme.fontWeightBold
                    }}>

                        {`${this.props.patient.last_name}`}
                        {', '}
                        {`${this.props.patient.first_name}`}

                    </Text>

                    <View>
                        <Text>
                            {`Age : ${this.props.patient.age}`}
                        </Text>

                        <Text>
                            {`Gender : ${this.props.patient.gender.charAt(0).toUpperCase() + this.props.patient.gender.slice(1)}`}
                        </Text>
                    </View>
                </CardItem>
                <CardItem>
                    <Grid>
                        <Row>
                            <Col size={4}>
                                <Text style={{
                                    fontSize: theme.fontSizeSmall
                                }}>
                                    Mobile Phone
                                </Text>
                            </Col>
                            <Col size={6}>
                                <TouchableOpacity onPress={() => this.dialCall(this.props.patient.mobile_number)}>
                                    <Text style={{
                                        color: 'blue'
                                    }}>
                                        {this.props.patient.mobile_number}
                                    </Text>
                                </TouchableOpacity>
                            </Col>
                        </Row>
                        <Row>
                            <Col size={4}>
                                <Text style={{
                                    fontSize: theme.fontSizeSmall
                                }}>
                                    Address
                                </Text>
                            </Col>
                            <Col size={6}>
                                <Text>
                                    {this.props.patient.address}
                                </Text>
                            </Col>
                        </Row>
                        <Row>
                            <Col size={4}>
                                <Text style={{
                                    fontSize: theme.fontSizeSmall
                                }}>
                                    City
                                </Text>
                            </Col>
                            <Col size={6}>
                                <Text>
                                    {this.props.patient.city}
                                </Text>
                            </Col>
                        </Row>
                        <Row>
                            <Col size={4}>
                                <Text style={{
                                    fontSize: theme.fontSizeSmall
                                }}>
                                    County
                                </Text>
                            </Col>
                            <Col size={6}>
                                <Text>
                                    {this.props.patient.county}
                                </Text>
                            </Col>
                        </Row>
                        <Row>
                            <Col size={4}>
                                <Text style={{
                                    fontSize: theme.fontSizeSmall
                                }}>
                                    State
                                </Text>
                            </Col>
                            <Col size={6}>
                                <Text>
                                    {StateStore.abbrToName(this.props.patient.state)}
                                </Text>
                            </Col>
                        </Row>
                        <Row>
                            <Col size={4}>
                                <Text style={{
                                    fontSize: theme.fontSizeSmall
                                }}>
                                    Zip
                                </Text>
                            </Col>
                            <Col size={6}>
                                <Text>
                                    {this.props.patient.zip}
                                </Text>
                            </Col>
                        </Row>
                        <Row>
                            <Col size={4}>
                                <Text style={{
                                    fontSize: theme.fontSizeSmall
                                }}>
                                    Location
                                </Text>
                            </Col>
                            <Col size={6}>
                                <TouchableOpacity onPress={() => this.onLocationShow(this.props.patient)}>
                                    <Text style={{
                                        color: 'blue'
                                    }}>
                                        Map
                                    </Text>
                                </TouchableOpacity>
                            </Col>
                        </Row>
                    </Grid>
                </CardItem>
                <Grid style={[styles.mb10]}>
                    <Col size={3}/>
                    <Col size={32} button>
                        <Button iconLeft small block
                                style={{
                                    borderRadius: theme.borderRadiusBase,
                                    backgroundColor: theme.defaultButtonBackground

                                }}
                                onPress={this.props.onSelect}

                        >
                            <Icon name='person'/>
                            <Text>Select</Text>
                        </Button>
                    </Col>
                    <Col size={1}/>
                    <Col size={32}>
                        <Button iconLeft small block
                                style={{
                                    borderRadius: theme.borderRadiusBase,
                                    backgroundColor: theme.defaultButtonBackground
                                }}>
                            <Icon name='ios-chatboxes'/>
                            <Text>Initiate Travel</Text>
                        </Button>
                    </Col>
                    <Col size={3}/>
                </Grid>
            </Card>
        );
    }
}

export default PatientCard;
