import theme from '../../../../theme/default';

export default {
    description: {
        backgroundColor: 'white',
        padding: 10,
        color: theme.defaultTextColor,
        borderRadius: 5,
        alignItems: 'center',
        fontSize: theme.fontSizeSmall,
        lineHeight: theme.lineHeightMedium,
    },
    menu_container: {}
};
