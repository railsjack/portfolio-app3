import React, {Component} from 'react';
import {Overlay} from 'teaset';
import {Button, Text, View} from 'native-base';
import theme from '../../../../theme/default';
import styles from './styles';

export class POP extends Component {

    constructor(props) {
        super(props);
    }

    static actionSheet = (options) => {
        let overlayView1 = (
            <Overlay.PullView side='bottom'
                              style={{alignItems: 'center', justifyContent: 'center'}}
                              modal={true}
                              ref={v => this.overlayView1 = v}
            >
                <View>
                    <Text style={styles.description}>{options.description}</Text>
                    {options.buttons && options.buttons.map((button, index) => {
                        return <Button
                            key={`key_${index}`}
                            {...button.options}
                            style={{borderRadius: theme.borderRadiusBase, margin: 2,}}
                            onPress={() => {
                                button.onPress && button.onPress();
                                this.overlayView1 && this.overlayView1.close();
                            }}>
                            <Text>{button.text}</Text>
                        </Button>;
                    })
                    }
                </View>
            </Overlay.PullView>
        );
        Overlay.show(overlayView1);
    };

    static modal = (options) => {

        let overlayView2 = (
            <Overlay.View side='bottom'
                          style={{alignItems: 'center', justifyContent: 'center'}}
                          modal={true}
                          ref={v => this.overlayView2 = v}
            >
                <View style={{
                    width: '100%', height: '100%', backgroundColor: 'white',
                    flexDirection: 'column'
                }}>
                    <View style={{width: '100%', height: '100%', paddingBottom: 52}}>{options.contentView}</View>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'stretch',
                        position: 'absolute', zIndex: 1, bottom: 1,
                        width: '100%'
                    }}>
                        {options.buttons && options.buttons.map((button, index) => {
                            return <Button
                                key={`key_${index}`}
                                {...button.options}
                                style={{borderRadius: 0,
                                    width: `${100 / options.buttons.length}%`}}
                                onPress={() => {
                                    button.onPress && button.onPress();
                                    this.overlayView2 && this.overlayView2.close();
                                }}>
                                <Text>{button.text}</Text>
                            </Button>;
                        })
                        }</View>
                </View>
            </Overlay.View>
        );
        Overlay.show(overlayView2);
        return this.overlayView2;
    };

    render() {
        return '';
    }
}
