/**
 * Custom Map for handling Zip Code and Location
 * */

import React, {Component} from 'react';
import {Alert, Image, Platform, Text, View} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import GeoCoder from '../../../../store/GeoCoderStore';
import MarkerArray from '../../../../store/MarkerArrayStore';

import srcMarkerIcon from '../../../../assets/images/marker-icon.png';
import srcMarkerIconBlue from '../../../../assets/images/marker-icon-blue.png';
import srcMarkerIconGreen from '../../../../assets/images/marker-icon-green.png';
import srcMarkerIconRed from '../../../../assets/images/marker-icon-red.png';
import constants from '../../../_base_data/constants';

/**
 * Customize MapView Class of react-native-maps
 * */
class CustomMapView extends Component {
    state = {
        loaded: false,
        region: {
            latitude: 0, //37.78825,
            longitude: 0, //-122.4324,
            latitudeDelta: 0.0922 * 2 * 2,
            longitudeDelta: 0.0421 * 2 * 2,
        },
        markers: []
    };

    constructor(props) {
        super(props);
        this._isMounted = false;
    }

    componentDidMount(): void {
        this._isMounted = true;
        setTimeout(async () => {
            this.showMapWithCurrentLocation()
                .then(async location => {

                    MarkerArray.set(this.state.markers);
                    

                    Platform.OS !== 'ios' && MarkerArray.add({
                        coordinate: location.coords, description: 'Your current location',
                        color: 'red'
                    });
                    await this.setState({markers: MarkerArray.get()});

                    if (this.props.patient) {

                        const {address, city, county, state, zip} = this.props.patient;
                        const _address = [address, city, county, state, zip].join(','); // There is invalid address & city in demo data
                        if (!address || !city || !county || !state || !zip) {
                            Alert.alert(constants.APP.NAME, 'Address of the patient is invalid. Please check it.');
                            return;
                        }
                        try {
                            let _location = await GeoCoder.getLocationByAddress(_address);
                            MarkerArray.add({
                                coordinate: _location,
                                description: `${this.props.patient.first_name}'s location`,
                                color: 'green'
                            });
                            await this.setState({markers: MarkerArray.get()});
                            let _coordinates = MarkerArray.getCoordinates();
                            setTimeout(async () => {
                                await this._mapView.fitToCoordinates(_coordinates, {
                                    edgePadding: {top: 250, left: 10, bottom: 10, right: 10}
                                });
                                this.mapDidLoad && this.mapDidLoad();
                            }, 1000);
                        } catch (error) {
                            this.props.onFailed && this.props.onFailed(error);
                        }
                    } else {
                        this.mapDidLoad && this.mapDidLoad();
                    }
                })
                .catch(error => {
                    this.props.onFailed && this.props.onFailed(error);
                });
        }, 1000);
    }

    onMapReady = async () => {

    };

    componentWillUnmount() {
        this._isMounted = false;
    }

    mapDidLoad = () => {
        if (this.props.showNavIcon) {

        }
    };


    /**
     * Set the region of the given Map
     * */
    setMapRegion = (location) => {
        let _region = this.state.region;
        this.setState({
            region: {
                latitude: location.latitude,
                longitude: location.longitude,
                latitudeDelta: _region.latitudeDelta,
                longitudeDelta: _region.longitudeDelta,
            },
            mapShown: true
        });
    };


    /**
     * actionSheet the Map in current location
     * */

    showMapWithCurrentLocation = async () => {
        return new Promise((resolve, reject) => {
            GeoCoder.getCurrentLocation()
                .then(async location => {
                    await this.setMapRegion({
                        latitude: location.coords.latitude,
                        longitude: location.coords.longitude
                    });
                    this.setState({loaded: true});
                    resolve(location);
                })
                .catch(error => {
                    this._isMounted && this.setState({debugError: JSON.stringify(error)});
                    reject(error);
                });
        });
    };


    render() {
        return (
            <View style={{width: '100%', height: '100%'}}>
                <Image style={{position: 'absolute', top: -200}}
                       source={srcMarkerIcon}/>
                {this.state.loaded &&
                <MapView style={{width: '100%', height: '100%'}}
                         ref={(_map) => this._mapView = _map}
                         initialRegion={{
                             latitude: this.state && this.state.region && this.state.region.latitude,
                             longitude: this.state && this.state.region && this.state.region.longitude,
                             latitudeDelta: 0.0922,
                             longitudeDelta: 0.0421,
                         }}
                         onMapReady={this.onMapReady}

                         _cacheEnabled={true}
                         _followsUserLocation={true}
                         _loadingEnabled={true}
                         _moveOnMarkerPress={false}
                         _showsCompass={true}
                         _showsMyLocationButton={true}
                         _showsPointsOfInterest={true}
                         _showsScale={true}
                         showsUserLocation={true}
                         _stopPropagation={true}
                         _toolbarEnabled={true}
                         _tracksInfoWindowChanges={false}
                         _tracksViewChanges={false}
                         _zoomControlEnabled={true}
                         maxZoomLevel={10}
                >
                    {this.state.markers.map((marker, index) => {
                        return <Marker key={`key_${index}`}
                                       coordinate={marker.coordinate}
                                       stopPropagation={true}
                                       style={{width: 80, height: 70}}
                        >
                            <Image
                                style={{position: 'absolute', bottom: 1, width: 26, left: 27}}
                                source={
                                    (marker.color === 'green' && srcMarkerIconGreen) ||
                                    (marker.color === 'blue' && srcMarkerIconBlue) ||
                                    (marker.color === 'red' && srcMarkerIconRed)
                                }/>
                            <Text style={{
                                fontWeight: 'bold',
                                fontSize: 12,
                                position: 'absolute',
                                top: 5, left: 0,
                                width: '100%', height: 25,
                                lineHeight: 12,
                                paddingVertical: 0,
                                textAlign: 'center',
                                backgroundColor: 'rgba(255,255,255, 0.6)'
                            }}>
                                {marker.description}
                            </Text>
                        </Marker>;
                    })}
                </MapView>
                }
            </View>
        );
    }
}

export default CustomMapView;
