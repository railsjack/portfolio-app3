import React, {Component} from 'react';
import {Alert} from 'react-native';
import {Button, Card, CardItem, Container, Content, Radio, Text} from 'native-base';
import theme from '../../theme/default';
import constants from '../_base_data/constants';


class CancelAppoint extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show_mandatory: false,
        };
    };

    async componentWillMount(): void {
    }

    async componentDidMount(): void {
    }


    handleRadioSummary = async (selectedSummary) => {
        await this.setState({
            reason_no_answer: false,
            reason_unavailable: false,
            reason_cancel_by_patient: false,
            reason_cancel_by_nurse: false,
        });
        await this.setState(selectedSummary);
        await this.setState({canSave: this.buildData(false) !== false});
    };


    buildData = (withSave = true) => {
        const {
            reason_no_answer, reason_unavailable,
            reason_cancel_by_patient, reason_cancel_by_nurse,
        } = this.state;
        if ((!reason_no_answer && !reason_unavailable && !reason_cancel_by_patient && !reason_cancel_by_nurse) /* not selected anything in summary */
        ) {
            return false;
        }

        if (withSave === false) {
            return true;
        }
        return {
            reason_no_answer: reason_no_answer,
            reason_unavailable: reason_unavailable,
            reason_cancel_by_patient: reason_cancel_by_patient,
            reason_cancel_by_nurse: reason_cancel_by_nurse,
        };

    };
    saveData = async () => {

        await this.setState({show_mandatory: false});
        let saveData = this.buildData();
        if (!saveData) {
            await this.setState({show_mandatory: true});
        }
        console.log('saveData: ', saveData);
        Alert.alert(constants.APP.NAME, 'Saving data...: ' + JSON.stringify(saveData));
    };


    render() {
        const {
            show_mandatory,
            reason_no_answer, reason_unavailable,
            reason_cancel_by_patient, reason_cancel_by_nurse,
        } = this.state;

        return (
            <Container style={[styles.container, {flex: 1, flexDirection: 'column', justifyContent: 'space-between',}]}>
                <Content padder style={{backgroundColor: theme.grayBgColor}}>

                    <Text style={{
                        textAlign: 'center',
                        fontSize: 20,
                        fontWeight: 'bold',
                        marginVertical: 15,
                    }}>
                        Cancel for Appointment
                    </Text>

                    <Card style={{marginTop: 30, paddingBottom: 10}}>
                        {/* /////// Reason for Cancellation /////// */}
                        <CardItem itemDivider style={{}}>
                            <Text style={{fontWeight: 'bold', fontSize: theme.fontSizeMedium}}>Reason for
                                Cancellation</Text>
                        </CardItem>
                        {/* No answer at door. */}
                        <CardItem button onPress={() => this.handleRadioSummary({reason_no_answer: true})}>
                            <Radio
                                color={theme.borderColor}
                                selectedColor={theme.defaultBgColor}
                                selected={reason_no_answer}
                                disabled
                            />
                            <Text style={styles.text1}>
                                No answer at door.
                            </Text>
                        </CardItem>
                        {/* End: No answer at door. */}

                        {/* Door answered, but patient not there or not available. */}
                        <CardItem button onPress={() => this.handleRadioSummary({reason_unavailable: true})}>
                            <Radio
                                color={theme.borderColor}
                                selectedColor={theme.defaultBgColor}
                                selected={reason_unavailable}
                                disabled
                            />
                            <Text style={styles.text1}>
                                Door answered, but patient not there or not available.
                            </Text>
                        </CardItem>
                        {/* End: Door answered, but patient not there or not available. */}

                        {/* Cancelled in route – By Patient */}
                        <CardItem button onPress={() => this.handleRadioSummary({reason_cancel_by_patient: true})}>
                            <Radio
                                color={theme.borderColor}
                                selectedColor={theme.defaultBgColor}
                                selected={reason_cancel_by_patient}
                                disabled
                            />
                            <Text style={styles.text1}>
                                Cancelled in route – By Patient
                            </Text>
                        </CardItem>
                        {/* End: Cancelled in route – By Patient */}

                        {/* Cancelled in route – By Mobile MedNurse */}
                        <CardItem button onPress={() => this.handleRadioSummary({reason_cancel_by_nurse: true})}>
                            <Radio
                                color={theme.borderColor}
                                selectedColor={theme.defaultBgColor}
                                selected={reason_cancel_by_nurse}
                                disabled
                            />
                            <Text style={styles.text1}>
                                Cancelled in route – By Mobile MedNurse
                            </Text>
                        </CardItem>
                        {/* End: Cancelled in route – By Patient */}

                        {show_mandatory && (!reason_no_answer && !reason_unavailable) &&
                        (!reason_cancel_by_patient && !reason_cancel_by_nurse) &&
                        <CardItem>
                            <Text style={styles.mandatory}>* This field is required.</Text>
                        </CardItem>
                        }

                    </Card>

                </Content>
                <Button style={{
                    backgroundColor: this.state.canSave !== true ? 'rgb(191,191,191)' : theme.redColor,
                    marginVertical: 5,
                    marginHorizontal: 11,
                    fontWeight: 'bold'
                }} disabled={this.state.canSave !== true} block onPress={this.saveData}>
                    <Text>Submit</Text>
                </Button>
            </Container>
        );
    }
}

const styles = {
    container: {},
    mandatory: {width: '100%', textAlign: 'left', color: 'red'},
    text: {},
    text1: {
        marginHorizontal: 15
    },
    mb: {
        marginBottom: 15
    }
};

export default CancelAppoint;
