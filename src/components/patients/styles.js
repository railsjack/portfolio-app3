import theme from '../../theme/default';

export default {
    container: {
        backgroundColor: '#FFF'
    },
    header: {
        backgroundColor: theme.defaultBgColor
    },
    content: {
        backgroundColor: 'green',
        paddingHorizontal: 10,
    },
    mb: {
        marginBottom: 15
    },
    mb5: {
        paddingBottom: 5
    },

    actions: {
        button: {
            flexDirection: 'row',
            justifyContent: 'flex-start',
            borderRadius: theme.borderRadiusBase,
            width: '90%',
            alignSelf: 'center',
            margin: 5,
        }
    }
};
