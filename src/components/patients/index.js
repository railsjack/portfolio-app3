import React, {Component} from 'react';
import {Platform} from 'react-native';
import PatientCard from './_CustomViews/PatientCard';
import PatientStore from '../../store/PatientStore';
import styles from './styles';
import {Button, Container, Content, Header, Icon, Input, Item, Text} from 'native-base';
import theme from '../../theme/default';

import SessionStore from '../../store/SessionStore';
import {Actions} from 'react-native-router-flux';
import {FlatList} from 'react-native';
import StateStore from '../../store/StateStore';

class Patients extends Component {

    constructor(props) {
        super(props);
    }

    async componentWillMount(): void {
    };

    async componentDidMount(): void {
        SessionStore.getUserData()
            .then(async userData => {
                await this.setState({currentUser: userData});

                this.state && this.state.currentUser &&
                PatientStore.findByNurseID(this.state.currentUser.nurse_id)
                    .then(patients => {
                        let _patients = [];
                        patients.map(patient=>{
                            let _patient = patient;
                            _patient.state_fullname = StateStore.abbrToName(_patient.state);
                            _patients.push(_patient);
                        })
                        this.setState({
                            patients: _patients
                        });
                        this.basePatients = _patients;
                    });
            });

    }

    searchFilterFunction = async text => {
        const patients = PatientStore.search(text, this.basePatients);
        await this.setState({patients: patients});
    };

    _onSelect = (patient) => {
        Actions.patientAction({patient: patient, title: `${patient.last_name}, ${patient.first_name}`});
    };

    render() {
        return (
            <Container style={styles.container}>
                <Header androidStatusBarColor={theme.defaultBgColor}
                        iosBarStyle="light-content"
                        style={{backgroundColor: 'white'}} searchBar rounded>
                    <Item>
                        <Icon active name="ios-search"/>
                        <Input placeholder="Search patients here" autoCorrect={false}
                               style={Platform.OS === 'android' ? {
                                   borderWidth: theme.borderWidth,
                                   borderColor: theme.borderColorLight,
                                   borderRadius: 50,
                                   height: 35, padding: 0,
                                   paddingLeft: 15,
                               }: {} }
                               onChangeText={text => this.searchFilterFunction(text)}
                        />
                        <Icon active name="people"/>
                    </Item>
                    <Button transparent>
                        <Text>Search</Text>
                    </Button>
                </Header>
                <Content padder>
                    {this.state && this.state.patients &&
                    <FlatList
                        data={this.state.patients}
                        extraData={this.state}
                        keyExtractor={(item, index) => String(index)}
                        renderItem={({item}) => {
                            return (
                                <PatientCard
                                    onSelect={() => {
                                        this._onSelect(item);
                                    }}
                                    patient={item}/>
                            );
                        }}
                    />
                    }
                </Content>
            </Container>
        );
    }
}

export default Patients;
