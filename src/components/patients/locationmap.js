import React, {Component} from 'react';
import PatientCard from './_CustomViews/PatientCard';
import PatientStore from '../../store/PatientStore';
import GeoCoderStore from '../../store/GeoCoderStore';
// import CustomMapView from '../userform/_CustomViews/CustomMapView';
import CustomMapView from '../patients/_CustomViews/CustomMapView';
import styles from './styles';
import {View} from 'react-native'
import {Button, Container, Content, Header, Icon, Input, Item, Left, ListItem, Right, Text} from 'native-base';
import theme from '../../theme/default';
import SessionStore from '../../store/SessionStore';

class LocationMap extends Component {

    constructor(props) {
        super(props);
        this.props = props;
    }

    componentWillMount(): void {
    };

    render() {
        const patient = this.props.patient
        return (
            <View style={{width: "100%"}}>
                <CustomMapView
                                patient={patient}
                                onFailed={error => {
                                    Alert.alert(constants.APP.NAME, 'An error occurred ' +
                                        +`while fetching current location. \n Detail: ${error}`);
                                    this.mapModal.close();
                                }}
                                style={{width: '100%', height: '100%'}}/>
                
                
            </View>
        );
    }
}

export default LocationMap;
