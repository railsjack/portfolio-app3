export default {
    medicine_data: [
        {id: 1,  title: 'Acetaminophen', name: 'Acetaminophen'},
        {id: 2,  title: 'Adderall', name: 'Adderall'},
        {id: 3,  title: 'Alprazolam', name: 'Alprazolam'},
        {id: 4,  title: 'Amitriptyline', name: 'Amitriptyline'},
        {id: 5,  title: 'Amlodipine', name: 'Amlodipine'},
        {id: 6,  title: 'Amoxicillin', name: 'Amoxicillin'},
        {id: 7,  title: 'Ativan', name: 'Ativan'},
        {id: 8,  title: 'Atorvastatin', name: 'Atorvastatin'},
        {id: 9,  title: 'Azithromycin', name: 'Azithromycin'},
        {id: 10, title: 'Ciprofloxacin', name: 'Ciprofloxacin'}
    ]
};
