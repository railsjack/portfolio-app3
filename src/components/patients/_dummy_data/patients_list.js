export default {
    'patients': [
        {
            'id': 109,
            'first_name': 'Jonas',
            'last_name': 'Chaney',
            'gender': 'male',
            'dob': '1994-01-09',
            'mobile_number': '(830)-456-7896',
            'address': '123 MainStreet',
            'city': 'Clearwater',
            'county': 'Pinellas',
            'state': 'FL',
            'zip': '33755',
            'user_id': 4,
            'created_at': '2019-04-24T06:56:05.000Z',
            'updated_at': '2019-04-30T06:18:53.000Z',
            'intake_selected': [
                '13',
                '22'
            ],
            'total_price': '10.0',
            'latitude': null,
            'longitude': null,
            'age': 25
        },
        {
            'id': 110,
            'first_name': 'Jonas',
            'last_name': 'Chaney',
            'gender': 'male',
            'dob': '1994-01-09',
            'mobile_number': '(830)-456-7896',
            'address': '8 N Stewart Ave',
            'city': 'Kissimmee',
            'county': 'Osceola',
            'state': 'FL',
            'zip': '34741',
            'user_id': 3,
            'created_at': '2019-04-24T06:56:56.000Z',
            'updated_at': '2019-04-30T06:18:54.000Z',
            'intake_selected': [
                '13',
                '22'
            ],
            'total_price': '10.0',
            'latitude': null,
            'longitude': null,
            'age': 25
        }
    ]
};
