import React, {Component} from 'react';
import {Body, Button, Card, CardItem, Container, Content, Icon, Left, Text} from 'native-base';
import {Col, Grid, Row} from 'react-native-easy-grid';
import {View} from 'react-native';
import styles from './styles';
import {POP} from './_CustomViews/POP';
import CustomMapView from './_CustomViews/CustomMapView';
import {Alert} from 'react-native';
import constants from '../_base_data/constants';
import GeoCoderStore from '../../store/GeoCoderStore';
import {Actions} from 'react-native-router-flux';
import UserStore from '../../store/UserStore';
import {logoutUser} from '../../actions/AuthActions';
import CountDown from 'react-native-countdown-component';


class Encounter extends Component {
    constructor(props) {
        super(props);
        this.props = props
    }


    _generateNote = () => {
        Actions.patientNote({title: 'Encounter notes'});
    };

    render() {
        const patient = this.props.patient
        return (
            <Container style={styles.container}>
            <Content padder>
                <Card style={styles.mb} transparent>
                    <Grid>
                        <Row>
                            <Col size={4}>
                                <Button iconLeft block success
                                    style={styles.actions.button}
                                    onPress={this._generateNote}
                                    
                                >
                                <Icon name='ios-paper'/>
                                <Text>Generate Notes</Text>
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col size = {4}>
                                <Button iconLeft block success
                                    style={styles.actions.button}       
                                    >
                                    <Icon name='ios-play'/>
                                    <Text>End Encounter</Text>
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Text> Remaining Time </Text>
                            <CountDown
                            style= {{marginLeft: 25, marginTop: 15}}
                            until={900}
                            digitStyle={{backgroundColor: 'green', borderWidth: 2, borderColor: '#1CC625'}}
                            digitTxtStyle={{color: 'white'}}
                            timeLabelStyle={{color: '#1CC625', fontWeight: 'bold'}}
                            separatorStyle={{color: '#1CC625'}}
                            timeToShow={['M', 'S']}
                            timeLabels={{m:"Minutes", s: "Seconds"}}
                            showSeparator
                            size={20}
                            />
                    </Row>
                    </Grid>
                </Card>
            </Content>
        </Container>
        );
    }
}



export default Encounter;
