import React, {Component} from 'react';
import CustomMapView from '../patients/_CustomViews/CustomMapView';
import {Alert, View} from 'react-native';
import constants from '../_base_data/constants';

class TrackNurse extends Component {

    constructor(props) {
        super(props);
        this.props = props;
    }

    componentWillMount(): void {
    };

    render() {
        const patient = this.props.patient;
        return (
            <View style={{width: '100%'}}>
                <CustomMapView
                    _patient={patient}
                    showNavIcon={true}
                    style={{width: '100%', height: '100%'}}
                    onFailed={error => {
                        Alert.alert(constants.APP.NAME, 'An error occurred ' +
                            `while fetching current location. \n Detail: ${JSON.stringify(error)}`);
                    }}
                    />
            </View>
        );
    }
}

export default TrackNurse;
