import React, {Component} from 'react';
import {Body, Button, Card, CardItem, Container, Content, Icon, Left, Text} from 'native-base';
import {Col, Grid, Row} from 'react-native-easy-grid';
import {View} from 'react-native';
import styles from './styles';
import {POP} from './_CustomViews/POP';
import CustomMapView from './_CustomViews/CustomMapView';
import {Alert} from 'react-native';
import constants from '../_base_data/constants';
import GeoCoderStore from '../../store/GeoCoderStore';
import {Actions} from 'react-native-router-flux';
import UserStore from '../../store/UserStore';
import {logoutUser} from '../../actions/AuthActions';

class InitiatedTravel extends Component {
    constructor(props) {
        super(props);
        this.props = props
    }

    beginEncounter =() =>{
        //Actions.encounter({patient: this.props.patient, title: `Visiting ${this.props.patient.last_name}`})
        Actions.patientNote({patient: this.props.patient, title: `Visiting ${this.props.patient.last_name}`});
    }


    cancelAppointment =() =>{
        Actions.cancelAppoint({title: "Cancel Appointment"});
    }


    noAnswer =() =>{
        Alert.alert("All LInk", "No Answer")
    }

    render() {
        const patient = this.props.patient
        return (
            <View>
                <CustomMapView
                patient={patient}
                style={{width: '100%'}}
                />
                <Grid style={{bottom: 115}}>
                    <Row>
                        <Col size= {4}>
                            <Button iconLeft block success
                                    onPress={this.beginEncounter}
                                    style={styles.actions.button}
                                    >
                                <Icon name='ios-play'/>
                                <Text>Begin Encounter</Text>
                            </Button>
                        </Col>
                    </Row>
                    <Row style= {{marginTop: 55}}>
                        <Col size={2} style= {{marginRight: 2}}>
                            <Button iconLeft block success
                                style={styles.actions.button}
                                onPress={this.noAnswer}
                            >
                            <Icon name='ios-navigate'/>
                            <Text>No Answer</Text>
                            </Button>
                        </Col>
                        <Col size = {2}>
                            <Button iconLeft block success
                                style={styles.actions.button}
                                onPress={this.cancelAppointment}
                                >
                                <Icon name='ios-arrow-round-back'/>
                                <Text>Cancel Appointment</Text>
                            </Button>
                        </Col>
                    </Row>

                </Grid>
            </View>

        );
    }
}



export default InitiatedTravel;
