import React, {Component} from 'react';
import {Alert, Platform} from 'react-native';
import {Button, Card, Container, Content, Icon, Text} from 'native-base';
import {Col, Grid, Row} from 'react-native-easy-grid';

import styles from './styles';
import {POP} from './_CustomViews/POP';
import CustomMapView from './_CustomViews/CustomMapView';
import constants from '../_base_data/constants';
import GeoCoderStore from '../../store/GeoCoderStore';
import {Actions} from 'react-native-router-flux';
import UserStore from '../../store/UserStore';
import {logoutUser} from '../../actions/AuthActions';
import {initiatedTravel} from './initiatedtravel';
import NotifService from '../../service/NotifService';
import PushNotification from 'react-native-push-notification';
import {notifyPatient} from '../../actions/PatientActions';


class PatientAction extends Component {

    state = {
        working: false,
        clicked: null,
        senderId: constants.APP.NOTIFICATION.SENDER_ID
    };

    constructor(props) {
        super(props);
        this.props = props;

        this.nClearTimer = 0;

        this.notif = new NotifService(this.onRegister.bind(this), this.onNotif.bind(this));
    }

    setWorking = async (isWorking) => await this.setState({working: isWorking});
    goBack = async () => {
        // await Actions.mainview();
    };


    onRegister(token) {
    }

    onNotif(notif) {
        console.log('onNotif(notif)', notif, Platform.OS);
        if (Platform.OS === 'ios') {
            this.notif.cancelNotifById(notif.id);
            const patient = this.props.patient;
            Actions.trackNurse({patient: patient, title: 'Location of Nurse'});
            PushNotification.setApplicationIconBadgeNumber(0);
        } else {
            const patient = this.props.patient;
            Actions.trackNurse({patient: patient, title: 'Location of Nurse'});
            this.notif.cancelNotifById(notif.id);
        }
    }

    handlePerm(perms) {
        Alert.alert('Permissions', JSON.stringify(perms));
    }

    _calculateDistance = async () => {
        const patient = this.props.patient ||
            {
                id: 1,
                first_name: 'Bvrvmw',
                last_name: 'Jknofloc',
                zip: '33978',
                address: 'Lahore',
                city: 'Ut aliquid et nulla',
                county: 'Kent',
                state: 'DE',
                territory: '123'
            };
        let distance;
        try {
            const {address, city, county, state, zip} = patient;
            const _address = [address, city, county, state, zip].join(','); // There is invalid address & city in demo data
            if (!address || !city || !county || !state || !zip) {
                Alert.alert(constants.APP.NAME, 'Address of the patient is invalid. Please check it.');
                return;
            }
            await this.setWorking(true);
            distance = await GeoCoderStore.getDistanceFromCurrentTo(_address);
            if (!distance) {
                Alert.alert(constants.APP.NAME, 'Address of the patient is invalid. Please check it.');
                await this.setWorking(false);
                return;
            }
        } catch (error) {
            Alert.alert(constants.APP.NAME, 'An error occurred ' +
                +`while getting distance. \n Detail: ${error}`);
            await this.setWorking(false);
            return;
        }
        await this.setWorking(false);
        return distance;
    };

    _initiateTravelWithNotification = async () => {
        const distance = await this._calculateDistance();
        let time;
        time = (distance.value * 1000) / 10.2889;   // normal taxi speed = 20knots = 10.2889m/s
        //time %= 18000; // Trim hours which is over 5hrs.
        const hrs = parseInt(time / 3600);
        const mins = parseInt((time % 3600) / 60);
        const timestr = `${hrs ? hrs + (hrs === 1 ? 'hr' : ' hrs') : ''} ${mins ? mins + (mins === 1 ? ' min' : ' mins') : ''}`;

        const patient = this.props.patient;
        const title = `Patient ${patient && patient.first_name} ${patient && patient.last_name} is ${distance.text} away from you`;
        const content = `it will take ${timestr} to reach them.`;
        //alert(JSON.stringify(distance)); return;
        const notifID = this.notif.localNotif({
            title: title,
            message: content,
            subText: 'Notification',
            bigText: content,
            actions: null,
            userInfo: {id: Math.random().toString().substr(2, 6)}
        });
        if (Platform.OS === 'android') {
            this.nClearTimer && clearTimeout(this.nClearTimer);
            this.nClearTimer = setTimeout(() => {
                this.notif.cancelNotifById(notifID);
            }, 10000);
        }
        console.log("distance", distance)
        data = {
            time: timestr,
            distance: distance.value
        }
        notifyPatient(data, patient.id)
        this._initiateTravel();
    };

    _initiateTravel = async () => {
        const distance = await this._calculateDistance();

        const patient = this.props.patient;
        const distanceInMiles = Math.ceil(distance.value * 0.62137);
        time = (distance.value * 1000) / 10.2889;   // normal taxi speed = 20knots = 10.2889m/s
        const hrs = parseInt(time / 3600);
        const mins = parseInt((time % 3600) / 60);
        const timestr = `${hrs ? hrs + (hrs === 1 ? 'hr' : ' hrs') : ''} ${mins ? mins + (mins === 1 ? ' min' : ' mins') : ''}`;
        POP.actionSheet({
            description: `Patient ${patient && patient.first_name} ${patient && patient.last_name} is ${distanceInMiles} miles ` +
                `away from your current location and it will take ${timestr} to reach them.`,
            buttons: [
                {
                    text: 'Confirm', options: {primary: true, block: true},
                    onPress: () => {
                        Actions.initiatedTravel({patient: patient, title: `initiated travel for ${patient.last_name}`});
                        
                        // this.mapModal = POP.modal({
                        //     containerStyle: {},
                        //     contentView: <CustomMapView
                        //         patient={patient}
                        //         onFailed={error => {
                        //             Alert.alert(constants.APP.NAME, 'An error occurred ' +
                        //                 +`while fetching current location. \n Detail: ${error}`);
                        //             this.mapModal.close();
                        //         }}
                        //         style={{width: '100%', height: '100%'}}/>,
                        //     buttons: [
                        //         {
                        //             text: 'Begin Encounter', options: { width: '95%', bordered: true, success: true, block: true}
                        //         }
                                
                        //     ],
                        //     buttons: [
                        //         {
                        //             text: 'No Answer', options: {primary: true, block: true}
                        //         },
                        //         {
                        //             text: 'Cancel Appointment', options: {bordered: true, block: true}
                        //         }
                        //     ]
                        // });
                    }
                },
                {
                    text: 'Cancel', options: {bordered: true, danger: true, block: true}
                }
            ]
        });
    };

    _generateNote = async () => {
        Actions.patientNote({title: 'Encounter notes'});
    };

    render() {
        const {patient} = this.props;
        return (
            <Container style={styles.container}>
                <Content padder>
                    <Card style={styles.mb} transparent>
                        <Grid>
                            <Row>
                                <Col size={2}>
                                    <Button iconLeft block success
                                            style={styles.actions.button}
                                            onPress={this._initiateTravelWithNotification}
                                            disabled={this.state && this.state.working}
                                    >
                                        <Icon name='ios-navigate'/>
                                        <Text>Initiate{'\n'}
                                            Travel{this.state && this.state.clicked}</Text>
                                    </Button>
                                </Col>
                                <Col size={2}>
                                    <Button iconLeft block success
                                            style={styles.actions.button}
                                            onPress={this.goBack}
                                    >
                                        <Icon name='ios-arrow-round-back'/>
                                        <Text>Previous</Text>
                                    </Button>
                                </Col>
                            </Row>
                        </Grid>
                    </Card>
                </Content>
            </Container>
        );
    }
}

export default PatientAction;
