/**
 * Defines styles for all elements over the project
 * It includes size, style, color
 * ex: fontSize, fontFamily, fontColor...
 * */

import color from 'color';

import {Dimensions, Platform} from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const platform = Platform.OS;
const platformStyle = undefined;
const isIphoneX =
    platform === 'ios' && (deviceHeight === 812 || deviceWidth === 812 || deviceHeight === 896 || deviceWidth === 896);

export default {
    platformStyle,
    platform,

    // Font
    DefaultFontSize: 16,
    fontFamily: platform === 'ios' ? 'System' : 'Roboto',
    fontSizeBase: 15,
    get fontSizeLarge() {
        return platform === 'ios' ? this.fontSizeBase * 1.4 : this.fontSizeBase * 1.4;
    },
    get fontSizeMedium() {
        return platform === 'ios' ? this.fontSizeBase * 1.2 : this.fontSizeBase * 1.2;
    },
    get fontSizeSmall() {
        return platform === 'ios' ? this.fontSizeBase * 1.1 : this.fontSizeBase * 1.1;
    },
    get fontSizeMini() {
        return platform === 'ios' ? this.fontSizeBase * 0.8 : this.fontSizeBase * 0.8;
    },
    get fontWeightBold() {
        return platform === 'ios' ? '600' : '500';
    },
    get fontWeightNormal() {
        return platform === 'ios' ? '400' : '400';
    },


    // StatusBar
    toolbarDefaultBg: platform === 'ios' ? '#F8F8F8' : '#3F51B5',
    get statusBarColor() {
        return color(this.toolbarDefaultBg)
            .darken(0.2)
            .hex();
    },


    // Text, TextInput
    get inputColor() {
        return this.textColor;
    },
    get inputColorPlaceholder() {
        return '#575757';
    },
    // Text
    textColor: '#444',
    placeholderSize: "10",
    inverseTextColor: '#fff',
    defaultBgColor: 'rgb(65, 178, 171)',
    grayBgColor: 'rgb(249, 249, 249)',
    lightGrayBgColor: 'rgb(191, 191, 191)',
    noteFontSize: 14,
    get defaultTextColor() {
        return this.textColor;
    },

    get placeholderFontSize(){
        return this.placeholderSize;
    },


    // Line Height
    lineHeightLarge: 32,
    lineHeightMedium: 27,
    lineHeightSmall: 22,
    lineHeight: platform === 'ios' ? 20 : 24,

    // Spinner
    defaultSpinnerColor: '#45D56E',
    inverseSpinnerColor: '#1A191B',

    // DatePicker


    // Button
    btnHeight: 27,
    btnLineHeight: 19,


    // iOS Color
    redColor: 'rgb(255,59,48)',
    orangeColor: 'rgb(255,149,0)',
    yellowColor: 'rgb(255,204,0)',
    greenColor: 'rgb(76,217,100)',
    headerbgColor: 'rgb(65,178,171)',
    tealBlueColor: 'rgb(90,200,250)',
    blueColor: 'rgb(0,122,255)',
    purpleColor: 'rgb(88,86,255)',
    pinkColor: 'rgb(255,45,85)',

    // Menu
    menuBgColor: 'rgba(250, 250, 250, 1)',

    //Button
    defaultButtonBackground: 'rgb(65,178,171)',

    // Other
    borderRadiusBase: platform === 'ios' ? 5 : 3,
    borderWidth: 1.5, //1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderColor: 'rgba(130, 130, 130, .5)',
    borderColorLight: 'rgba(190, 190, 190, .5)',
    contentPadding: 10,
    dropdownLinkColor: '#414142',
    inputLineHeight: 24,
    deviceWidth,
    deviceHeight,
    isIphoneX,
    inputGroupRoundedBorderRadius: 30,

    //iPhoneX SafeArea
    Inset: {
        portrait: {
            topInset: 24,
            leftInset: 0,
            rightInset: 0,
            bottomInset: 34
        },
        landscape: {
            topInset: 0,
            leftInset: 44,
            rightInset: 44,
            bottomInset: 21
        }
    }
};
