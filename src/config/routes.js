/**
 * Router Configuration file
 * Using react-native-router-flux
 *

 */
import React, {Component} from 'react';
import LandingPage from '../components/landingpage';
import Login from '../components/login';
import Scheduler from '../components/scheduler';
import Patients from '../components/patients';
import LocationMap from '../components/patients/locationmap';
import PatientAction from '../components/patients/action';
import TrackNurse from '../components/patients/tracknurse';
import History from '../components/history';
import HistoryDetail from '../components/history/detail';
import Account from '../components/account';
import SideBar from '../components/shared/SideBar';
import {Drawer, Router, Scene, Stack} from 'react-native-router-flux';
import Signup from '../screens/signup';
import {Icon} from 'native-base';
import theme from '../theme/default';
import SessionStore from '../store/SessionStore';
import PatientNote from '../components/patients/patientnote';
import InitiatedTravel from '../components/patients/initiatedtravel';
import Encounter from '../components/patients/encounter';
import ForgotPassword from '../components/forgot_password';
import Welcome from '../components/welcome';
import CancelAppoint from '../components/patients/cancelappoint';

class Routes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: undefined,
            userData: null

        };
    }


    componentWillMount(): void {
        SessionStore.isLoggedIn()
            .then(async (loggedData) => {
                await this.setState({isLoggedIn: loggedData.isLoggedIn, userData: loggedData.userData});
            });
    }

    render() {

        return (
            <Router>
                <Stack key="root">
                    <Scene key="landing"
                           component={LandingPage}
                           hideNavBar={true} type='reset'/>
                    <Scene key="login"
                           initial={this.state && this.state.isLoggedIn === false}
                           hideNavBar={true} type='reset'
                           component={Login} title="Mobile MedNurse Login"/>
                    <Drawer
                        key="mainview"
                        type="reset" initial={this.state && this.state.isLoggedIn === true}
                        contentComponent={SideBar}
                        drawerIcon={<Icon style={{color: 'white'}} name='menu'/>}
                        hideNavBar drawerPosition="right">
                        <Router hideNavBar>
                            <Stack key="drawerRoot"
                                   navigationBarStyle={{backgroundColor: theme.defaultBgColor}}
                                   titleStyle={{color: theme.inverseTextColor}}>
                                <Scene key="welcome" component={Welcome} />
                                <Scene key="patients" component={Patients}
                                       title={`Patients for ${this.state.userData != null ? this.state.userData.first_name : ''} ${this.state.userData != null ? this.state.userData.last_name : ''}`}/>
                                <Scene key="patientAction" component={PatientAction} />
                                <Scene key="patientNote" component={PatientNote} />
                                <Scene key="cancelAppoint" component={CancelAppoint} />
                                <Scene key="trackNurse" component={TrackNurse} />
                                <Scene key="history" title='History/Logs' component={History} />
                                <Scene key="historyDetail" component={HistoryDetail} />
                                <Scene key="account" component={Account} title="Account"/>
                                <Scene key="initiatedTravel" component={InitiatedTravel} title="MedNurse In Travel"/>
                                <Scene key="encounter" component={Encounter} title="MedNurse In Travel"/>
                            </Stack>
                        </Router>
                    </Drawer>

                    <Scene key="scheduler" component={Scheduler} hideNavBar={true}/>
                    <Scene key="locationMap" component={LocationMap} title={`Location`}/>
                    <Scene key="signup" component={Signup} title="Mobile MedNurse Sign Up"/>
                    <Scene key="forgotPassword" component={ForgotPassword} title="Forgot Password"/>
                </Stack>
            </Router>
        );
    }
}

export default Routes;
