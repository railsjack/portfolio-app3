import constants from '../components/_base_data/constants';


class StorageStore {

    constructor(){
        this.value = {};
    }

    set = (key, value) => {
        this.value[constants.APP.STORAGE_KEY + '_' + key] = value;
    };

    get = (key) => {
        return this.value[constants.APP.STORAGE_KEY + '_' + key];
    }
};

export default new StorageStore();
