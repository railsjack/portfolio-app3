import validationRules from "./ValidationRules";
import validatejs from "../components/userform/libs/validate.min";
import constants from "../components/_base_data/constants";
import {Alert} from "react-native";

class ValidationStore{
    /**
     * Checks if a given value is valid or not
     * @params
     *   fieldName
     *     name of the field to be checked
     *     ex: name
     *   formValues
     *     object includes name and value
     *     ex: {name: "Tom", gender: "male", birth: "...", ...}
     *   callback
     *     a method of function to be executed if the given value is not valid.
     *   callback_arg
     *     argument to be used when execute callback
     * @return
     *   returns false if the given values is not valid, otherwise true
     * @description
     *   This validation's rule is based on "ValidationRule.js"
     *   This method is not exported, and it's private.
     *   It is called in the following methods.
     * */
    processValidate = (fieldName, formValues, callback, callback_arg) => {
        const result = validatejs(formValues, validationRules);
        // If there is an error message, return it!
        if (result && result[fieldName]) {
            // Return only the field error message if there are multiple
            callback && callback(callback_arg);
            Alert.alert(constants.APP.NAME, result[fieldName][0]);
            return false;
        }
        return true;
    };
    /**
     * Checks if a given value is valid or not
     * @params
     *   fieldName
     *     name of the field to be checked
     *     ex: name
     *   formValues
     *     object includes name and value
     *     ex: {name: "Tom", gender: "male", birth: "...", ...}
     *   callback
     *     a method of function to be executed if the given value is not valid.
     * @return
     *   returns false if the given values is not valid, otherwise true
     * @description
     *   This validation's rule is based on "ValidationRule.js"
     * */
    validate = (fieldName, values, callback) => {
        let formValue = {};
        formValue[fieldName] = values[fieldName];
        return this.processValidate(fieldName, formValue, callback);
    };


    /**
     * Checks if a given value is valid or not
     * @params
     *   fieldName
     *     name of the field to be checked
     *     ex: password
     *   withFieldName
     *     name of the other field name to be checked at the same time
     *     It's used to check the equiality.
     *     ex: password and confirm_password
     *   formValues
     *     object includes name and value
     *     ex: {name: "Tom", gender: "male", birth: "...", ...}
     *   callback
     *     a method of function to be executed if the given value is not valid.
     * @return
     *   returns false if the given values is not valid, otherwise true
     * @description
     *   This validation's rule is based on "ValidationRule.js"
     *   Since withFieldName is dependent on fieldName, you need to describe
     *   the validation rule in ValidationRule.js. Refer to the sample on it.
     * */
    validate_with = (fieldName, withFieldName, values, callback) => {
        let formValues = {};
        formValues[fieldName] = values[fieldName];
        formValues[withFieldName] = values[withFieldName];
        return this.processValidate(fieldName, formValues, callback);
    };


    /**
     * Checks if a given value is valid or not
     * It checks the validation in the collection
     * ex: licensure_cards: [
     *   {state: "AZ", designation: "RN". license_no: "123"},
     *   {state: "AL", designation: "CNA". license_no: "456"},
     * ]
     * @params
     *   fieldNames
     *     array of names to be checked
     *     ex: ["state", "designation", "license_no"]
     *   valuesInCollection
     *     collection including the values (It's array, but not object)
     *     ex: licensure_cards: [
     *       {state: "AZ", designation: "RN". license_no: "123"},
     *       {state: "AL", designation: "CNA". license_no: "456"},
     *     ]
     *   callback
     *     a method of function to be executed if the given value is not valid.
     *     It is called back with the given argument.
     *     ex: index: 1
     * @return
     *   returns false if the given values is not valid, otherwise true
     * @description
     *   This validation's rule is based on "ValidationRule.js"
     *   the validation rule in ValidationRule.js. Refer to the sample on it.
     *
     * */
    validate_collection = (fieldNames, valuesInCollection, callback) => {

        if (!valuesInCollection || valuesInCollection.length == 0) {
            callback && callback(0)
            return false;
        }

        for (let _index in valuesInCollection) {
            let formValue = valuesInCollection[_index];
            for (let _index1 in fieldNames) {
                let fieldName = fieldNames[_index1];
                let _ret = this.processValidate(fieldName, formValue, callback, _index);
                if (!_ret) {
                    return false;
                }
            }
        }

        return true;
    };


};

export default new ValidationStore();
