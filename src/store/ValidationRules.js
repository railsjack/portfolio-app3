const validationRules = {
    first_name: {
        // first name is required
        presence: {
            message: "^Please enter your first name"
        },
        length: {
            minimum: 1,
            message: "^Please enter your first name"
        }
    },
    last_name: {
        // last name is required
        presence: {
            message: "^Please enter your last name"
        },
        length: {
            minimum: 1,
            message: "^Please enter your last name"
        }
    },
    email: {
        // Email is required
        presence: {
            message: "^Please enter an email address"
        },
        // and must be an email (duh)
        email: {
            message: "^Please enter a valid email address"
        }
    },
    dob: {
        // Email is required
        presence: {
            message: "^Please select your birth day"
        },
        // and must be an email (duh)
        length: {
            minimum: 1,
            message: "^Please select your birth day"
        }
    },
    address1: {
        presence: {
            message: "^Please enter your address 1"
        },
        length: {
            minimum: 1,
            message: "^Please enter your address 1"
        }
    },
    address2: {
        presence: {
            message: "^Please enter your address 2"
        },
        length: {
            minimum: 1,
            message: "^Please enter your address 2"
        }
    },
    city: {
        presence: {
            message: "^Please enter your city"
        },
        length: {
            minimum: 1,
            message: "^Please enter your city"
        }
    },
    zip: {
        presence: {
            message: "^Please enter your zip code"
        },
        length: {
            minimum: 1,
            message: "^Please enter your zip code"
        }
    },
    home_phone: {
        presence: {
            message: "^Please enter your home phone"
        },
        length: {
            minimum: 1,
            message: "^Please enter your home phone"
        }
    },
    mobile_phone: {
        presence: {
            message: "^Please enter your mobile phone"
        },
        length: {
            minimum: 1,
            message: "^Please enter your mobile phone"
        }
    },
    bio: {
        presence: {
            message: "^Please enter your bio"
        },
        length: {
            minimum: 1,
            message: "^Please enter your bio"
        }
    },
    //licensure_card:{
    state: {
        presence: {
            message: "^Please select a state"
        },
        length: {
            minimum: 1,
            message: "^Please select a state"
        }
    },
    county: {
        presence: {
            message: "^Please select a county"
        },
        length: {
            minimum: 1,
            message: "^Please select a county"
        }
    },
    designation: {
        presence: {
            message: "^Please select a designation"
        },
        length: {
            minimum: 1,
            message: "^Please select a designation"
        }
    },
    license_no: {
        presence: {
            message: "^Please enter your a license number"
        },
        length: {
            minimum: 1,
            message: "^Please enter your a valid license number"
        }
    },
    upload_picture_valid: {
        presence: {
            message: "^Please select your picture for upload"
        },
        length: {
            minimum: 1,
            message: "^Please select your picture for upload"
        }
    },
    upload_resume_valid: {
        presence: {
            message: "^Please select your resume for upload"
        },
        length: {
            minimum: 1,
            message: "^Please select your resume for upload"
        }
    },
    //},

    password: {
        // Password is also required
        presence: {
            message: "^Please enter a password"
        },
        // And must be at least 8 characters long
        length: {
            minimum: 8,
            message: "^Your password must be at least 8 characters"
        }
    },
    password_confirmation: {
        // You need to confirm your password
        presence: true,
        // and it needs to be equal to the other password
        equality: {
            attribute: "password",
            message: "^The passwords does not match"
        }
    },
};

export default validationRules;
