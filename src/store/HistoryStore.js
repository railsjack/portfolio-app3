import constants from '../components/_base_data/constants';
import {Alert} from 'react-native';
import SessionStore from './SessionStore';

class HistoryStore {

    constructor() {
        this.value = {};
    }

    getLogs = () => {
        return new Promise(async (resolve, reject) => {
            if (constants.APP.USE_PRE_DATA.IN_HISTORY_LIST) {
                let demoData = this.getDemoData();
                resolve(demoData.logs);
            } else {
                let _logs = [];
                let _currentUser = await SessionStore.getUserData();

                let _url = constants.APP.END_POINT.HISTORY.LIST+'?authentication_token='+_currentUser.auth_token;
                _url = _url.replace(/\:id/gi, nurse_id);
                console.log('_url: ', _url);
                fetch(_url, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'authentication_token': _currentUser.auth_token
                    },
                }).then(response => response.json())
                    .then(async (response) => {

                        if (response.error) {
                            Alert.alert(constants.APP.NAME, "Please sing in to access Patients List");
                        } else {

                            _logs = response.logs;

                            resolve(_logs);
                        }
                    });
            }
        });
    };

    getDemoData = () => {
        return constants.PRE_DATA.HISTORY_LIST;
    };

    search = (key, in_array) => {
        return in_array.filter(item => {
            const itemData = JSON.stringify(Object.values(item)).toUpperCase();
            const textData = key.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
    };

};

export default new HistoryStore();
