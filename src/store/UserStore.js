import constants from '../components/_base_data/constants';
import StateStore from './StateStore';
import LicensureCardStore from './LicensureCardStore';
import SessionStore from './SessionStore';


class UserStore {

    constructor() {
        this.value = {};
    }

    getCurrentUser = async () => {

        let _tmp;

        let _logged_user;

        if (constants.APP.USE_PRE_DATA.IN_USERSTORE) {
            _logged_user = constants.PRE_DATA.RESPONSE_WHEN_LOGGED_IN.data;
            _tmp = _logged_user.nurse || {};
            _tmp.nurse_id = _tmp.id;
            _tmp.email = (_logged_user.user || {}).email;
            _tmp.auth_token = (_logged_user.user || {}).authentication_token;
        } else {
            /*_logged_user = Storage.get('loggedUser') || {};*/
            _tmp = await SessionStore.getUserData();

        }
        return _tmp;
    };


    /**
     * Get the information from the current user.
     * */
    getUserFormData = async () => {
        let _user_data = await this.getCurrentUser();


        let formData = {
            stepFormData1: Object.assign({}, constants.APP.USERFORM_DATA_STRUCTURE.stepFormData1),
            stepFormData2: Object.assign({}, constants.APP.USERFORM_DATA_STRUCTURE.stepFormData2),
            stepFormData3: Object.assign({}, constants.APP.USERFORM_DATA_STRUCTURE.stepFormData3),
            stepFormData4: Object.assign({}, constants.APP.USERFORM_DATA_STRUCTURE.stepFormData4),
        };

        /**
         * Get the information for Step 1
         * */
        formData.stepFormData1.first_name = _user_data.first_name;
        formData.stepFormData1.last_name = _user_data.last_name;
        formData.stepFormData1.gender = _user_data.gender;
        formData.stepFormData1.dob = this.getValidDate(_user_data.dob, 'M/D/Y');
        formData.stepFormData1.mobile_number = _user_data.mobile_number;
        formData.stepFormData1.phone_number = _user_data.phone_number;
        formData.stepFormData1.address1 = _user_data.address1;
        formData.stepFormData1.address2 = _user_data.address2;
        formData.stepFormData1.city = _user_data.city;
        formData.stepFormData1.county = _user_data.county;
        formData.stepFormData1.state = StateStore.abbrToName(_user_data.state);
        formData.stepFormData1.zip = _user_data.zip;
        formData.stepFormData1.bio = _user_data.bio;
        formData.stepFormData1.email = _user_data.email;


        let _cards = [];
        _user_data.nurse_states &&
        _user_data.nurse_states.map(state => {
            _cards.push({
                state: StateStore.abbrToName(state.state),
                designation: LicensureCardStore.getDesignationName(state.designation),
                license_no: state.license_no
            });
        });

        formData.stepFormData1.licensure_cards = _cards;
        formData.stepFormData1.upload_picture_valid = 'a';
        formData.stepFormData1.upload_picture.fileName = 'a';
        formData.stepFormData1.upload_resume_valid = 'b';
        formData.stepFormData1.upload_resume.fileName = 'b';

        /**
         * This is necessary for testing, and it should be removed.
         * */
        formData.stepFormData1.password = 'abcdef123';
        formData.stepFormData1.password_confirmation = 'abcdef123';
        // =========== End of Step 1


        /**
         * Get the information for Step 2
         * */
        let _counties = {};
        _user_data.counties &&
        _user_data.counties.map(county => {
            _counties[StateStore.getStateNameByNumber(county.nurse_state_id)] = county;
        });
        formData.stepFormData2.counties = _counties;
        // =========== End of Step 2


        return formData;
    };

    /**
     * Get the valid formated date from the given IOS date
     * 'dob': '1994-01-09',
     * */
    getValidDate = (date, toFormat) => {
        let d = typeof date === 'object' ? date : new Date(date),
            month = (d.getMonth() + 1),
            day = (d.getDate() + 1),
            year = d.getFullYear();

        day = (day < 10 ? '0' : '') + day;
        month = (month < 10 ? '0' : '') + month;
        year = (year < 100 ? 1900 : 0) + year;

        let ret_date = toFormat;
        ret_date = ret_date.replace(/Y/gi, year < 100 ? year + 1000 : year);
        ret_date = ret_date.replace(/M/gi, month);
        ret_date = ret_date.replace(/D/gi, day);


        return ret_date;

    };

};

export default new UserStore();
