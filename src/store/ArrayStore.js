class ArrayStore {
    items = [];
    _justRemovedItem = {};

    constructor(items) {
        this.items = items;
    }

    set = (items) => {
        this.items = items;
    };

    all = () => {
        return this.items;
    };

    findById = (id) => {
        return this.items.filter(filtered_item => filtered_item.id === id)[0];
    };

    add = (item) => {
        this.items.push(item);
    };

    remove = (item) => {
        this._justRemovedItem = item;
        this.items = this.items.filter(filtered_item => filtered_item.id !== item.id);
    };

    getJustRemovedItem = () => {
        return this._justRemovedItem;
    };


    indexOf = (item) => {
        let ret = -1;
        if (this.items) {
            for (let i = 0, _len = this.items.length; i < _len; i++) {
                if (this.items[i].id === item.id) {
                    ret = i;
                    break;
                }
            }
        }
        return ret;
    };

}

export default new ArrayStore();
