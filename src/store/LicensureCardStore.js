import license_designations from "../components/_base_data/license_designations";

class LicensureCardStore {
    constructor() {

    }

    getDesignations = () => {
        return license_designations;
    };

    getDesignationName = (index) => {
        return license_designations.filter(d => d.index === index)[0].name;
    };

    getDesignationIndex = (name) => {
        return license_designations.filter(d => d.name === name)[0].index;
    };

    createNewByIndex = (index) => {
        return {
            key: `key_${index}`,
            index: index,
            name: `State ${index}`
        };
    };

    createWithOriginalData = (orgCardData) => {
        return {
            state: orgCardData.state,
            designation: orgCardData.designation,
            license_no: orgCardData.license_no
        };
    };

};

export default new LicensureCardStore();
