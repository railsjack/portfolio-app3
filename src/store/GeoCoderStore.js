import constants from '../components/_base_data/constants';
import AsyncStorage from '@react-native-community/async-storage';
import MarkerArrayStore from './MarkerArrayStore';

/**
 * Handles all method related Google Map and GeoCoderStore
 * */
class GeoCoderStore {
    constructor() {

    }


    /**
     * Get EndPoint URL corresponding to keys
     *  @params:
     *    key:
     *      It determines which to get end point
     *      GET_ZIP_CODE
     *        get zip code by lat,lng
     *      GET_LOCATION
     *        get location(lat,lng) by zip code
     *    ...args
     *      variant arguments
     *      It differs depending on keys
     *  @return
     *    it differs depending on keys
     *    in case key === "GET_ZIP_CODE"
     *      it returns endpoint URL for getting zip code by location
     *    in case key === "GET_LOCATION"
     *      it returns endpoint URL for getting location by zip code
     * */
    getAPIEndpointFor = (key, ...args) => {
        let _end_point, _arg, ret;

        /**
         * In case you need to get zip code by lat,lng
         * */
        if (key === 'GET_ZIP_CODE') {
            ret = ((key === 'GET_ZIP_CODE' &&
                (_arg = args[0]) &&
                (typeof _arg.latitude !== 'undefined' && typeof _arg.longitude !== 'undefined')) &&
                (_end_point = constants.GOOGLE_MAP.GEOCODE_URL.GET_BY_LATLNG ,
                    _end_point = _end_point.replace(/API_KEY/g, constants.GOOGLE_MAP.API_KEY) ,
                    _end_point = _end_point.replace(/LAT/g, _arg.latitude) ,
                    _end_point.replace(/LNG/g, _arg.longitude)));
        }

        /**
         * In case you need to get the location by zip code
         * */
        if (key === 'GET_LOCATION') {
            ret = (key === 'GET_LOCATION' &&
                (_arg = args[0]) &&
                (typeof _arg !== 'undefined') &&
                (_end_point = constants.GOOGLE_MAP.GEOCODE_URL.GET_BY_ZIPCODE ,
                    _end_point = _end_point.replace(/API_KEY/g, constants.GOOGLE_MAP.API_KEY) ,
                    _end_point.replace(/ZIPCODE/g, _arg)));
        }

        /**
         * In case you need to get the location by address
         * */
        if (key === 'GET_LOCATION_BY_ADDRESS') {
            ret = (key === 'GET_LOCATION_BY_ADDRESS' &&
                (_arg = args[0]) &&
                (typeof _arg !== 'undefined') &&
                (_end_point = constants.GOOGLE_MAP.GEOCODE_URL.GET_BY_ADDRESS ,
                    _end_point = _end_point.replace(/API_KEY/g, constants.GOOGLE_MAP.API_KEY) ,
                    _end_point.replace(/ADDRESS/g, _arg)));
        }

        /**
         * In case you need to get the distance
         * */
        if (key === 'GET_DISTANCE') {
            ret = (key === 'GET_DISTANCE' &&
                (_arg = args[0]) &&
                (typeof _arg !== 'undefined') &&
                (_end_point = constants.GOOGLE_MAP.GEOCODE_URL.GET_DISTANCE ,
                    _end_point = _end_point.replace(/API_KEY/g, constants.GOOGLE_MAP.API_KEY) ,
                    _end_point = _end_point.replace(/FROM/g, _arg.from_latlng),
                    _end_point.replace(/TO/g, _arg.to_latlng)));
        }

        return ret;
    };

    /**
     * Get the current location based on Mobile's GPS
     * */
    getCurrentLocation = () => {
        return new Promise((resolve, reject) => {
            navigator.geolocation
                .getCurrentPosition(
                    location => resolve(location),
                    error => reject(error),
                    {enableHighAccuracy: true}
                );
        });
    };


    /**
     * Get the Zip code from coordinates
     * */
    getZipCodeByCoordinate = async (coordinate) => {
        /**
         * we need to get the address component by coordinate from Google
         * */
        return new Promise((resolve, reject) => {
            let _end_point = this.getAPIEndpointFor('GET_ZIP_CODE', coordinate);
            fetch(_end_point)
                .then((response) => response.json())
                .then(addrComponent => {
                    let _zipcode = this.getZipCodeFromAddressComponent(addrComponent);
                    resolve(_zipcode);
                })
                .catch(error => reject(error));
        });
    };


    /**
     * Get Location from the Address Component of Google GeoCoderStore Response
     * @param
     *   response
     * @return
     *   return the location (latitude, longitude)
     * */
    getLocationFromAddressComponent = (response) => {
        return (response.status === 'OK' &&
            response.results &&
            response.results[0] &&
            response.results[0].geometry &&
            response.results[0].geometry.location &&
            response.results[0].geometry.location.lat &&
            response.results[0].geometry.location.lng &&
            {
                latitude: response.results[0].geometry.location.lat,
                longitude: response.results[0].geometry.location.lng
            }) ||
            {latitude: 0, longitude: 0};

    };


    /**
     * Get Zip Code from the Address Component of Google GeoCoderStore Response
     * @param
     *   response
     * @return
     *   return the zip code
     * @note
     * */
    getZipCodeFromAddressComponent = (response) => {
        let components;
        return (response.status === 'OK' &&
            response.results &&
            response.results[0] &&
            response.results[0].address_components &&
            (components = response.results[0].address_components.filter((addr_component) => {
                return addr_component &&
                    addr_component.types &&
                    // Google Map sends postal_code instead of zipcode
                    addr_component.types.indexOf('postal_code') > -1 &&
                    (addr_component.long_name);
            })) &&
            components.length > 0 &&
            components[0].long_name) || '';
    };

    /**
     * Get the location by Zip Code
     * @param: Zip code
     * @return:
     *   Promise for getting Location by Zip Code
     * */
    getLocationByZipCode = (zipcode) => {
        return new Promise((resolve, reject) => {
            let _end_point = this.getAPIEndpointFor('GET_LOCATION', zipcode);
            fetch(_end_point)
                .then((response) => response.json())
                .then(addrComponent => {
                    let _location = this.getLocationFromAddressComponent(addrComponent);
                    resolve(_location);
                })
                .catch(error => reject(error));

        });
    };


    /**
     * Get the location by State and County
     * @param:
     *   State: state name of US
     *   County: county name of the State
     * @return:
     *   Promise for getting Location by State and County
     * */
    getLocationByAddress = async (...args) => {
        return new Promise((resolve, reject) => {
            let _end_point = this.getAPIEndpointFor('GET_LOCATION_BY_ADDRESS', args.join('+').replace(/\s/gi,','));
            fetch(_end_point)
                .then((response) => response.json())
                .then(addrComponent => {
                    let _location = this.getLocationFromAddressComponent(addrComponent);
                    resolve(_location);
                })
                .catch(error => reject(error));

        });
    };

    /**
     * Save the marker information to the cache db
     * */
    addMarkerInfoToCache = (markerInfo) => {
        return new Promise((resolve, reject) => {
            this.readAllMarkersFromCache()
                .then(markers => {
                    let _markers = markers || [];

                    MarkerArrayStore.set(_markers);
                    MarkerArrayStore.indexOf(markerInfo) === -1 &&
                    MarkerArrayStore.add(markerInfo);
                    _markers = MarkerArrayStore.get();

                    AsyncStorage.setItem(constants.APP.STORAGE_KEY.LOCATION, JSON.stringify(_markers))
                        .then(response => resolve(response))
                        .catch(error => reject(error));
                })
                .catch(error => reject(error));
        });
    };

    /**
     * Get the marker information from cache db
     * */
    readAllMarkersFromCache = () => {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(constants.APP.STORAGE_KEY.LOCATION)
                .then(response => resolve(JSON.parse(response)))
                .catch(error => reject(error));
        });
    };


    /**
     * Get the zipcodes from zipcodes Array
     * */
    getZipCodesArrayFromZipCodeObjects = (zipcodeObjects) => {
        return (zipcodeObjects || []).map(zipcodeObject => zipcodeObject.zipcode);
    };


    /**
     * Get the locations from zipcodes
     * @params
     *   zipcodes: array
     * @return
     *   promize -> resolve (locations: array)
     * @notice
     *   It will returns multi promise
     * */
    getLocationsByZipCodes = (zipcodes) => {
        return new Promise((resolve, reject) => {
            let _promises = [];
            zipcodes.map(zipcode => {

                _promises.push(this.getLocationByZipCode(zipcode));
            });
            Promise.all(_promises)
                .then(values => {

                    resolve(values);
                })
                .catch(error => {

                });
        });
    };

    /**
     * Get distance between 2 points
     * @params
     *  from: string (lat,lng)
     *  to: string (lat,lng)
     * @return
     *  distance: float
     * */

    getDistanceBetween = (from_latlng, to_latlng) => {
        return new Promise((resolve, reject) => {
            /* we will not use google distance matrix api
            * While we find another alternative, it will return hardcoded value.*/
            resolve({value: 2500, text: '2.5 km'});

            /*let _arg = {from_latlng: from_latlng, to_latlng: to_latlng};
            let _end_point = this.getAPIEndpointFor('GET_DISTANCE', _arg);
            fetch(_end_point)
                .then((response) => response.json())
                .then(_distance => {
                    resolve(
                        _distance && _distance.rows && _distance.rows[0] && _distance.rows[0].elements &&
                        _distance.rows[0].elements[0] && _distance.rows[0].elements[0].distance
                    );
                })
                .catch(error => reject(error));*/

        });
    };

    /**
     * Get distance between A and B, here A is given by zipcode
     * @params
     *   zipcode: string
     * @return
     *   promize -> resoluve (distance: float)
     *   This is fastest algorithm checked by
     *   https://jsperf.com/haversine-salvador/32
     * */
    getDirectDistanceBetween = (lat1, lon1, lat2, lon2) => {
        const deg2rad = Math.PI / 180;
        lat1 *= deg2rad;
        lon1 *= deg2rad;
        lat2 *= deg2rad;
        lon2 *= deg2rad;
        const diam = 12742; // Diameter of the earth in km (2 * 6371)
        const dLat = lat2 - lat1;
        const dLon = lon2 - lon1;
        const a = (
            (1 - Math.cos(dLat)) +
            (1 - Math.cos(dLon)) * Math.cos(lat1) * Math.cos(lat2)
        ) / 2;
        const value = Number(diam * Math.asin(Math.sqrt(a))).toFixed(2) * 1;
        const text = `${value} km`;
        return {value, text};
    };

    /**
     * Get distance between A and B, here A is given by zipcode
     * @params
     *   zipcode: string
     * @return
     *   promize -> resoluve (distance: float)
     * */
    getDistanceFromCurrentTo = async (to_address) => {
        return new Promise((resolve, reject) => {
            this.getCurrentLocation()
                .then(current_location => {
                    this.getLocationByAddress(to_address)
                        .then(location=>{
                            const distance = this.getDirectDistanceBetween(
                                current_location.coords.latitude, current_location.coords.longitude,
                                location.latitude, location.longitude
                            );
                            resolve(distance);
                        })
                        .catch(error=> reject(error));
                    /*this.getDistanceBetween(from_latlng, to_address)
                        .then(distance => {
                            resolve(distance);
                        })
                        .catch(error => reject(error));*/
                })
                .catch(error => {
                    reject(error);
                });
        });
    };


}

export default new GeoCoderStore();
