class MarkerArrayStore {
    markers = [];
    _justRemovedMarker = {};

    constructor(markers) {
        this.markers = markers;
    }

    set = (markers) => {
        this.markers = markers;
    };

    get = () => {
        return this.markers;
    };

    add = (marker) => {
        this.markers.push(marker);
    };

    remove = (marker) => {
        this._justRemovedMarker = marker;
        this.markers = this.markers.filter(filtered_marker => filtered_marker.zipcode !== marker.zipcode);
    };

    getJustRemovedMarker = () => {
        return this._justRemovedMarker;
    };


    indexOf = (marker) => {
        let ret = -1;
        if (this.markers) {
            for (let i = 0, _len = this.markers.length; i < _len; i++) {
                if (this.markers[i].zipcode === marker.zipcode) {
                    ret = i;
                    break;
                }
            }
        }
        return ret;
    };

    getCoordinates = () => {
        let _coordinates = [];
        this.markers.map(marker => _coordinates.push({
            latitude: marker.coordinate.latitude,
            longitude: marker.coordinate.longitude
        }));
        return _coordinates;
    };

    getZipCodes = () => {
        let _zipcodes = [];
        this.markers.map(marker => _zipcodes.push(marker.zipcode));
        return _zipcodes;
    };
}

export default new MarkerArrayStore();
