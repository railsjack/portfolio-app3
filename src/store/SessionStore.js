import constants from '../components/_base_data/constants';
import AsyncStorage from '@react-native-community/async-storage';


class SessionStore {

    constructor() {

    }

    login = async (userData) => {
        let _tmp;
        _tmp = userData.nurse || {};
        _tmp.nurse_id = _tmp.id;
        _tmp.email = (userData.user || {}).email;
        _tmp.nurse_states = userData.nurse_states || {};
        _tmp.counties = userData.counties || {};
        _tmp.auth_token = (userData.user || {}).authentication_token;
        await AsyncStorage.setItem(constants.APP.STORAGE_KEY.LOGIN,
            JSON.stringify({userData: _tmp, isLoggedIn: true}));
    };

    logout = async () => {
        return AsyncStorage.removeItem(constants.APP.STORAGE_KEY.LOGIN);
    };

    isLoggedIn = async () => {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(constants.APP.STORAGE_KEY.LOGIN)
                .then(data=>{
                    let d = JSON.parse(data) || {};
                    console.log('isLoggedIn data: ', d);
                    resolve({isLoggedIn: d.isLoggedIn, userData: d.userData});
                })
                .catch(error=>reject(false, error));
        });
    };

    getUserData = async() => {
        return new Promise((resolve, reject)=>{
            AsyncStorage.getItem(constants.APP.STORAGE_KEY.LOGIN)
                .then(data=>{
                    let d = JSON.parse(data) || {};
                    resolve(d.userData);
                })
                .catch(error=>reject(error));
        });
    }
};

export default new SessionStore();
