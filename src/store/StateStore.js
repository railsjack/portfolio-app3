import state_counties from '../components/_base_data/state_counties';
import state_abbreviations from '../components/_base_data/state_abbreviations';
import state_abbr2fullnames from '../components/_base_data/state_abbr2fullnames';

class StateStore {
    constructor() {
    }

    allStates = () => {
        return Object.keys(state_counties)
            .map(name => {
                return {id: name, name: name, title: name, value: name};
            });
    };

    allCounties = (state_name) => {
        return state_counties[state_name] && state_counties[state_name].map(item => {
            return {id: item, name: item, title: item, value: item};
        });
    };

    allCountiesWithAbbr = (state_name) => {
        let _state_name = this.abbrToName(state_name);
        return Object.keys(state_counties)
            .filter(name => {
                return name === _state_name;
            });
    };

    allStateNames = () => {
        return Object.keys(state_counties)
            .map(name => {
                return {name: name, abbr: state_abbreviations[name]};
            });
    };

    getStateNameByNumber = (state_number) => {
        let _number = 1, break_flag = false, _state;
        Object.keys(state_counties)
            .map(state => {
                if (break_flag) {
                    return;
                }
                (state_number === _number++) && (break_flag = true, _state = state);
            });
        return _state;
    };

    nameToAbbr = (name) => state_abbreviations[name] || name;
    abbrToName = (abbr) => state_abbr2fullnames[abbr] || abbr;

}

export default new StateStore();
