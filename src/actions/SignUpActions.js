import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';
import {API_URL} from '../config'
import FCM from "react-native-fcm";
import {loginUser} from './'
import {has} from "lodash";
const REQUEST_URL = `${API_URL}/sessions`;
import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  LOGOUT_USER,
  LOGOUT_USER_SUCCESS,
  ACTION_IN_PROGRESS,
  RESET_TO_ACTION_QUEUE,
  RESET_ITEM_ACTION_QUEUE,
  RESET_TRIPLOG_LIST,
  SET_DEVICE_TOKEN,
  FIRSTNAME_CHANGED,
  LASTNAME_CHANGED,
  SIGNUP_FAILED,
  SIGNUP_SUCCESS,
  SIGNUP_INPROGRESS,
  CONFIRM_SIGNUP,
  INVALID_CONFIRM_CODE,
  CLEAR_ERROR,
  CONFIRM_SIGNUP_SUCCESS,
  RESENT_CONFIRMATION_CODE
} from './types';

export const signUpEmailChanged = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};

export const clearError = () => {
  return {
    type: CLEAR_ERROR,
  };
};

export const codeChanged = (text) => {
  return {
    type: CONFIRM_SIGNUP,
    payload: text
  };
};
export const signUpPasswordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};

export const signUpFirstnameChanged = (text) => {
  return {
    type: FIRSTNAME_CHANGED,
    payload: text
  };
};

export const signUpLastnameChanged = (text) => {
  return {
    type: LASTNAME_CHANGED,
    payload: text
  };
};

export const confirmSignup=(params)=>{
  return (dispatch,getState) => {
    const {signup_code, user}= params
    dispatch({ type: SIGNUP_INPROGRESS });
    const data = {
      signup_code: signup_code,
    }
      fetch(`${REQUEST_URL}/confirm_user`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json'
        },
        body: JSON.stringify({user: data})
      })
      .then((response) => response.json())
      .then((res) => {
        if(!res.error_code){
          dispatch({
            type: 'none',
          });
          dispatch({
            type: CONFIRM_SIGNUP_SUCCESS,
          });
          AsyncStorage.setItem('user', JSON.stringify(res)).then(()=> console.log('logged in successfully'))
          confirmUserSuccess(dispatch,getState,res)
        } else {
          dispatch({ type: INVALID_CONFIRM_CODE, confirmation_error: res.message });
        }
      })
      .catch((error) => {
        dispatch({ type: INVALID_CONFIRM_CODE, confirmation_error: res.message });
      })
      .done();
  }
}

export const resendConfirmEmail=({email})=>{
  return (dispatch,getState) => {
    dispatch({ type: SIGNUP_INPROGRESS });
    const data = {
      email: email,
    }
      fetch(`${REQUEST_URL}/resend_email`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json'
        },
        body: JSON.stringify({user: data})
      })
      .then((response) => response.json())
      .then((res) => {
        if(!res.error_code){
          dispatch({
            type: 'none',
          });
          dispatch({
            type: RESENT_CONFIRMATION_CODE,
          });
        } else {
          dispatch({ type: INVALID_CONFIRM_CODE, confirmation_error: 'Email address not found' });
        }
      })
      .catch((error) => {
        dispatch({ type: INVALID_CONFIRM_CODE, confirmation_error: res.message });
      })
      .done();
  }
}


export const signUp=({firstname,lastname,email,password})=>{
  return (dispatch,getState) => {
    dispatch({ type: SIGNUP_INPROGRESS });
    const data = {
      firstname: firstname,
      lastname: lastname,
      email: email,
      password: password,
    }
    // const json = JSON.stringify(data);
      fetch(`${REQUEST_URL}/sign_up`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json'
        },
        body: JSON.stringify({user: data})
      })
      .then((response) => response.json())
      .then((res) => {
        if(!res.error){
          dispatch({
            type: SIGNUP_SUCCESS,
            payload: res
          });
          Actions.ConfirmAccount({user: res})
        } else {
          dispatch({ type: SIGNUP_FAILED, signup_error:  res.error.split(",")[0]});
        }
      })
      .catch((error) => {
        dispatch({ type: SIGNUP_FAILED, signup_error:  error });
      })
      .done();
    }
}

const confirmUserSuccess = (dispatch, getState, user, navigation) => {
  FCM.requestPermissions({badge: true, sound: true, alert: true}).then(()=>{
    const{deviceToken,user} = getState().auth
    if (!deviceToken){
      FCM.getFCMToken().then(token => {
          const auth_token = user.authentication_token;
          const REQUEST_URL = `${API_URL}/sessions/register_device?access_token=${auth_token}`;
          var object = {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            body: JSON.stringify({
              device: {device_id: token}
            })
          };
            fetch(REQUEST_URL, object)
              .then(response => response.json())
              .then(responseJson => {
                if (has(responseJson, "error")) {
                } else {
                  dispatch({ type: SET_DEVICE_TOKEN, deviceToken: token });
                }
              })
              .catch(function(err) {
                alert(err);
              });

      });
    }
  }).catch(()=>console.log('notification permission rejected'));
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: user
  });
  Actions.main({ type: 'reset' });
};
