import { USERS_LIST } from '../actions/types';
import { API_URL } from '../config';
var _ = require('lodash')
export const getUsersList = () => {
  return (dispatch, getState) => {
    const user = getState().auth.user
    if (user) {
      const token = user.authentication_token
      const REQUEST_URL = `${API_URL}/users?access_token=${token}`
      fetch(REQUEST_URL)
        .then(response => response.json())
        .then(responseJson => {
          dispatch({ type: USERS_LIST, users: responseJson })
        })
        .catch(error => {
          alert(error)
        })
    }
  }
}



