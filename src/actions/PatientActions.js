import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import {Alert} from 'react-native';
import {API_URL} from '../config';
import {showMessage} from 'react-native-flash-message';
import Storage from '../store/StorageStore';
import SessionStore from '../store/SessionStore';
import constants from '../components/_base_data/constants';


export const notifyPatient = (data, patient_id) => {
    return new Promise(async (resolve, reject) => {
        let _currentUser = await SessionStore.getUserData();
        console.log("currentUser",_currentUser)
        const json = JSON.stringify(data);
        let _url = constants.APP.END_POINT.PATIENT.NOTIFICATION+'?authentication_token='+_currentUser.auth_token;
                    _url = _url.replace(/\:id/gi, patient_id);
        let REQUEST_URL = _url.replace(/\:nurse_id/gi, _currentUser.nurse_id);
        console.log("data", data)
        console.log("json", json)
        console.log("URL", REQUEST_URL)
        fetch(REQUEST_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
            body: json
        })
            .then((response) => response.json())
            .then(async (res) => {
                if (res.data) {
                    console.log('res', res);
                    
                } else {
                    
                }
            })
            .catch((error) => {
                console.log('Patient Action failed ', error);
            })
            .done();
    });
    

};

