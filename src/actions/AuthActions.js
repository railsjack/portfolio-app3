import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import {Alert, Platform, StatusBar} from 'react-native';
import {API_URL} from '../config';
import {showMessage} from 'react-native-flash-message';
import Storage from '../store/StorageStore';
import SessionStore from '../store/SessionStore';

const REQUEST_URL = `${API_URL}/sessions`;

export const loginUser = (data) => {
    const json = JSON.stringify(data);
    fetch(REQUEST_URL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        },
        body: json
    })
        .then((response) => response.json())
        .then(async (res) => {
            if (res.data) {
                console.log('res', res, res.data.nurse);
                await AsyncStorage.setItem('user', JSON.stringify(res.data.nurse));
                await Storage.set('loggedUser', res.data);
                await SessionStore.login(res.data);
                if (Platform.OS === 'android') {
                    StatusBar.setHidden(false);
                }
                Actions.mainview({currentNurse: res.data.nurse});
            } else {
                console.log('Login failed');
                showMessage({
                    message: 'Invalid Email or Password',
                    type: 'danger',
                    duration: 3000
                });
                Alert.alert('AllLink', 'Invalid Email or Password');
            }
        })
        .catch((error) => {
            console.log('Login failed in catch', error);
        })
        .done();
};

export const logoutUser = async (currentUser) => {
    let LOGOUT_URL = API_URL + '/sessions/:id?authentication_token=' + currentUser.auth_token;
    let url = LOGOUT_URL.replace(/:id/gi, currentUser.nurse_id);
    console.log('LOGOUT_URL: ', currentUser, url);
    fetch(url, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }

    })
        .then((response) => response.json())
        .then(async (res) => {
            console.log(res);
            if (res.success) {
                await SessionStore.logout();
                //AsyncStorage.setItem('user', null);
                Storage.set('loggedUser', null);
                // Actions.landing('Landing');
                Actions.login({userAction: true});
                showMessage({
                    message: 'You have been logged Out Successfully',
                    type: 'success',
                    duration: 3000
                });
            } else {
                console.log('Signout failed');
                showMessage({
                    message: res.message || "Signout failed",
                    type: 'danger',
                    duration: 3000
                });
                Actions.reset('login', {userAction: true});
            }
        })
        .catch((error) => {
            console.log('Sing out failed in catch', error);
        })
        .done();
};



export const resetPassword = (data) => {
    const json = JSON.stringify(data);
    fetch(REQUEST_URL + '/forgot_password', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        },
        body: json
    })
        .then((response) => response.json())
        .then(async (res) => {
            if (res.data) {
                console.log('res', res, res.data.nurse);
                Actions.reset('login');
            } else {
                console.log('Password reset failed');
                showMessage({
                    message: 'Failed to reset Password',
                    type: 'danger',
                    duration: 3000
                });
                Alert.alert('AllLink', 'Failed to reset Password');
            }
        })
        .catch((error) => {
            console.log('Failed to reset Password', error);
        })
        .done();
};
