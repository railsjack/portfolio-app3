//     AUTH ACTIONS
export const EMAIL_CHANGED = 'email_changed';
export const FIRSTNAME_CHANGED = 'firstname_changed';
export const LASTNAME_CHANGED = 'lastname_changed';
export const SIGNUP_FAILED = 'signup_failed';
export const SIGNUP_SUCCESS = 'signup_success';
export const SIGNUP_INPROGRESS = 'signup_inprogress';
export const CONFIRM_SIGNUP = 'confirm_signup';
export const CONFIRM_SIGNUP_SUCCESS = 'confirm_signup_success';
export const INVALID_CONFIRM_CODE = 'invalid_confirm_code';
export const RESENT_CONFIRMATION_CODE = 'resent_confirmation_code';

export const CLEAR_ERROR = 'clear_error';

export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';
export const LOGOUT_USER = 'logout_user';
export const LOGOUT_USER_SUCCESS = 'logout_user_success';
export const SET_INITIAL_STATE='set_initial_state'
export const LOAD_INITIAL_DATA='load_initial_data'
export const SET_DEVICE_TOKEN='set_device_token'
