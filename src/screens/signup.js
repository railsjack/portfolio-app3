/**
 * The screen for the signup
 * @WhatToDo
 *  Renders the Signup Form
 *  It is implemented by calling a Component in components/userform
 * @return
 *
 * */

import React, { Component } from "react";
import UserForm  from "../components/userform";
import constants from '../components/_base_data/constants';

class SignUp extends Component {
  render() {
    return <UserForm mode={constants.APP.USERFORM_MODE.SIGNUP} />;
  }
}

export default SignUp;
