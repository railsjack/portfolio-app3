/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import FlashMessage from 'react-native-flash-message';
import Routes from './src/config/routes';
import {TopView} from 'teaset';

export default class App extends Component {
    render() {
        return (
            <TopView>
                <Routes/>
                <FlashMessage/>
            </TopView>
        );
    }
}
