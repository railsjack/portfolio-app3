/**
 * @format
 */

import React from "react";
import {name as appName} from './app.json';
import App from './App';
//import App from './src/components/patients/cancelappoint';
import { StyleSheet, Text, View, AppRegistry } from "react-native";

AppRegistry.registerComponent(appName, () => App);
